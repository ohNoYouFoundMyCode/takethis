#include <stdio.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>

#include <lcm/lcm.h>
#include "exlcm_batteryInfo_t.h"

#define PORT_BATTERY_PORT "/dev/ttts6"
#define STARBOARD_BATTERY_PORT "/dev/ttts7"
#define BAUDRATE B115200

int debug = 0;

int main(int argc, char** argv)
{
	//setup for LCM
 	lcm_t * lcm = lcm_create("udpm://239.255.76.67:7667?ttl=1");
    if(!lcm) 
    {
		printf("LCM BATTERY channel failure\n");
    	return 1;
    }

    exlcm_batteryInfo_t my_data;
    
	//////////////////////////////////setup for Port Serial Port////////////////////////////////////
	//writing to port
	int PORT_SERIAL;
	struct termios Port_oldtio, Port_newtio;	//place for old and new port settings for serial port
	
	//open port port with error checking
	PORT_SERIAL = open(PORT_BATTERY_PORT, O_RDWR | O_NOCTTY );
	if (PORT_SERIAL <0) {perror(PORT_BATTERY_PORT); exit(-1); }
	
	tcgetattr(PORT_SERIAL,&Port_oldtio); // save current port settings 
	Port_newtio.c_cflag = BAUDRATE  | CS8 | CLOCAL;
	Port_newtio.c_iflag = IGNPAR;
	Port_newtio.c_oflag = 0;
	Port_newtio.c_lflag = 0;
	Port_newtio.c_cc[VMIN]=1;
	Port_newtio.c_cc[VTIME]=0;
	tcflush(PORT_SERIAL, TCIFLUSH);
	tcsetattr(PORT_SERIAL,TCSANOW,&Port_newtio);
	
	//////////////////////////////////setup for Starboard Serial Port////////////////////////////////////
	int STARBOARD_SERIAL;
	struct termios Starboard_oldtio, Starboard_newtio;	//place for old and new port settings for serial port

	STARBOARD_SERIAL = open(STARBOARD_BATTERY_PORT, O_RDWR | O_NOCTTY );
	if (STARBOARD_SERIAL <0) {perror(STARBOARD_BATTERY_PORT); exit(-1); }

	//configuration of serial port, 115200 baud, 8bits no parity 1 stop
	tcgetattr(STARBOARD_SERIAL,&Starboard_oldtio); // save current port settings 
	Starboard_newtio.c_cflag = BAUDRATE  | CS8 | CLOCAL | CREAD;
	Starboard_newtio.c_iflag = IGNPAR; //ignores parity errors
	Starboard_newtio.c_oflag = 0; //voodoo
	Starboard_newtio.c_lflag = 0; //voodoo
	Starboard_newtio.c_cc[VMIN]=1; //voodoo
	Starboard_newtio.c_cc[VTIME]=0; //voodoo
	tcflush(STARBOARD_SERIAL, TCIFLUSH); //clears whatever is in the serial port
	tcsetattr(STARBOARD_SERIAL,TCSANOW,&Starboard_newtio);	//sets configuration

	while(1)
	{
		///////////////////////stuff for read and write of port serial port /////////////////////////////
		//writes to RoboTeq motor controller with a query for the starboard battery voltage
		//only works when string is hard coded into the write function, not when passed as a string from sprintf function - Travis
		////////////////////////////////////PORT VOLTAGE/////////////////////////////////////////////////
		write(PORT_SERIAL, "?V 2", 8);
		//sets up variables for 
		char portBufferV[255];
		memset(portBufferV, '\0', sizeof portBufferV);
		read(PORT_SERIAL, portBufferV, sizeof(portBufferV));
		//clears garbage from end of string
		char voltagePort[10];
		strncpy(voltagePort, portBufferV+6, 4);
		voltagePort[4]=0;

		if(debug==1)
		{	
			printf("%s\n", portBufferV);
			printf("%s\n", voltagePort);
		}
		//converts buffer to int with proper conversion
		float portVoltage = atoi(voltagePort)/10.0;
		if(debug==1)
		{
			printf("%s", "The Port voltage is: ");
			printf("%.1f\n", portVoltage);
		}
		my_data.port_voltage = portVoltage;
		//sleep(1);


		///////////////////////stuff for read and write of starboard serial port /////////////////////////////
		//writes to RoboTeq motor controller with a query for the starboard battery voltage
		//only works when string is hard coded into the write function, not when passed as a string from sprintf function - Travis
		/////////////////////////////////////Starboard Voltage////////////////////////////////////////////////
		write(STARBOARD_SERIAL, "?V 2", 8);
		//sets up variables for 
		char starboardBuffer[255];
		memset(starboardBuffer, '\0', sizeof starboardBuffer);
		read(STARBOARD_SERIAL, starboardBuffer, sizeof(starboardBuffer));
		//clears garbage from end of string
		char voltageStarboard[10];
		strncpy(voltageStarboard, starboardBuffer+6, 4);
		voltageStarboard[4]=0;

		if(debug==1)
		{
			printf("%s\n", starboardBuffer);
			printf("%s\n", voltageStarboard);
		}
		//converts buffer to int with proper conversion
		float starboardVoltage = atoi(voltageStarboard)/10.0;

		if(debug==1)
		{
			printf("%s", "The Starboard voltage is: ");
			printf("%.1f\n", starboardVoltage);
		}
		my_data.starboard_voltage = starboardVoltage;
		//sleep(1);

		/////////////////////////////////////Port Current////////////////////////////////////////////////
		/*write(PORT_SERIAL, "?A", 6);
		//sets up variables for 
		char portBufferA[255];
		memset(portBufferA, '\0', sizeof portBufferA);
		sleep(1);
		read(PORT_SERIAL, portBufferA, sizeof(portBufferA));
		//clears garbage from end of string
		char currentPort[10];

		strncpy(currentPort, portBufferA+4, 4);
		printf("%s\n", portBufferA);
		printf("%s\n", currentPort);

		//converts buffer to int with proper conversion
		float portCurrent = atoi(currentPort)/10.0;
		printf("%s", "The Port current is: ");
		printf("%.1f\n", portCurrent);
		my_data.port_current = portCurrent;

		/////////////////////////////////////Starboard Current////////////////////////////////////////////////
		write(STARBOARD_SERIAL, "?A", 6);
		//sets up variables for 
		char starboardBufferA[255];
		memset(starboardBufferA, '\0', sizeof starboardBufferA);
		sleep(1);
		read(PORT_SERIAL, starboardBufferA, sizeof(starboardBufferA));
		//clears garbage from end of string
		char currentStarboard[10];
		strncpy(currentStarboard, starboardBufferA+6, 4);
		currentStarboard[4]=0;
		//converts buffer to int with proper conversion
		float starboardCurrent = atoi(currentStarboard)/10.0;
		printf("%s", "The Starboard current is: ");
		printf("%.1f\n", starboardCurrent);
		my_data.starboard_current = starboardCurrent;*/



	exlcm_batteryInfo_t_publish(lcm, "BATTERY", &my_data);
	}
    lcm_destroy(lcm);
	//TODO create escape functionality and port restore
	//restore port port to original settings
	//tcsetattr(PORT_SERIAL,TCSANOW,&Port_oldtio);
	//tcsetattr(STARBOARD_SERIAL,TCSANOW,&Starboard_oldtio);		

	return 0;
}
