#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>

#include <lcm/lcm.h>
#include "exlcm_batteryInfo_t.h"
#include "compass_compass_t.h"
#include "imu_imu_t.h"

#define PORT_D "/dev/ttyS1"

int RF_Port;

float portVoltage=0;
float starboardVoltage=0;
float portCurrent=0;
float starboardCurrent=0;

float gpsDesiredX=0;
float gpsDesiredY=0;
float gpsCurrentX=0;
float gpsCurrentY=0;

float headingDesired=0;
float headingCurrent=0;

//LCM handler for BATTERY channel
static void battery_handler(const lcm_recv_buf_t *rbuf, const char * channel, const exlcm_batteryInfo_t * msg, void * user)
{
	portVoltage = msg-> port_voltage;
	starboardVoltage = msg-> starboard_voltage;
	portCurrent = msg-> port_current;
	starboardCurrent = msg-> starboard_current;
}

static void compass_handler(const lcm_recv_buf_t *rbuf, const char * channel, const compass_compass_t * msg, void * user)
{
	headingCurrent = msg->heading;
}

static void imu_handler(const lcm_recv_buf_t *rbuf, const char * channel, const imu_imu_t * msg, void * user)
{
	gpsCurrentX = msg->lat;
	gpsCurrentY = msg->lon;
}

//TODO LCM message for state of the vehicle

int main(int argc, char** argv)
{
	//setup for LCM
 	lcm_t * battery= lcm_create("udpm://239.255.76.67:7667?ttl=1");
    if(!battery) 
    	return 1;
   
	lcm_t * compass = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!compass)
		return -1;
	
	lcm_t * imu = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!imu)
		return -1;

	//Subscribers
    exlcm_batteryInfo_t_subscribe(battery, "BATTERY", &battery_handler, NULL);
	compass_compass_t_subscribe(compass, "COMPASS", &compass_handler, NULL);
	imu_imu_t_subscribe(imu, "IMU", &imu_handler, NULL);
	    

	//beginning of RF serial port config
	char *devicename_rf = PORT_D;
	struct termios RF_oldtio, RF_newtio;	//place for old and new port settings for serial port	
	//open
	RF_Port = open(devicename_rf, O_RDWR | O_NOCTTY | O_NONBLOCK);
	
	tcgetattr(RF_Port,&RF_oldtio); // save current port settings 
	RF_newtio.c_cflag = B115200  | CS8 | CLOCAL | CREAD;
	RF_newtio.c_iflag = IGNPAR;
	RF_newtio.c_oflag = 0;
	RF_newtio.c_lflag = 0;
	RF_newtio.c_cc[VMIN]=1;
	RF_newtio.c_cc[VTIME]=0;
	tcflush(RF_Port, TCIFLUSH);
	tcsetattr(RF_Port,TCSANOW,&RF_newtio);	
	
	char theString[115];
	//sleep(1);

	//file for writing data
	FILE *f = fopen("/home/eclipse/Travis/commsRF/data.txt", "w");
	if (f == NULL)
	{
	    printf("Error opening file!\n");
	    exit(1);
	}
	
	while(1)
	{

		lcm_handle(compass);
		lcm_handle(imu);
		lcm_handle(battery);

		/*printf("Port Voltage:%.1f, Starboard Voltage:%.1f, Port Current:%.1f, Starboard Current:%.1f,"
			"Desired GPS X:%.4f, Desired GPS Y:%.4f, Current GPS X:%.4f, Current GPS Y:%.4f, Desired Heading:%.3f, " 
			"Current Heading:%.1f.\n\n\r", portVoltage, starboardVoltage, portCurrent,
			starboardCurrent, gpsDesiredX, gpsDesiredY, gpsCurrentX,
			gpsCurrentY, headingDesired, headingCurrent);*/

		//printf("%s\n", "Im here");
		sprintf(theString,"Port Voltage:%.1f, Starboard Voltage:%.1f, Current GPS X:%.4f, Current GPS Y:%.4f, " 
			"Current Heading:%.1f.\n\n\r", portVoltage, starboardVoltage, gpsCurrentX, gpsCurrentY, headingCurrent);

		/*sprintf(theString,"Port Voltage:%.1f, Starboard Voltage:%.1f, Current GPS X:%.4f, Current GPS Y:%.4f, Current"
			" Heading:%.1f.\n\n\r", portVoltage, starboardVoltage, gpsCurrentX, gpsCurrentY, headingCurrent);*/

		write(RF_Port, &theString, sizeof(theString));
		/* print some text */
		fprintf(f, "%s\n", theString);


		//sleep(1);
	}

    lcm_destroy(battery);
	//restore serial port to intial conditions
	tcsetattr(RF_Port,TCSANOW,&RF_oldtio);	
	fclose(f);
	return 0;
}
