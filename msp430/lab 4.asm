;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer
;-------------------------------------------------------------------------------
                                            ; Main loop here

            mov.b    #00000000b,&P1OUT
            mov.b    #11111111b,&P1DIR
            mov.b    #00000000b,&P2DIR

InfLoop     mov.b    &P2IN, R5

            bit.b    #BIT0, R5
            jnz      rotate

read        mov.b    R5, R6            ; copy switches value to R6
            and.b    #00111000b,R6     ; mask desired LED patterd from P2 pins 3-5
            mov.b    R6, &P1OUT        ; display the patern on P1
            jmp      InfLoop

rotate      bit.b    #BIT1, R5
            jnz      goLeft
            mov.b    R6, R8
            rrc.b    R8
            rrc.b    R6
            jmp      patout

goLeft      mov.b    R6, R8
            rlc.b    R8
            rlc.b    R6
            jmp      patout

patout      mov.b    R6, &P1OUT        ; display the roated pattern
            call     #Delay            ; call subroutine delay
            jmp      InfLoop

Delay       bit.b    #BIT2, R5
            jnz      fast
            mov.w    #90000, R7
            dec.w    R7
            jnz      dloop

fast        mov.w    #15000, R7
            dec.w    R7
            jnz      dloop

dloop       dec.w    R7
            jnz      dloop
            ret

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect     .stack
;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
