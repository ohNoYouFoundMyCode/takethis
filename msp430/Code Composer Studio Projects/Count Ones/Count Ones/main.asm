;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here

; Counts # of 1's in R5
; result goes into R5

Count_Ones:	clr.w	R6						; Sets R6 to zero

Loop:		tst.w	R5						; Sets VNZC for R5
			jeq		Done					; If Z=1 you're done
			rla.w	R5						; Puts MSB of R5 into Carry
											; and 0 into LSB
			jnc		Skip					; If no carry bit, skip counter
			inc.b	R6						; If carry bit, increment R6
Skip:		jmp		Loop					; Loop

Done:		mov.w	R6, R5					; When done, put R6 into R5
			ret								; Because subroutine
;-------------------------------------------------------------------------------

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
