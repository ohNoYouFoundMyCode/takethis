//***********************************************************************
//******** CDA3331 Intro to Micro class April 17, 2015
//******** Dr. Bassem Alhalabi and TA Pablo Pastran
//******** Skeleton Program for Lab 5
//******** Run this program as is to show you have correct hardware connections
//******** Explore the program and see the effect of Switches on pins P2.3-5
//******** Lab5 Grade --> Make the appropriate changes to build the desired effects of input switches

#include <msp430.h> 

//Digit configuration, make sure segments h-a are connected to PORT1 pins 7-0
int LEDS[] = {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;	        // Stop watchdog timer
    int switches=0x00;
    int leftdigit=0, rightdigit=0;
    int swdelay=0;
    int setter=0;
    int setterTwo=0;
    int enterLoopOne=0;
    int enterLoopTwo=0;
    int countSpeed;

    P1DIR = 0xff; 						// port 1 all output
    P2DIR = 0x03;						// port 2 all inputs

 for (;;)
 {
	 switches = P2IN;  					// read all switches values
     //This part of the main loop executes only once every 20 iteration
	 //this way we slow down the changes of the displayed numbers while we keep multiplexing of the display relatively faster
	 swdelay++;
     if (swdelay >= 20)
     {
    	 swdelay =0;

     	 //check switches for 000 --> zero values
	 	 if (((switches & BIT5) == BIT5) && ((switches & BIT4) == BIT4)&& ((switches & BIT3) == BIT3))
	 	 {countSpeed=0; setter=0; setterTwo=0; rightdigit=0; leftdigit=0;}

	 	 //check switches for 001 --> right digit count up
	 	 if (((switches & BIT5) == BIT5) && ((switches & BIT4) == BIT4)&& ((switches & BIT3) != BIT3))
	 	 {countSpeed=0; setter=0; setterTwo=0; rightdigit++; if (rightdigit >=10) {rightdigit=0;} }

	 	 //check switches for 010 --> left digit count up
	 	 if (((switches & BIT5) == BIT5) && ((switches & BIT4) != BIT4)&& ((switches & BIT3) == BIT3))
	 	 {countSpeed=0; setter=0; setterTwo=0; leftdigit++ ; if (leftdigit >=10) {leftdigit=0;} }

	 	 //check switches for 011 --> Values hold
	 	 if (((switches & BIT5) == BIT5) && ((switches & BIT4) != BIT4)&& ((switches & BIT3) != BIT3))
	 	 {countSpeed=0; setter=0;setterTwo=0;}

	 	 //check switches for 100 --> Counter cycles up from 00 to 99
	 	 //setter is used for initial set to zero
	 	 if (((switches & BIT5) != BIT5) && ((switches & BIT4) == BIT4)&& ((switches & BIT3) == BIT3))
	 	 {countSpeed=1; enterLoopOne=1;if(setter==0){rightdigit=0;leftdigit=0;setter=1;enterLoopOne=0;}
	 	 setterTwo=0; if(enterLoopOne==1){rightdigit++; if(rightdigit==10&&leftdigit==9){rightdigit=9;}else if(rightdigit>=10) {rightdigit=0; leftdigit++; if(leftdigit >=10) {leftdigit=0;}}}}

	 	 //check switches for 101 --> Counter cycles up from the preset value to 99
	 	 if (((switches & BIT5) != BIT5) && ((switches & BIT4) == BIT4)&& ((switches & BIT3) != BIT3))
	 	 {countSpeed=1; setter=0; setterTwo=0; rightdigit++; if(rightdigit==10&&leftdigit==9){rightdigit=9;}else if(rightdigit>=10) {rightdigit=0; leftdigit++; if(leftdigit >=10) {leftdigit=0;}}}

	 	 //check switches for 110 --> Counter cycles down from preset value to 00
	 	 if (((switches & BIT5) != BIT5) && ((switches & BIT4) != BIT4)&& ((switches & BIT3) == BIT3))
	 	 {countSpeed=1; setter=0; setterTwo=0; rightdigit--; if(rightdigit<=0&&leftdigit<=0){rightdigit=0;leftdigit=0;}else if(rightdigit<0) {leftdigit--;rightdigit=9;if(leftdigit<0){leftdigit=0;}}}

	 	 //check switches for 111 --> Counter cycles down from 99 to 00
	 	 if (((switches & BIT5) != BIT5) && ((switches & BIT4) != BIT4)&& ((switches & BIT3) != BIT3))
	 	 {countSpeed=1; enterLoopTwo=1;if(setterTwo==0){rightdigit=9;leftdigit=9;setterTwo=1;enterLoopTwo=0;}
	 	 setter=0; if(enterLoopTwo==1){rightdigit--; if(rightdigit<=0&&leftdigit<=0){rightdigit=0;leftdigit=0;}else if(rightdigit<0) {leftdigit--;rightdigit=9;if(leftdigit<0){leftdigit=0;}}}}
     }



     //this code is executed once at the end of the main loop
	 //it displays left and right digits alternatively, each one for 10 ms
	 //note that both digits must be turned off to avoid aliasing
	 //the two different cases are for fast count and slow count
     if(countSpeed==1)//display fast
     {
		 //Display the left digit
		 P2OUT= BIT1;
		 P1OUT= LEDS[leftdigit];  __delay_cycles (5000);
		 P1OUT= 0; P2OUT=0;       __delay_cycles (500);
		 //Display the right digit
		 P2OUT= BIT0;
		 P1OUT= LEDS[rightdigit]; __delay_cycles (5000);
		 P1OUT= 0; P2OUT=0;       __delay_cycles (500);
     }
     if(countSpeed==0)//display slow
     {
		 //Display the left digit
		 P2OUT= BIT1;
		 P1OUT= LEDS[leftdigit];  __delay_cycles (10000);
		 P1OUT= 0; P2OUT=0;       __delay_cycles (1000);
		 //Display the right digit
		 P2OUT= BIT0;
		 P1OUT= LEDS[rightdigit]; __delay_cycles (10000);
		 P1OUT= 0; P2OUT=0;       __delay_cycles (1000);
     }
 }
}
