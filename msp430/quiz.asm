;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            

;-------------------------------------------------------------------------------
LIST_IN            .set    0x0200                  ;Memory allocation      ARY1
LIST_OUT           .set    0x0240                  ;Memory allocation      ARY2
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
SETUP			clr		R4
				clr		R5
				clr		R6
				clr		R7
				clr		R8
				clr		R9
				mov.w	#LIST_IN, R4
				mov.w	#LIST_OUT, R5

listInSetup		mov.w   #10, 0(R4)            	;Memory allocation
                mov.w   #0x1, 2(R4)        	  	;Memory allocation
                mov.w   #0x2, 4(R4)           	;Memory allocation
                mov.w   #0x2, 6(R4)           	;Memory allocation
                mov.w   #0x4, 8(R4)           	;Memory allocation
                mov.w   #0x5, 10(R4)          	;Memory allocation
                mov.w   #0x6, 12(R4)          	;Memory allocation
                mov.w   #0x6, 14(R4)          	;Memory allocation
                mov.w   #0x8, 16(R4)          	;Memory allocation
                mov.w   #0x9, 18(R4)          	;Memory allocation
                mov.w   #0xA, 20(R4)          	;Memory allocation
                push	R4						;Saves R4
				mov.w	@R4, R6					;setup for counter

copyToListOut	mov.w	@R4, 0(R5)				;starts copy
				mov.w	R4, R7					;sets up first item to compare
				mov.w	R7, R8					;sets up second item to compare
				incd 	R4
				incd 	R8
				jmp		areTheyEqual			;go to compare
carryOn			incd	R5						;increments R5
				dec		R6						;decrements counter
				jnz		copyToListOut			;if the counter is not zero, copy another
				pop		R4						;Restores R4
				jmp		Mainloop				;exits from loop

areTheyEqual	cmp.w	@R7, 0(R8)				;checks if the values in the mem are equal
				jne		carryOn					;returns
				mov.w	R6, R9
remover			mov.w	R8, R7
				incd	R8
				incd	R7
				dec		R9
				jnz		remover
				jmp		carryOn


Mainloop		jmp		Mainloop


;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
