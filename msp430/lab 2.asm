;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
ARY1		.set	0x0200					;Memory allocation Array 1
ARY1S		.set	0x0210					;Memory allocation Array 1 sorted
ARY2		.set	0x0220					;Memory allocation Array 2
ARY2S		.set	0x0230					;Memory allocation Array 2 sorted
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
CLEAR		clr		R5
			clr		R6
			clr		R7
			clr		R8
			clr		R9
			clr		R10
			clr		R11
			clr		R12

SETUP		mov.w	#1, R5				;flag=1
			mov.w	@R4, R8				;setup for "end"
			cmp		#1, R5				;is flag==1
			jne		#ENDDO				;break loop
			cmp		#1, R8				;end>1
			jl		#ENDDO
			je		#ENDDO
			mov.w	#1, R9
			mov.w	#2, R10
			mov.w	#0, R5
			mov.w	1(R4), R11
			mov.w	2(R4), R12
			cmp		R7, R8
			jg		#ENDDO2
			cmp		@R11, 0(R12)
			jl		#ENDIF1
			je		#ENDIF1


ENDDO		jmp		Mainloop

ENDIF1		jmp		Mainloop

Mainloop	jmp		Mainloop				;Infinite loop

ArraySetup1	mov.b	#10, 0(R4)				;Setup for Array 1
			mov.b	#20, 1(R4)
			mov.b	#89, 2(R4)
			mov.b	#-5, 3(R4)
			mov.b	#13, 4(R4)
			mov.b	#63, 5(R4)
			mov.b	#-1, 6(R4)
			mov.b	#88, 7(R4)
			mov.b	#2, 8(R4)
			mov.b	#-88, 9(R4)
			mov.b	#1, 10(R4)
			ret

;-------------------------------------------------------------------------------

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
