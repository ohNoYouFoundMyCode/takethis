;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

CLEAR		clr		R4						;zeros the register
			clr		R5						;zeros the register
			clr		R6						;zeros the register
			clr		R7						;zeros the register
			clr		R9						;zeros the register
			clr		R10						;zeros the register

LAB2		mov.w	#-7, R4					;loads "a" into R4
			mov.w	#50, R10				;loads a value into R10
			mov.w	#0xFF, R9				;loads value for neg-pos conversion into R9

XCALC		call	#nFactorial
			add.w	R5, R5					;mulitplies by 2

FCALC		push 	R10						;preserves R10
			add.w	R5, R10					;numerator sum
			rra		R10						;divide by two
			rra		R10						;divide by two (totaling division by four)
			mov.w	R10, R7
			pop		R10						;restores R10

Mainloop	jmp		Mainloop

nFactorial:	tst.w	R4						;sets test bits for R4
			jz		zero					;tests zero case
			jn		neg						;tests negative case
			mov.w	R4, R5					;setup for factorial
facLoop:	dec.w	R4						;reduces contents of R4 by 1
			jz		end						;if zero it set, factorial is done
			call	#mult					;go to mulitply function
			jmp		facLoop					;loop back to factorial loop upon return from mult
zero:		mov.w	#1, R5					;0! = 1
			jmp		end						;you're done
neg:		sub.w	R4, R9					;conversion to positive
			add.b	#1, R9					;adds 1
			mov.w	R9, R4					;R4 is now positive
			jmp		nFactorial				;now compute
end:		ret

mult:		clr		R6						;setup
			clr		R7						;setup
			mov.w	R4, R6					;counter setup while preserving factorial counter
loop:		add.w	R5, R7					;begin multiplication
			dec.w	R6						;drop counter
			jnz		loop					;if counter is not zero, continue to loop
			mov.w	R7, R5					;move anser to R5
			ret								;back to facLoop




;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
