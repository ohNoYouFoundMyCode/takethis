//*************************************************************************************
//*************************************************************************************
//	D A T E  . C P P
//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: The state of the object (private data) has not been initialized
//Postcondition: The state has been initialized to the date 01/01/0001
//Description:  This is the default constructor which will be called automatically when 
//an object is declared.  It will initialize the state of the class.
//*************************************************************************************\

#include "date.h"

Date::Date()
{
	cout << "The default constructor has been called." << endl;
	myMonth = 01;
	myDay = 01;
	myYear = 0001;
	this->display();
}


//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: The state of the object has not been initialized
//Postcondition: The state of the object has been initiliazed to an explicited stated date
//Description: This function is the explicit-value constructor and will be called accordingly.
// The meat and potatoes of this member function is error checking, ensuring that the user
// has input a valid date.  The function will provide feedback if the date is correct
//or incorrect, and will even inform the user of a leap year entry.
//TODO: this function could be enhanced to ensure that only the 2nd month, February, will
//allow for a user to enter 28 or 29 days.
//*************************************************************************************
Date::Date(unsigned m, unsigned d, unsigned y)
{
	cout << "The explicit-value constructor has been called." << endl;
	bool error = false;
	if (m > 12 || m <= 0)
	{
		cout << "negative bucko, " << m << " is not a valid month." << endl;
		error = true;
	}
	if (d > 31)
	{
		cout << "not a shot, there is no month with " << d << " days in it." << endl;
		error = true;
	}
	if (y <= 0)
	{
		cout << "come on, how about a year in the past two millenium." << endl;
		error = true;
	}

	//This statement will allow all three possible errors to be checked prior to exit
	if (error)
	{
		cout << endl;
		return;
	}

	//Since the day, month, and year requirements have been met, the answer must be valid.
	//The only checking left is whether or not the day is in a leap year.  
	if (d == 29)
	{
		myMonth = m;
		myDay = d;
		myYear = y;
		cout << "Oh yeah! It's a leap year!" << endl;
		this->display();
	}
	else
	{
		myMonth = m;
		myDay = d;
		myYear = y;
		cout << "Nailed it!" << endl;
		this->display();
	}
	
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: N/A
//Postcondition: N/A 
//Description: This function does not change state information.  It is simply to show
//the current state info.
//*************************************************************************************
void Date::display()
{
	cout << myMonth << "/" << myDay << "/" << myYear << endl << endl;
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: N/A
//Postcondition: N/A 
//Description: This function does not change state information.  It simply returns the
//month.
//*************************************************************************************	
int Date::getMonth()
{
	return myMonth;
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: N/A
//Postcondition: N/A 
//Description: This function does not change state information.  It simply returns the
//day.
//*************************************************************************************
int Date::getDay()
{
	return myDay;
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: N/A
//Postcondition: N/A 
//Description: This function does not change state information.  It simply returns the
//year.
//*************************************************************************************
int Date::getYear()
{
	return myYear;
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: The month is set to some value.
//Postcondition: The month has been changed to a user defined value.
//Description:  A simple setter.
//*************************************************************************************
void Date::setMonth(unsigned m)
{
	myMonth = m;
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: The day is set to some value.
//Postcondition: The day has been changed to a user defined value.
//Description:  A simple setter.
//*************************************************************************************
void Date::setDay(unsigned d)
{
	myDay = d;
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: The year is set to some value.
//Postcondition: The year has been changed to a user defined value.
//Description:  A simple setter.
//*************************************************************************************
void Date::setYear(unsigned y)
{
	myYear = y;
}

//*************************************************************************************
//Name:Travis Moscicki  Date: 9/2/2015
//Precondition: N/A
//Postcondition: N/A
//Description:  This function showcases a friend function and operator overloading with
//chaining
//*************************************************************************************
ostream & operator<<(ostream & out, const Date & dateObj)
{
	out << dateObj.myMonth << "/" << dateObj.myDay << "/" << dateObj.myYear;
	return out;

}
