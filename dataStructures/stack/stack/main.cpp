#include "stack.h"

int main()
{
	stack S;
	string yesOrNo;
	string expression;
	int count = 0;
	do
	{
		if (count == 0)
		{
			cout << "Hello there.  Would you like to convert a postfix to infix expression?" << endl;
			cout << "Please enter yes or no." << endl;
			cin >> yesOrNo;
		}
		else
		{
			cout << "Hello again.  Would you like to convert another postfix to infix expression?" << endl;
			cout << "Please enter yes or no." << endl;
			cin >> yesOrNo;
		}
		if (yesOrNo == "yes")
		{
			cout << "please enter your expression." << endl;
			cin >> expression;
			S.input(expression);
			count++;
		}
	} while (yesOrNo != "no");
	return 0;
}

