/********************************************************************************************
Name: Travis Moscicki             Z#:23252751
Course: Date Structures and Algorithm Analysis (COP3530)
Professor: Dr. Lofton Bullard
Due Date: 10/15/2015			      Due Time: 11:30PM
Total Points: 100
Assignment 4: Postfix to Infix via Stack

Description: This program has been developed to showcase a stack for the purpose of converting
a postfix expression to an infix expression.  Multiple member functions are created to help with
organization and manipulation.
*********************************************************************************************/


#include <iostream>
#include <string>

using namespace std;

typedef string stack_element;

class stack_node
{
public:
	stack_element data;
	stack_node *next;
};

class stack
{
public:
	stack();
	~stack();
	stack(const stack &);
	void push(const stack_element &);
	stack_element pop();
	void input(const string &input);
	stack_element top();
	void print();
	bool errorCheck(const string &input);

private:
	stack_node *s_top;
};