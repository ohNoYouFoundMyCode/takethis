#include "stack.h"

//Default Constructor
//Precondition: State variables are uninitialized.
//Postcondition: top pointer initialized
stack::stack()
{ 
	cout << "Inside Default Constructor\n"; 
	s_top = 0; 
}

//Default Destructor
//Precondition: There is a stack.
//Postcondition: The stack is destroyed.
stack::~stack()
{
	cout << "Inside !stack \n";
	while (s_top != 0)
	{
		pop();
	}
}

//Copy Constructor
//Precondition: There is a stack in memory.
//Postcondition: There are two identical stacks in memory.
//Description: Copys one stack into another.
stack::stack(const stack & Org)
{
	cout << "Inside the Copy Constructor\n";
	stack_node *p = Org.s_top;

	(*this).s_top = 0;

	while (p != 0)
	{
		(*this).push(p->data);
		p = p->next;
	}
}

//Push
//Precondition: There is a stack.
//Postcondition: An item has been added to the stack.
void stack::push(const stack_element & item)
{
	cout << "Inside push \n";
	stack_node *p = new stack_node;

	p->data = item;
	p->next = s_top;
	s_top = p;
}

//Pop
//Precondition: There is a stack.
//Postcondition: The item on the top of the stack is removed.
stack_element stack::pop()
{
	cout << "Inside pop \n";
	stack_node *p;

	if (s_top != 0)
	{
		p = s_top;
		s_top = s_top->next;
		return p->data;
		delete p;
	}
	return 0;
}

//Input
//Precondition:
//Postcondition: 
//Description: This is the meat and potatoes of the program.  This takes in the user input string,
//pushes it to the stack, and converts the postfix expresion to infix along the way.  It does this by
//utilizing 2 holding bins for the top two items on the stack - operand1 and operand2.  Performs error
//checkingg with errorCheck fuction.
void stack::input(const string &input)
{
	string infix, operand1, operand2, expr;
	if (!this->errorCheck(input))
	{
		for (unsigned int i = 0; i < input.length(); i++)
		{
			string c = input.substr(i, 1);
			if (c == "+" || c == "-" || c == "*" || c == "/")
			{
				operand2 = this->pop();
				operand1 = this->pop();
				expr = "(" + operand1 + c + operand2 + ")";
				this->push(expr);
			}
			else
			{
				this->push(c);
			}
		}
		infix = this->pop();
		cout << "The infix expression is" << endl << infix << endl;
	}
}

//Top
//Precondition:N/A. 
//Postcondition: N/A.
//Description: This returns the information on the top of the stack.
stack_element stack::top()
{
	cout << "Inside top \n";

	if (s_top == 0)
	{
		exit(1);
	}
	else
	{
		return s_top->data;
	}
}

//Print
//Precondition: N/A.
//Postcondition: N/A.
//Description: Printer function.
void stack::print()
{
	cout << "Inside print \n";

	for (stack_node *p = s_top; p != 0; p = p->next)
	{
		cout << p->data << endl;
	}
}


//Performs checks as follows: There will be too many operands if the stack contains 1 more operand than
//the number of operators to be processed (before the next operand, if applicable).
//There will be to many operators if the number of operators+1 is greater than the number of operands.
bool stack::errorCheck(const string &input)
{
	string operand1, operand2, expr;
	int operandCount = 0, operatorCount = 0;
	stack *temp = new stack;

	//original stack is being corrupted in this operation

	for (unsigned int i = 0; i < input.length(); i++)
	{
		string c = input.substr(i, 1);
		if (c == "+" || c == "-" || c == "*" || c == "/")
		{
			operand2 = temp->pop();
			expr = "(" + operand1 + c + operand2 + ")";
			temp->push(expr);
			operatorCount++;
		}
		else
		{
			temp->push(c);
			operandCount++;
		}
	}
	if (operandCount > (operatorCount + 1))
	{
		cout << "There are too many operands." << endl;
		return true;
	}
	else if ((operatorCount+1)>operandCount)
	{
		cout << "There are too many operators." << endl;
		return true;
	}
	return false;
}