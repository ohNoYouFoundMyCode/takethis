/********************************************************************************************
Name: Travis Moscicki             Z#:23252751
Course: Date Structures and Algorithm Analysis (COP3530)
Professor: Dr. Lofton Bullard
Due Date: 10/21/2015			      Due Time: 11:30PM
Total Points: 100
Assignment 6: Doubly Linked Circular Queue

Description: This program has been developed to showcase a doubly linked circular queue.
This is a FIFO data structure that a user can navigate through in a circular fashion.
*********************************************************************************************/


#include "bqueue.h"

//Default Constructor
//Precondition: State variables are uninitialized.
//Postcondition: Front is initialized to NULL.
//Description: This function will initialize all state variables.
BQUEUE::BQUEUE()
{
	front=NULL;
}

//Default Destructor
//Precondition: There is a circular queue in memory.
//Postcondition: The circular queue has been removed from memory.
//Description: This function checks to see if there is a queue.  If so,
//items in the array are deleted until the stepping pointer returns to front.
BQUEUE::~BQUEUE()
{
	if (!front) return;
	bqnode *ptr = front->next;
	while (ptr != front)
	{
		bqnode *delteThis = ptr;
		ptr = ptr->next;
		delete delteThis;
	}
}

//Copy Constructor
//Precondition: There is one circular queue in memory.
//Postcondition: There are two circular queues in memory.
//Description: This function steps through the queue to be copied
//and makes a deep copy in the local queue.  When the end of the queue
//being copied is reached, the last item in the new queue is linked to 
//the first item.
BQUEUE::BQUEUE(const BQUEUE &toCopy)
{
	front = new bqnode;
	front->next = new bqnode;
	bqnode *tempOneLocal = front;
	bqnode *tempTwoLocal = front->next;
	bqnode *tempOneCopy = toCopy.front;
	bqnode *tempTwoCopy = toCopy.front->next;

	while (tempTwoCopy != toCopy.front)
	{
		tempOneLocal->time = tempOneCopy->time;
		tempTwoLocal->prev = tempOneLocal;
		tempOneLocal = tempTwoLocal;
		tempOneLocal->next = new bqnode;
		tempTwoLocal = tempOneLocal->next;
		tempOneCopy = tempOneCopy->next;
		tempTwoCopy = tempTwoCopy->next;
	}
	tempOneLocal->time = tempOneCopy->time;
	tempOneLocal->next = front;
	front->prev = tempOneLocal;


}

//Empty Checker
//Precondition: N/A.
//Postcondition: N/A.
//Description: Simply checks is there are values in the queue.
bool BQUEUE::Empty()
{ 
	return front == 0; 
}

//Enqueue
//Precondition: There is a circular queue.
//Postcondition: There is one more item in the circular queue.
//Description: This will add an item to the end of the current queue.
void BQUEUE::Enqueue(int newTime)
{
	if (!front)
	{
		front = new bqnode;
		front->time = newTime;
		front->next = front->prev = front;
	}
	else
	{
		bqnode *pnew = new bqnode;
		pnew->time = newTime;
		pnew->next = front;
		pnew->prev = front->prev;
		front->prev->next = pnew;
		front->prev = pnew;
	}
}

//Dequeue
//Precondition: There is a circular queue.
//Postcondition: There is one less item in the circular queue.
//Description: This function will remove the value in the front of the
//queue and then link the back to the front accordingly.
void BQUEUE::Dequeue()
{
	if (!front) return;
	if (front->next == front)
	{ 
		delete front; 
		front = NULL; 
		return; 
	}
	bqnode *deleteThis = front;
	front = front->next;
	front->prev = deleteThis->prev;
	deleteThis->prev->next = front;
	delete deleteThis;
}

//Printer
//Precondition: N/A.
//Postcondition: N/A.
//Description: This functions prints the contents in the queue
//by looking for when the front of the list is reached.
void BQUEUE::Print()
{
	if (!front) 
	{
		cout << "(empty)" << endl; 
		return; 
	}
	cout << front->time << " ";
	bqnode *ptr = front->next;
	while (ptr != front)
	{
		cout << ptr->time << " ";
		ptr = ptr->next;
	}
	cout << endl;
}
