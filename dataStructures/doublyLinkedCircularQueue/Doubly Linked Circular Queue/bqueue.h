#ifndef BQUEUE_H
#define BQUEUE_H

#include <iostream>

using namespace std;

class bqnode
{
public:
	int time;
	bqnode *prev, *next;
};

class BQUEUE
{
public:
	BQUEUE();
	~BQUEUE();
	BQUEUE(const BQUEUE &);
	bool Empty();
	void Enqueue(int);
	void Dequeue();
	void Print();
private:
	bqnode *front, *back;
};

#endif