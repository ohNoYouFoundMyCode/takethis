/********************************************************************************************
Name: Travis Moscicki             Z#:23252751
Course: Date Structures and Algorithm Analysis (COP3530)
Professor: Dr. Lofton Bullard
Due Date: 9/15/2015			      Due Time: 11:00PM
Total Points: 100
Assignment 3: Dynamic Array Program

Description: This program has been developed to showcase a dynamically allocated array for 
use in keeping a database of strings.  Multiple member functions are created to help with
organization and manipulation.
*********************************************************************************************/

#include "tlist.h"

//Default Constructor
//Precondition: State variables are uninitialized.
//Postcondition: A file called myData.txt has been read into the dynamically allocated array DB, the state variable
//count contains the number of items in the .txt file and capacity will of the order 2^n.
//Description: This function will initialize all state variables, check to see if the dynamically allocated array is 
//full, double the size of the array if full, read in items from the .txt file, and increment the state variable count.
TLIST::TLIST()
{
	cout << "Default Constructor Invoked." << endl;
	count = 0;
	capacity = 2;
	DB = new string[capacity];
	ifstream reader("myData.txt");
	string line;
	if (reader.is_open())
	{
		while ( getline (reader, line) )
		{ 
			this->Insert(line);
		}
		reader.close();
	}
	else
	{
		cout << "can't open that" << endl;
	}
}

//Copy Constructor
//Precondition: The state variables of the current object are uninitialized.
//Postcondition: The state variables count and capacity of the current object are the size of the passed object,
//deep copy of the passed dynamic array has been made in the current object.
//Description: This function will perform a deep copy of the passed TLIST object's dynamic array.
TLIST::TLIST(const TLIST  & copy)
{
	cout << "Copy Constructor Invoked." << endl;
	count = copy.count;
	capacity = copy.capacity;
	DB = new string[copy.capacity];
	for (int i = 0; i < count; i++)
	{
		DB[i] = copy.DB[i];
	}
	
}

//Destructor
//Precondition: There is a dynamic array in memory.
//Postcondition: The memory containing the dynamic array is free.
//Description: a standard destructor for dynamically allocated memory (dynamic array)
TLIST::~TLIST()
{
	cout << "Destructor invoked" << endl;
	delete[] DB;
}

//Empty Checker
//Precondition: N/A - state variables are unchanged.
//Postcondition: N/A - state variables are unchanged.
//Description: This function will return 1 if the array is empty and 0 if not empty.
bool TLIST::IsEmpty()
{
	cout << "IsEmpty Invoked" << endl;
	return count==0;
}

//Full Checker
//Precondition: N/A - state variables are unchanged.
//Postcondition: N/A - state variables are unchanged.
//Description: Count must be compared to capacity-1 because of the 0 index.  Function will
//return 1 if the array is full and 0 otherwise
bool TLIST::IsFull()
{
	cout << "IsFull Invoked" << endl;
	return count==capacity-1;
}

//Search
//Precondition: N/A - state variables are unchanged.
//Postcondition: N/A - state variables are unchanged.
//Description: This function scans the dynamic array of the current object for a passed string
//and will return the index if the key is found or -1 if it is not.

//Update to assignment from handed in copy - brakets added to if statement fixes bug where
//items are not appropriately removed from the array.
int TLIST::Search(const string & key)
{
	cout << "Search Invoked" << endl;
	for (int i = 0; i < count; i++)
	{
		if (this->DB[i] == key)
		{
			cout << "item found" << endl;
			return i;
		}
	}
	cout << "item not found" << endl;
	return -1;
}

//Inserter
//Precondition: A TLIST object has been initialized.
//Postcondition: There is 1 additional string in the current object's dynamic array and count
//has been incremented by 1.
//Description: This function will insert the passed string into the current object's array,
//doubling size if necessary.
void TLIST::Insert(const string & key)
{
	cout << "Insert Invoked" << endl;
	if (this->IsFull())
	{
		this->DoubleSize();
	}
	this->DB[count] = key;
	count++;
	this->sort();
}

//Instertion Sorter
//Precondition: The current object's dynamic array is unsorted.
//Postcondition: The current object's dynamic array is sorted.
//Description: This function will sort the strings inside of the current
//object's dynamic array by introduction of a temp string valueholder and
//a temp int placeholder.
//////////////////*******************************************************************
void TLIST::sort()
{
	int j;
	string temp;
	for (int i = 1; i < count; i++)
	{
		j = i;
		while (j>0 && this->DB[j] < this->DB[j - 1])
		{
			temp = this->DB[j];
			this->DB[j] = this->DB[j-1];
			this->DB[j - 1] = temp;
			j--;
		}
	}
}
//Remover
//Precondition: A TLIST object has been initialized.
//Postcondition: There is 1 less string in the current object's array and count has been
//decremented by 1.
//Description: This function will search for the key, shift strings from index+1 (index of key) down 1,
//and then decrease the size of the array by 1.
void TLIST::Remove(const string & key)
{
	cout << "Remove Invoked" << endl;
	cout << "attempting to remove " << key << " from the array." << endl;
	int temp = this->Search(key);
	if (temp >= 0)
	{
		for (temp; temp < count; temp++)
		{
			DB[temp] = DB[temp + 1];
		}
		count--;
		cout << key << " has been removed." << endl;
	}

}

//Counter
//Precondition: N/A - state variables are not changed.
//Postcondition: N/A - state variables are not changed. 
//Description: This program creates a new dynmically allocated array from the string
//at the user defined location of the current object's dynamic array.  This new array
//will hold all substrings that are the size of substring.  Finally, the program will
//simply compare each index of this new array to the substring, incrementing a counter
//upon positive hits.
int TLIST::SubstringCount(const int loc, const string & substring)
{
	string *temp = new string[DB[loc].size() - (substring.size()-1)];
	int counter = 0;
	for (unsigned int i = 0; i < DB[loc].size() - (substring.size() - 1); i++)
	{
		for (unsigned int t = 0; t < substring.size(); t++)
		{
			temp[i] += this->DB[loc][i+t]; 
		}
		if (temp[i] == substring)
		{
			counter++;
		}
	}
	delete[] temp;
	return counter;
}

//Remover
//Precondition: There is a TLIST object.
//Postcondition: The string at a user defined index of the current object's 
//dynamic array will have all instances of substring removed.
//Description: Ths reuses the code from the function SubstringCounter to
//make a temporary dynamic array.  All instances of the substring are found,
//and then removed using the substr member function of the string class.
void TLIST::SubstringRemove(const int loc, const string & substring)
{
	string *temp = new string[DB[loc].size() - (substring.size() - 1)];
	int counter = 0;
	for (unsigned int i = 0; i < DB[loc].size() - (substring.size() - 1); i++)
	{
		for (unsigned int t = 0; t < substring.size(); t++)
		{
			temp[i] += this->DB[loc][i + t];
		}
		if (temp[i] == substring)
		{
			DB[loc] = DB[loc].substr(0, i) + DB[loc].substr(i + substring.size());
		}
	}
	this->sort();
	delete[] temp;
}

//Substring Inserter
//Precondition: There is a TLIST object.
//Postcondition: A single string in the current object's dynamic array has had a
//substring added to it at a certain position.
//Description: This function takes in a user input for index in the dynamic array, 
//position in that index's string, and substring.  It then copies all characters after
//and including the user defined position into a temp string, resizes the original 
//string to be only the characters before the user definted position, and concatenates 
//the new string to the resized original string.
void TLIST::SubstringInsert(const int loc, const int pos, const string & substring)
{
	string temp = substring;
	unsigned int posIterator = pos;
	for (posIterator; posIterator < this->DB[loc].size(); posIterator++)
	{
		temp += this->DB[loc][posIterator];
	}
	DB[loc].resize(pos);
	DB[loc] += temp;
	this->sort();
}

//Doubler
//Precondition: A TLIST object has been initialized.
//Postcondition: The capacity of the current object has been doubled.
//Description: This function will double the size of the current object's array by
//doubling the value of capacity, creating a new dynamic array of this size,
//copying the current array into the new array, freeing the current array from
//memory, and then assigning the new array to the current object's array.
void TLIST::DoubleSize()
{
	this->capacity *= 2;
	string *temp = new string[capacity];
	for (int i = 0; i < count; i++)
	{
		temp[i] = DB[i];
	}
	delete[] DB;
	DB = temp;
}

//Insertion operator overloading
//Precondition: N/A - state variables are unchanged.
//Postcondition: N/A - state variables are unchanged.
//Description: This overloads the insertion operator with chaining
//for use with a TLIST object, printing out all stored strings.
ostream & operator<<(ostream & out, const TLIST & Org)
{
	if (Org.count > 0)
	{
		for (int i = 0; i < Org.count; i++)
		{
			out << Org.DB[i] << endl;
		}
	}
	else
		out << "Sorry this list is empty!";
	return out;
}
