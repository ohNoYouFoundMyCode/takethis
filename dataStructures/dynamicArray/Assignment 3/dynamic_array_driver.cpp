#include "tlist.h"

int main()
{
	bool printStatements;
	cout << "Welcome to assignment 3.  Please enter your choice." << endl;
	cout << "[0]--only print array at end of program." << endl;
	cout << "[1]--print array after every change." << endl;
	cin >> printStatements;
	TLIST myList;
	if (printStatements)
	{
		//cout << myList << endl;
	}
	TLIST someList = myList;
	if (printStatements)
	{
		cout << someList << endl;
	}

	string tester = "ohyeah";
	myList.Insert(tester); 

	cout << myList.IsEmpty() << endl;
	cout << myList.IsFull() << endl;


	if (printStatements)
	{
		cout << myList << endl;
	}
	cout << myList.Search("ohyeah") << endl;
	myList.Remove("stringtrist");
	if (printStatements)
	{
		cout << myList << endl;
	}

	myList.SubstringInsert(2, 4, "xxxxx");
	if (printStatements)
	{
		cout << myList << endl;
	}
	
	myList.SubstringRemove(2, "zz");
	if (printStatements)
	{
		cout << myList << endl;
	}
	
	cout << myList.SubstringCount(2, "zz") << endl;
	cout << myList << endl;
	return 0;
}