//Travis Moscicki
//C for E
//Homework 4
#include <iostream>
#include <cmath>
#include <math.h>

using namespace std;

int main()
{
    //Part a
    double x;
    cout << "Hello!  Today I would like to compute the value of sin(x)" << endl;
    cout << "for you using factorials, but without using the factorial program." << endl;
    cout << "Please enter a value for x in radians:";
    cin >> x;
    //declares variables, initializing some
    double signchanger = -1, factorial, funcx = 0, n, fac;
    //a loop that runs from 1 to 11 in increments of 2
    for (n = 1; n <= 11; n = n + 2)
    {
        //signchanger makes it so the sign alternates
        signchanger = signchanger*-1;
        factorial = 1;
        //this for loop performs the factorial function by means of a countdown
        for (fac = n; fac >= 1; fac = fac - 1)
        {
            factorial = factorial*fac;
        }
        //sumation function
        funcx = funcx + (signchanger*pow(x, n) / factorial);
    }
    cout << "The value of sin(" << x << ") is = " << funcx << "." << endl;


    //Part b
    double numberofterms = 1;
    for (n = 1; n <= numberofterms; n = n + 1)
    {
        signchanger = signchanger*-1;
        factorial = 1;
        for (fac = n; fac >= 1; fac = fac - 1)
        {
            factorial = factorial*fac;
        }
        funcx = funcx + (signchanger*pow(x, n) / factorial);
    }
    //computes how far away my answer was from the accepted answer
    double difference = abs(funcx - (sin(x)));
    if (difference>.01)
    {
        numberofterms = numberofterms + 1;
    }


    cout << "The series needed " << numberofterms << " terms to ensure a result within .01" << endl;
    cout << "of the accepted value for sin(" << x << ")." << endl;
    return 0;
}
