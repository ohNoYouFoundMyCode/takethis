//Travis Moscicki
//Homework 3

#include <iostream>
#include <cmath>
using namespace std;

int main()
{ //Part 2
    //This program will compute factorials from 1 to 20
    long long int number, factorial, countdown;
    for (int number=0; number<=20; number++)
    {
            factorial=1;
            for (countdown=number; countdown>=1; countdown--)
            {
                factorial=factorial*countdown;
            }

            cout << "Number     Factorial" << endl;
            cout << number << "          " << factorial <<endl;
    }
  return 0;
}


