//Homework 4 Part 1
//Travis Moscicki
//C for Engineers
#include <iostream>
#include <cmath>
#include <math.h>

using namespace std;

int main()
{
    //defines variables, setting the initial minimum high
    double minimum = 50, func, t;
    //initiazes a for loop at -10, increases it to 10 in increments of .1
    for (t = -10; t <= 10; t = t + .1)
    {
        //defines a function
        func = 2 * pow(t, 2) - 6 * t + 10;
        if (func<minimum)
        {
            minimum = func;
        }
    }
    cout << "The minimum value of the function is " << minimum << " and occurs at t=" << t << "." << endl;

    return 0;
}
