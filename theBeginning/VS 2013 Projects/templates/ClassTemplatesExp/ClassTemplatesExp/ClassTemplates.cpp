#include <iostream>
#include <string>
using namespace std;

const int SIZE = 5;

/*********************************************************************/
// Class declaraton for Array_Class
/*********************************************************************/

template <class New_Type>
class Array_Class
{
public:
	Array_Class();
	~Array_Class();
	void Add(New_Type item);
	int Search(New_Type item);
	void Print();
	bool Is_Empty();
	bool Is_Full();
	void Remove(New_Type item);
private:
	New_Type *A;
	int count;
};

/*********************************************************************/
// Class definitions for the member function of Array_Class
/*********************************************************************/

template <class New_Type>
Array_Class<New_Type>::Array_Class()
{
	cout << "You are inside the default constructor.\n";
	cout << "New_Type has a size of " << sizeof(New_Type) << " bytes\n\n";
	count = 0;
	A = new New_Type[SIZE];
}

template <class New_Type>
Array_Class<New_Type>::~Array_Class()
{
	cout << "The Destructor has been called.\n\n";
	delete[] A;
	count = 0;
	A = 0;
}

template <class New_Type>
void Array_Class<New_Type>::Add(New_Type item)
{
	if (count<SIZE)
	{
		A[count++] = item;
	}
	else
	{
		cout << "The array is full.\n";
	}
}


template <class New_Type>
int Array_Class<New_Type>::Search(New_Type item)
{
	int i;

	for (i = 0; i<count; i++)
	{
		if (item == A[i])
		{
			return i;
		}
	}
	return -1;
}

template <class New_Type>
void Array_Class<New_Type>::Print()
{
	int i;

	for (i = 0; i<count; i++)
	{
		cout << "A[" << i << "] = " << A[i] << endl;
	}
}

 template<class New_Type>
bool Array_Class<New_Type>::Is_Empty()
{
	if (sizeof(Array_Class) == 0)
	{
		return true;
	}
	else
		return false;
}

template<class New_Type>
bool Array_Class<New_Type>::Is_Full ()
{
	if (count == SIZE)
	{
		return true;
	}
	else
		return false;
}

 template<class New_Type>
 void Array_Class<New_Type>::Remove(New_Type item)
 {
	 int i = Array_Class::Search(item);
	 cout << i << endl;
	 if (i > -1)
	 {
		 for (i; i < SIZE; i++)
		 {
			 A[i] = A[i + 1];
			 cout << i << endl;
		 }
		 count--;
	 }
	 else
	 {
		 cout << "sorry, that doesn't exist." << endl;
	 }
 }

int main()
{
	Array_Class<string>(my_string);
	Array_Class<int>(my_Ints);
	Array_Class<char>(my_Chars);
	my_string.Add("Hello");
	my_string.Add("Goodbye");
	my_string.Add("ComeHere");
	my_string.Add("SayNo");
	my_string.Add("SayYes");
	my_Ints.Add(1);
	my_Ints.Add(2);
	my_Ints.Add(3);
	my_Ints.Add(4);
	my_Ints.Add(5);
	my_Chars.Add('a');
	my_Chars.Add('b');
	my_Chars.Add('c');
	my_Chars.Add('d');
	my_string.Print();
	my_Ints.Print();
	my_Chars.Print();
	cout << my_string.Search("SayYes") << endl;
	cout << my_string.Search("No") << endl;
	cout << my_string.Is_Empty() << endl;
	cout << my_string.Is_Full() << endl;
	my_Ints.Remove(3);
	my_Ints.Print();
	return 0;
}
