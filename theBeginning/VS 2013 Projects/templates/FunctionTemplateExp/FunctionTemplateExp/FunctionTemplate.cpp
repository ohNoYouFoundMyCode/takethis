#include <iostream>
#include <string>

using namespace std;

template <typename a_type, typename b_type>
void Tester4(a_type &x, b_type &y)
{
	a_type temp = x;
	x = y;
	y = temp;
}


void swapValues(int &a, int &b)
{
	int temp = a;
	a = b;
	b = temp;
}

void swapValues(string &a, string &b)
{
	string temp = a;
	a = b;
	b = temp;
}

void swapValues(char &a, char&b)
{
	char temp = a;
	a = b;
	b = temp;
}

int main()
{
	string x = "first", y = "second";
	int m = 10, n = 20;
	char q = 'Q', r = 'R';

	cout << "x before = " << x << " and y before = " << y << endl;
	Tester4(x, y);
	cout << "x after = " << x << " and y after = " << y << endl;
	cout << endl;

	cout << "m before = " << m << " and n before = " << n << endl;
	Tester4(m, n);
	cout << "m after = " << m << " and n after = " << n << endl;
	cout << endl;

	cout << "q before = " << q << " and r before = " << r << endl;
	Tester4(q, r);
	cout << "q after called = " << q << " and r after = " << r << endl;
	cout << endl;

	return 0;
}

