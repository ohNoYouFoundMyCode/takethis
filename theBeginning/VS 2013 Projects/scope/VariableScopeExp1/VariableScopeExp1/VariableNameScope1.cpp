// VariableNameScope1.cpp
//
//#include <iostream>
//using namespace std;
//
//void Function_One();
//void Function_Two();
//void Function_Three(int);
//
//int i = 100; // I am global!
//
//void Function_One()
//{
//	cout << "i in Function_One = " << i << endl;
//}
//
//void Function_Two()
//{
//	int i = 555;
//	cout << "i in Function_Two = " << i << endl;
//}
//
//void Function_Three(int i)
//{
//	cout << "i in Function_Three = " << i << endl;
//}
//
//int main()
//{
//	int i = 777; // I am local!
//
//	Function_Three(888);
//	Function_Two();
//	Function_One();
//
//	cout << "i in main = " << i << endl;
//
//	return 0;
//}

// VariableNameScope2.cpp

#include <iostream>

using namespace std;

int i = 111;

int main()
{
	{
		int i = 222;
		{
			int i = 333;
			cout << "i = " << i << endl;
			{
				int i = 444;
				cout << "i = " << i << endl;
				{
					cout << "i = " << i << endl;
				}
			}
		}

		cout << "i = " << i << endl;
	}

	cout << "i = " << i << endl;

	return 0;
}
