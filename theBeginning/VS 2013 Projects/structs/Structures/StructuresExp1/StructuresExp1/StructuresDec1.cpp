// StructuresDec1.cpp

#include <iostream>
#include <string>

using namespace std;


int main()
{

	struct student_record
	{
		string firstname, lastname;
		double age, income;
		int number_of_children;
		char sex;
	};


	student_record Mary; // declare a structure of type student_record

	// populate the fields in the structure
	cout << "Enter a first name and last name (separated by a space): ";
	cin >> Mary.firstname;
	cin >> Mary.lastname;

	cout << "Enter age: ";
	cin >> Mary.age;

	cout << "Enter income: ";
	cin >> Mary.income;

	cout << "Enter number of children: ";
	cin >> Mary.number_of_children;

	cout << "Enter sex: ";
	cin >> Mary.sex;

	// access/display the populated fields of the structure
	cout << Mary.firstname << "    " << Mary.lastname << endl;
	cout << Mary.age << endl;
	cout << Mary.income << endl;
	cout << Mary.number_of_children << endl;
	cout << Mary.sex << endl;

	return 0;
}
