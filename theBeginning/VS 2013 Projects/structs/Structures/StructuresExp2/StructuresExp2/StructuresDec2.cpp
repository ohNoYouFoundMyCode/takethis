// StructuresDec2.cpp

#include <iostream>
#include <string>

using namespace std;

struct student_record
{
	string firstname, lastname;
	double age, income;
	int number_of_children;
	char sex;
};

int main()
{
	student_record Mary;  // create a structure of type student_record
	student_record Susan; // create another structure of type student_record

	// populate the fields in the first structure (Mary)
	cout << "Entering fields for Mary...\n" << endl;
	cout << "Enter a first name and a lastname (separated by a space): ";
	cin >> Mary.firstname;
	cin >> Mary.lastname;

	cout << "Enter age: ";
	cin >> Mary.age;

	cout << "Enter income: ";
	cin >> Mary.income;

	cout << "Enter number of children: ";
	cin >> Mary.number_of_children;

	cout << "Enter sex: ";
	cin >> Mary.sex;

	cout << "\nAssigning Susan to be equal to Mary." << endl;
	Susan = Mary; // assign one structure (Susan) to be equal to another (Mary)

	// display the new structure fields (Susan)
	cout << "\nDisplaying fields for Susan...\n" << endl;
	cout << Susan.firstname << "    " << Susan.lastname << endl;
	cout << Susan.age << endl;
	cout << Susan.income << endl;
	cout << Susan.number_of_children << endl;
	cout << Susan.sex << endl;

	return 0;
}
