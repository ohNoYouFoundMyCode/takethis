// StructureLogOps.cpp

#include <iostream>
#include <string>

using namespace std;

struct student_record
{
	string firstname, lastname;
	double age, income;
	int number_of_children;
	char sex;
};

int main()
{
	student_record Mary; // declare a structure of type student_record
	student_record Susan; // declare another structure of type student_record

	// populate the first structure (Mary)
	cout << "Populating the fields in the structure called Mary..." << endl;
	cout << "\nEnter the firstname and lastname: ";
	cin >> Mary.firstname;
	cin >> Mary.lastname;

	cout << "Enter age: ";
	cin >> Mary.age;

	cout << "Enter income: ";
	cin >> Mary.income;

	cout << "Enter number of children: ";
	cin >> Mary.number_of_children;

	cout << "Enter sex: ";
	cin >> Mary.sex;

	cout << "Assign the value of Mary to another structure called Susan." << endl;
	Susan = Mary;

	cout << "\nCompare the structure Mary to the structure Susan" << endl;
	if (Susan.firstname <= Mary.firstname)
	{
		cout << Susan.firstname << "    " << Mary.lastname << endl;
		cout << Susan.age << endl;
		cout << Susan.income << endl;
		cout << Susan.number_of_children << endl;
		cout << Susan.sex << endl;
	}

	return 0;
}
