// IntroClasses2.cpp

#include <iostream>

using namespace std;

class Bank_Acct
{
public:
	Bank_Acct();  // default constructor
	Bank_Acct(double);
	double Check_Balance();
	void Deposit(double);
	void Withdrawal(double);

private:
	double balance;
};

Bank_Acct::Bank_Acct()
{
	balance = 0;
}

Bank_Acct::Bank_Acct(double amount)
{

	balance = amount;
}

double Bank_Acct::Check_Balance()
{
	return balance;
}

void Bank_Acct::Deposit(double amount)
{
	balance = balance + amount;
}

void Bank_Acct::Withdrawal(double amount)
{
	balance = balance - amount;
}

int main()
{
	Bank_Acct my_Acct;
	Bank_Acct your_Acct(10340.85);

	// initial account balance for your account
	cout << "Your account balance = " << your_Acct.Check_Balance() << endl;

	your_Acct.Deposit(512.30); // deposit into your account
	cout << "Your account balance = " << your_Acct.Check_Balance() << endl;

	your_Acct.Withdrawal(8284.56); // withdraw from your account
	cout << "Your account balance = " << your_Acct.Check_Balance() << endl;

	// initial account balance for my account
	cout << "My account balance = " << my_Acct.Check_Balance() << endl;

	my_Acct.Deposit(2516.83); // deposit into my account
	cout << "My account balance = " << my_Acct.Check_Balance() << endl;

	my_Acct.Withdrawal(25.96); // withdraw from my account
	cout << "My account balance = " << my_Acct.Check_Balance() << endl;

	return 0;
}
