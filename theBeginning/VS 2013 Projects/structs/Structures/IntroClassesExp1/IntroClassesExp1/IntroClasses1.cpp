// IntroClasses1.cpp
#include <iostream>
using namespace std;

class Bank_Acct
{
public:
	Bank_Acct();  // initialize the state
	double Check_Balance(); // return the dollar amount of balance
	void Deposit(double); // increase balance by a dollar amount
	void Withdrawal(double); // decrease balance by a dollar amount

private:
	double balance;
};


Bank_Acct::Bank_Acct()
{
	balance = 0;
}

double Bank_Acct::Check_Balance()
{
	return balance;
}

void Bank_Acct::Deposit(double amount)
{
	balance = balance + amount;
}

void Bank_Acct::Withdrawal(double amount)
{
	balance = balance - amount;
}

int main()
{
	Bank_Acct my_Acct;
	cout << "My account balance = " << my_Acct.Check_Balance() << endl;

	my_Acct.Deposit(2516.83); // deposit some money into the account
	cout << "My account balance = " << my_Acct.Check_Balance() << endl;

	my_Acct.Withdrawal(25.96); // withdraw soome money from the account
	cout << "My Account Balance = " << my_Acct.Check_Balance() << endl;

	return 0;
}
