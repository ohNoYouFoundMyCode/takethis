#include <iostream>

using namespace std;

int Other_Fibonacci(int n);

void main(void)

{

	cout << Other_Fibonacci(6) << endl;

}


int Other_Fibonacci(int n)

{

	if (n < 0)

		return 0;

	if (n == 0 || n == 1)

	{

		return 1;

	}

	return Other_Fibonacci(n - 1) + Other_Fibonacci(n - 2);

}