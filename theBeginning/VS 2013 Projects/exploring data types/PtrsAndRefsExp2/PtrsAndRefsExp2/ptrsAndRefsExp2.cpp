// ptrsAndrefs2.cpp

#include <iostream>

using namespace std;

int main()
{
	int i; // new integer
	int *p = 0; // a pointer initialized to null 
	int &temp=i; // a reference to the location for the variable i
	
	cout << &i << endl; // display the address of data for the variable i
	cout << p << endl; // display the address that pointer p is pointing to
	cout << &temp << endl << endl; // display the address the reference is pointing to

	i=90; // assign i to a value
	p = &i; // assign p to point to the address where the data in i is

	cout << i << endl; // display the value in i
	cout << (*p) << endl; // display the value in the location pointed to by p
	cout << temp << endl; // display the value in the location that the reference points to

	return 0;
}