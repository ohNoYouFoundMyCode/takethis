// SimpleTypesDec2.cpp

#include <iostream>
using namespace std;

int main()
{
	bool response = 0;
	char character = 'A';
	int integer = 123.456789;
	float single_precision_number = 1234.56789;
	double double_precision_number = 1234.56789;

	cout << "response =  " << response <<endl;
	cout << "character =  " << character <<endl;
	cout << "integer =  " << integer <<endl;
	cout << "single_precision_number =  " << single_precision_number << endl;
	cout << "double_precision_number =  " << double_precision_number << endl;

	return 0;
}