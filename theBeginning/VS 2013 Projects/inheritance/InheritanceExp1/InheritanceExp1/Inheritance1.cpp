// Inheritance1.cpp

#include <string>
#include <iostream>

using namespace std;

class Person
{
protected:
	string firstName;
	string lastName;

public:
	Person(void)
	{
		cout << "In the Person constructor" << endl;
		cout << "Enter a first name: ";
		cin >> firstName;
		cout << "Enter a last name: ";
		cin >> lastName;
	}

	string getFirstName(void)
	{
		return firstName;
	}

	string getLastName(void)
	{
		return lastName;
	}
};

class Employee : public Person
{
protected:
	float salary;

public:
	Employee()
	{
		cout << "In the Employee constructor (a derived class)" << endl;
		cout << "Enter a salary (no commas): ";
		cin >> salary;
	}

	float getSalary(void)
	{
		return salary;
	}
};


int main(void)
{
	Employee Number_one; // calls constructor of Person, then constructor of Employee
	Employee Number_two; // calls constructor of Person, then constructor of Employee

	cout << endl << endl << endl;

	cout << "Retrieving Number_one's first name and last name from class Person:\n";
	cout << "   " << Number_one.getFirstName() << " " << Number_one.getLastName() << endl;
	cout << "\nRetrieving Number_one's salary from class Employee:\n";
	cout << "   " << Number_one.getSalary() << endl;

	cout << endl << endl << endl;

	cout << "Retrieving Number_two's first name and last name from class Person:\n";
	cout << "   " << Number_two.getFirstName() << " " << Number_two.getLastName() << endl;
	cout << "\nRetrieving Number_two's salary from class Employee:\n";
	cout << "   " << Number_two.getSalary() << endl;

	system("PAUSE");
	return 0;
}
