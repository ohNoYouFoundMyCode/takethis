// Inheritance2.cpp

#include <string>
#include <iostream>

using namespace std;

class Person
{
protected:
	string firstName;
	string lastName;

public:
	Person(void)
	{
		cout << "In constructor for Person." << endl;
		cout << "Enter a first name: ";
		cin >> firstName;
		cout << "Enter a last name: ";
		cin >> lastName;
	}

	string getFirstName(void)
	{
		return firstName;
	}

	string getLastName(void)
	{
		return lastName;
	}

	virtual void CalculateAndPrintPayrollInformation(void)
	{
		cout << "This is payroll information for a Base object:" << endl;
		cout << "Gross Pay is 0" << endl;
		cout << "Income Tax is 0" << endl;
		cout << "Net Pay is 0" << endl << endl;
	}
};

class Employee : public Person
{
protected:
	float annual_salary;
	double gross_pay, hours_worked, hourly_rate, income_tax, net_pay;

public:
	Employee()
	{
		cout << "In constructor for Employee (a derived class)." << endl;
		cout << "Enter an annual salary (no commas): ";
		cin >> annual_salary;
	}

	float getSalary(void)
	{
		return annual_salary;
	}

	void CalculateAndPrintPayrollInformation(void)
	{
		
		//Person::CalculateAndPrintPayrollInformation();

		cout << "In constructor for Taxpayer (a derived class of Employee)." << endl;
		cout << "Enter hours worked: ";
		cin >> hours_worked;
		cout << "Enter hourly rate: ";
		cin >> hourly_rate;

		gross_pay = hours_worked * hourly_rate;
		income_tax = gross_pay * 0.25;
		net_pay = gross_pay - income_tax;

		cout << "Gross pay = " << gross_pay << endl;
		cout << "Income tax  = " << income_tax << endl;
		cout << "Net Pay = " << net_pay << endl;
	}

};

int main(void)
{
	cout << "Creating John, who is of type Person:" << endl;
	Employee John;
	cout << "\nCreating Mary, who is of type Employee:" << endl;
	Employee Mary;



	cout << endl << endl << endl;

	cout << "\nGetting John's information (Person):\n";
	cout << endl << John.getFirstName() << " " << John.getLastName() << endl;
	John.CalculateAndPrintPayrollInformation();

	cout << endl << endl << endl;

	cout << "Getting Mary's information (Employee):\n";
	cout << Mary.getFirstName() << " " << Mary.getLastName() << ", whose salary is "
		<< Mary.getSalary() << endl << endl;

	cout << "\nIf Mary were an hourly employee, let's calculate her payroll information:\n";
	Mary.CalculateAndPrintPayrollInformation();

	system("PAUSE");
	return 0;
}
