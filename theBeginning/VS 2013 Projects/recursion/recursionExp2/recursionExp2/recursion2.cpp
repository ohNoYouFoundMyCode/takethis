#include <iostream>
using namespace std;

int R_power(int count, const int & base)
{
	if (count == 0)
		return 1;
	else
		return base * R_power(count - 1, base);
}

int I_power(int count, const int & base)
{
	int multiend = 1;

	while (count > 0)
	{
		multiend *= base;
		count--;
	}
	return multiend;
}

int main()
{
	int count = 10;
	int base = 2;

	cout << R_power(count, base) << endl;

	cout << I_power(count, base) << endl;

	return 0;
}
