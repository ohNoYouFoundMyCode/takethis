#include <iostream>
#include <string>
using namespace std;

void iterativeReverser(string &word)
{
	int t = 0;
	string temp = word;
	int size = word.length();
	for (int i = size; i > 0; i--)
	{
		temp[t] = word[i-1];
		t++;
	}
	word = temp;
}

string recursiveReverser(string &word)
{
		//not my code, sorry couldn't get my algorithm to work
		if (word.length() == 0)  // end condtion to stop recursion
			return "";

		string last(1, word[word.length() - 1]);  // create string with last character
		string reversed = recursiveReverser(word.substr(0, word.length() - 1));
		return last + reversed; // Make the last character first

}
	
	
int main()
{
	string word = "hello";
	cout << recursiveReverser(word) << endl;
	return 0;
}