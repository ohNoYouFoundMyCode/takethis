#include <iostream>
using namespace std;

class LIST_NODE
{
public:
	int data; // data element of node
	LIST_NODE *next; // pointer element of node
};

class LINKED_LIST_CLASS
{
public:
	LINKED_LIST_CLASS(); // default constructor
	LINKED_LIST_CLASS(LINKED_LIST_CLASS &); // copy constructor
	~LINKED_LIST_CLASS(); // destructor
	void Add(int); // mutator
	void Print(); // accessor
private:

	LIST_NODE *front; // pointer to front of list (header node)

};

LINKED_LIST_CLASS::LINKED_LIST_CLASS()
{
	cout << endl << "The default constructor has been called.\n";
	front = new LIST_NODE;
	front->next = 0;  //initialize next field to null
	front->data = -10000;
}

LINKED_LIST_CLASS::LINKED_LIST_CLASS(LINKED_LIST_CLASS & org)
{
	cout << endl << "The copy constructor has been called.\n";

	front = new LIST_NODE;

	front->next = 0;
	front->data = -10000;

	LIST_NODE *p = org.front->next;
	LIST_NODE *back = 0;

	while (p != 0)
	{
		if (back == 0)
		{
			front->next = new LIST_NODE;
			back = front->next;
			back->next = 0;
			back->data = p->data;
		}
		else
		{
			back->next = new LIST_NODE;
			back = back->next;
			back->data = p->data;
			back->next = 0;
		}
		p = p->next;
	}

}

LINKED_LIST_CLASS::~LINKED_LIST_CLASS()
{
	cout << endl << "The destructor has been called.\n";

	while (front->next != 0)
	{
		LIST_NODE *p = front->next;
		front->next = front->next->next;
		delete p;
	}
	delete front;
	front = 0;
}

void LINKED_LIST_CLASS::Add(int item)
{
	LIST_NODE *p = new LIST_NODE;

	p->data = item;

	if (front->next == 0) // empty list
	{
		front->next = p;
		p->next = 0;
	}
	else // list has information and is not empty
	{
		p->next = front->next;
		front->next = p
			;
	}
}

void LINKED_LIST_CLASS::Print()
{
	cout << endl;
	for (LIST_NODE *p = front->next; p != 0; p = p->next)
	{
		cout << p->data;
		if (p->next != 0)
		{
			cout << "-->";
		}
	}
	cout << endl << endl;
}

int main()
{
	LINKED_LIST_CLASS L1;

	L1.Add(5);
	L1.Add(10);
	L1.Add(29);

	L1.Print();

	LINKED_LIST_CLASS L2 = L1;

	L2.Print();

	return 0;
}
