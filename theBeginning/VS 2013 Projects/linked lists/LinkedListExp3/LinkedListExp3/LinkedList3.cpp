#include <iostream>
using namespace std;

class LIST_NODE
{
public:
	int data; // data element of node
	LIST_NODE *next; // pointer element of node
};

class LINKED_LIST_CLASS
{
public:
	LINKED_LIST_CLASS(); // default constructor
	LINKED_LIST_CLASS(LINKED_LIST_CLASS &); // copy constructor
	~LINKED_LIST_CLASS(); // destructor
	void Add(int); // mutator
	void Print(); // accessor
	LIST_NODE * Search(int); // accessor
	void Remove(int); // mutator
	bool Is_Empty(); // accessor
private:
	LIST_NODE *front; // pointer to front of list

};


/*Precondition: no parts of an object LINKED_LIST_CLASS have been definited
  Postcondition: the first element of a LINKED_LIST_CLASS has been defined
  This sets the first element of a LINKED_LIST_CLASS object to -10000.*/
LINKED_LIST_CLASS::LINKED_LIST_CLASS()
{
	cout << endl << "The default constructor has been called.\n";
	front = new LIST_NODE;
	front->next = 0;  // initialize the next field to null
	front->data = -10000;
}

/*Precondition: There is no means to copy one LINKED_LIST_CLASS object into another.
Postcondition: There is a means to copy one LINKED_LIST_CLASS object into another.
When invoced, this member function will copy one ojbect into another.*/
LINKED_LIST_CLASS::LINKED_LIST_CLASS(LINKED_LIST_CLASS & org)
{
	cout << endl << "The copy constructor has been called.\n";

	front = new LIST_NODE;

	front->next = 0;
	front->data = -10000;

	LIST_NODE *p = org.front->next;
	LIST_NODE *back = 0;

	while (p != 0)
	{
		if (back == 0)
		{
			front->next = new LIST_NODE;
			back = front->next;
			back->next = 0;
			back->data = p->data;
		}
		else
		{
			back->next = new LIST_NODE;
			back = back->next;
			back->data = p->data;
			back->next = 0;
		}
		p = p->next;
	}

}

/*Precondition: a LINKED_LIST_CLASS object exists in memory
Postcondition: the LINKED_LIST_CLASS object has been destroyed
This sets the first element of a LINKED_LIST_CLASS object to -10000.*/
LINKED_LIST_CLASS::~LINKED_LIST_CLASS()
{
	cout << endl << "The destructor has been called.\n";

	while (front->next != 0)
	{
		LIST_NODE *p = front->next;
		front->next = front->next->next;
		delete p;
	}
	delete front;
	front = 0;
}

/*Precondition: the LINKED_LAST_CLASS object is in a state
Postcondition: an element has been added to the LINKED_LIST_CLASS
this will add an object to the LINKED_LIST_CLASS.*/
void LINKED_LIST_CLASS::Add(int item)
{
	LIST_NODE *p = new LIST_NODE;

	p->data = item;

	if (front->next == 0) // empty list
	{
		front->next = p;
		p->next = 0;
	}
	else // list has information and is not empty
	{
		p->next = front->next;
		front->next = p;
	}
}

/*Precondition: the elements of the LINKED_LIST_CLASS object have not been output to the monitor
Postcondition: the elements of the LINKED_LIST_CLASS object have been ouput to the monitor
this will print the elements of the LINKED_LIST_CLASS object to the monitor.*/
void LINKED_LIST_CLASS::Print()
{
	cout << endl;
	for (LIST_NODE *p = front->next; p != 0; p = p->next)
	{
		cout << p->data;
		if (p->next != 0)
		{
			cout << "-->";
		}
	}
	cout << endl << endl;
}



/*Precondition: the LINKED_LAST_CLASS object has not been searched
Postcondition: the LINKED_LAST_CLASS object has been searched
this will search the LINKED_LIST_CLASS object for a user defined input.*/
LIST_NODE * LINKED_LIST_CLASS::Search(int key)
{
	for (LIST_NODE *p = front->next; p != 0; p = p->next)
	{
		if (p->data == key)
			return p;
	}
	return 0; // key not found in list
}

/*Precondition: the LINKED_LAST_CLASS is in a state
Postcondition: the LINKED_LAST_CLASS object has had one element removed
this will remove one element from the LINKED_LIST_CLASS object.*/
void LINKED_LIST_CLASS::Remove(int key)
{
	LIST_NODE *p = Search(key);

	if (p == 0)
	{
		cout << key << " is not in the list. No removal performed!\n";
	}
	else
	{
		LIST_NODE *q = front;

		while (q->next->data != key)
		{
			q = q->next;
		}

		q->next = p->next; // CRITICAL STEP!!!!
		delete p;
	}

}

bool LINKED_LIST_CLASS::Is_Empty()
{
	return front->next == 0;
}

int main()
{
	LINKED_LIST_CLASS L1;

	L1.Add(5);
	L1.Add(10);
	L1.Add(29);
	L1.Add(10);
	
	cout << L1.Search(29)->data << endl;
	
	L1.Print();

	LINKED_LIST_CLASS L2 = L1;

	L2.Print();

	L1.Remove(10);
	L1.Print();

	return 0;
}
