#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    //Travis Moscicki
    //C for Engineers Homework 3
    //Part 1
    //the purpose of this program is to represent the graph provided
    double x, y;
    cout << "Howdy! Teacher gave us a graph, the following are the values of" << endl;
    cout << "y for each x between -10 and 10 using increments of .1" << endl;

    //this for loop goes from -10 to 10 by increments of .1
    for (x = -10; x <= 10; x = x + .1)
    {
        if (x <= -4)
        {
            y = x + 4;
        }
        else if (x>-4 && x <= 4)
        {
            y = 0;
        }
        else if (x>4)
        {
            y = x - 4;
        }
        cout << "For x=" << x << ", y=" << y << endl;
    }
    return 0;
}
