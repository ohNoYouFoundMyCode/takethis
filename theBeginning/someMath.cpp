//Travis Moscicki
//Part 2
//C for Engineers Exam 1

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    //this program will showcase a for loop, an if statement, an else statement, and cmath
    cout << "Alright, lets do some real math now." << endl;
    //declares variables as doubles because func can be a fraction as well as a negative
    double x, func;
    //initializes a loops of all even numbers between 0 and 25
    for (x=0; x<=25; x=x+2)
    {
        //only evaluates loop when x does not equal 8
        if (x!=8)
    //this program is a function with a verticle asymtote and x=8, so we will not let the fucntion be evaluated at that value.
           {func=(4*pow(x,3)-x+8)/(pow(x,2)-64);
            cout <<"When x="<<x<<" F(x)="<<func<<endl;}
        else
            cout <<"F(x) is infinity!!!!!"<< endl;
    }
    return 0;
}
