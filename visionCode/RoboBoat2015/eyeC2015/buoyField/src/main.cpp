//Travis Moscicki 2014
//Florida Atlantic University College of Engineering and Computer Science
//AUVSI RoboBoat Competition

//Revision B Beginning May 25, 2015
//


//This program is writen for the purpose of identifying multiple objects of both the same color and different colors in an outdoor setting with both
//high and dynamic lighting.  A buoy class is introced that allows us to populate a vector of this class of objects.
//We use two layers of filtration.  The first is normalizedBGR, which makes colors more vibrant and uniform.  The second is chromaticity (YCrCb) which 
//is designed to filter out changes in intensity of light.

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

#include "../include/includes.h"
#include "../include/buoyClass.h"
#include "../include/buoyLocation.h"

bool runWithRecord, threshROI, threshROIAll, threshTrackbar, displayCamera, displayThresh, greenROITest, redROITest;

//default capture width and height
const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;
const int minSize = 200;
const int maxSize = 20000;
Mat cameraFeed;
Mat normalized;
Mat thresholded;
bool calibrationMode, showImages;
float divideBy;
Scalar theSum;
double illuminationGreen;
double illuminationBlue;
double illuminationRed;

int chan1MinTrackbar = 0;
int chan1MaxTrackbar = 255;
int chan2MinTrackbar = 0;
int chan2MaxTrackbar = 255;
int chan3MinTrackbar = 0;
int chan3MaxTrackbar = 255;
Mat camChannels[3];
double blueSum;
double greenSum;
double redSum;
Mat sumChannels;
int numberPixels = 640*480;
int numberPixelsTotal = 638*478*3;
BUOY green("greenSpeed"), red("redSpeed");


int xSize, ySize, threshMin1, threshMax1, threshMin2, threshMax2, threshMin3, threshMax3, recSize, meanH, meanS, meanV;			
string threshFile;			
ofstream threshWrite;

Point point1, point2; /* vertical points of the bounding box */
int drag = 0;
Rect rect; /* bounding box */
Mat img, roiImg; /* roiImg - the part of the image in the bounding box */
int select_flag = 0;

void greyEdge(Mat);
void createTrackbars();
void onTrackbar(int, void*);
void mouseHandler(int event, int x, int y, int flags, void* param);

int main(int argc, char* argv[])
{

	red.count = green.count = 0;
	string truth = "descriptors/truth.txt";
	ifstream truthReader;
	truthReader.open(truth.c_str());
	if (truthReader.is_open())
	{
		truthReader >> runWithRecord >> threshROI >> threshROIAll >> threshTrackbar >> displayCamera >> displayThresh >> greenROITest >> redROITest;
	}
	else
		cout << "sorry, couldn't open the truth file" << endl;

	truthReader.close();
	VideoCapture capture;
	capture.open(0);
	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
	
	VideoWriter feed("cameraFeed.avi", CV_FOURCC('M', 'J', 'P', 'G'), 3, Size(FRAME_WIDTH, FRAME_HEIGHT), true);
	VideoWriter thresh("thresholded.avi", CV_FOURCC('M', 'J', 'P', 'G'), 3, Size(FRAME_WIDTH, FRAME_HEIGHT), true);
	VideoWriter bal("balanced.avi", CV_FOURCC('M', 'J', 'P', 'G'), 3, Size(FRAME_WIDTH, FRAME_HEIGHT), true);

	if (threshTrackbar==1)
	{
		createTrackbars();
	}

	green.findValues();
	red.findValues();

	while (1)
	{		
		red.objVec.clear();
		green.objVec.clear();
		//cout << "im after clear" << endl;	

		for (int i=0; i<10; i++)
		{capture>>cameraFeed;}
		medianBlur(cameraFeed, cameraFeed, 7);
		cout << "im after cameraRead" << endl;	
		greyEdge(cameraFeed);
		cout << "im after greyEdge" << endl;	

		if(threshTrackbar==1)
		{
			cout << "in trackbar two" <<endl;
			Mat thresholdTest;
			inRange(normalized, Scalar(chan1MinTrackbar, chan2MinTrackbar, chan3MinTrackbar), Scalar(chan1MaxTrackbar, chan2MaxTrackbar, chan3MaxTrackbar), thresholdTest);
			string threshColor = "Threshold Tester";
			imshow(threshColor, thresholdTest);
		}



		if(threshROI)
		{
			cout << "in here" << endl;
			
			cvSetMouseCallback("image", mouseHandler, NULL);
			if (select_flag == 1)
			{
			    imshow("ROI", roiImg); /* show the image bounded by the box */
			}
			rectangle(normalized, rect, CV_RGB(255, 0, 0), 3, 8, 0);
			imshow("image", normalized);
			recSize = xSize*ySize;
			
			Mat threshChannels[3];
			split(roiImg, threshChannels);
			meanH = (sum(threshChannels[0])[0])/(recSize);
			meanS = (sum(threshChannels[1])[0])/(recSize);
			meanV = (sum(threshChannels[2])[0])/(recSize);
			//chosen by comparing trackbar values to mean values
	
			if (greenROITest)
			{			
				threshMin1 = meanH+10;
				threshMax1 = meanH+30;
				threshMin2 = meanS;
				threshMax2 = 255;
				threshMin3 = 0;
				threshMax3 = 255;
			}
			
			if(redROITest)
			{		
				threshMin1 = meanH+10;
				threshMax1 = meanH+30;
				threshMin2 = meanS;
				threshMax2 = 255;
				threshMin3 = 0;
				threshMax3 = 255;
			}
			
			threshFile = "descriptors/treshVal.txt";
			threshWrite.open(threshFile.c_str());
			if (threshWrite.is_open())
			{
				threshWrite << threshMin1 << endl << threshMax1 << endl << threshMin2 << endl << threshMax2 << endl << threshMin3 << endl << threshMax3;
			}
			else
				cout << "sorry, couldn't open the truth file" << endl;

			threshWrite.close();
		}

		for(int i=0; i<10; i++)
		{
			green.setThreshold(normalized);
			green.morphOps();
			green.trackObject(cameraFeed);
			red.setThreshold(normalized);
			red.morphOps();
			red.trackObject(cameraFeed);
		}
		if(green.count>8)
		{
			green.drawObject(cameraFeed);
			red.drawObject(cameraFeed);
		}

		if(displayCamera==1)
		{
			imshow("camera feed", cameraFeed);
			imshow("normalized", normalized);
		}

		if(displayThresh==1)
		{
			green.show();
			//red.show();
		}		

		if(runWithRecord==1)
		{		
			feed.write(cameraFeed);
			thresh.write(normalized);
			//bal.write(yellow.thresholdedLocal);
		}		
		
		if (waitKey(30) == 27)
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
		//cout << "im after wait" << endl;
	}
	return 0;
}//end of main







void greyEdge(Mat cameraFeed)
{
	split(cameraFeed, camChannels);
	Mat camChannels2[3];
	split(cameraFeed, camChannels2);
	blueSum=greenSum=redSum=0;

	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{
			Point2f location(x, y);					
			double dy, dx;
			dy=(camChannels[0].at<uchar>(location.y+1, location.x)-camChannels[0].at<uchar>(location.y-1, location.x))/2;	
			dx=(camChannels[0].at<uchar>(location.y, location.x+1)-camChannels[0].at<uchar>(location.y, location.x-1))/2;
			blueSum+=pow(sqrt(dy*dy+dx*dx),16);				
			//blueSum+=sqrt(dy*dy+dx*dx);					
			//blueGrad.at<uchar>(location.y, location.x) = sqrt(dy*dy+dx*dx);	
			//cout << num<< endl;
		
		}
	}	
	for (int y = 1; y < 480; y++)
	{
		for (int x = 1; x < 640; x++)
		{
			Point2f location(x, y);
			double dy, dx;		
			dy=(camChannels[1].at<uchar>(location.y+1, location.x)-camChannels[1].at<uchar>(location.y-1, location.x))/2;	
			dx=(camChannels[1].at<uchar>(location.y, location.x+1)-camChannels[1].at<uchar>(location.y, location.x-1))/2;
			greenSum+=pow(sqrt(dy*dy+dx*dx),16);	
			//greenSum+=sqrt(dy*dy+dx*dx);	
			//cout << num<< endl;
		
		}
	}
			
	for (int y = 1; y < 479; y++)
	{
		for (int x = 1; x < 639; x++)
		{
			Point2f location(x, y);
			double dy, dx;		
			dy=(camChannels[2].at<uchar>(location.y+1, location.x)-camChannels[2].at<uchar>(location.y-1, location.x))/2;	
			dx=(camChannels[2].at<uchar>(location.y, location.x+1)-camChannels[2].at<uchar>(location.y, location.x-1))/2;
			redSum+=pow(sqrt(dy*dy+dx*dx),16);
			//redSum+=sqrt(dy*dy+dx*dx);
			//cout << num<< endl;
		
		}
	}
	
	//double gradSum = sqrt(blueSum*blueSum+greenSum*greenSum+redSum*redSum);
	illuminationBlue = pow(blueSum/numberPixels, 1/16.0);
	illuminationGreen = pow(greenSum/numberPixels, 1/16.0);
	illuminationRed = pow(redSum/numberPixels, 1/16.0);
	//illumination = (gradSum/numberPixelsTotal);
	cout << "Sums:" << blueSum << ", " << greenSum << ", " << redSum << endl;

	Scalar blue = (sum(camChannels2[0]));
	double blueVal = (blue[0]/numberPixels);

	Scalar greenChan = (sum(camChannels2[1]));
	double greenVal = (greenChan[0]/numberPixels);
	
	Scalar redChan = (sum(camChannels2[2]));
	double redVal = (redChan[0]/numberPixels);
	
	cout << blue[0] <<endl;
	cout << numberPixels << endl;
	cout << blueVal << endl;
	cout << "Illuminations:" << illuminationBlue << ", " << illuminationGreen << ", " << illuminationRed << endl;
	cout << "Means:" << blueVal << ", " << greenVal << ", " << redVal << endl;

	
	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{
		Point2f location(x, y);			
		camChannels2[0].at<uchar>(location.y, location.x)=camChannels2[0].at<uchar>(location.y, location.x)*(illuminationBlue/blueVal);	
		//cout << num<< endl;
		}
	}
	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{	
		Point2f location(x, y);			
		camChannels2[1].at<uchar>(location.y, location.x)=camChannels2[1].at<uchar>(location.y, location.x)*(illuminationGreen/greenVal);	
		//cout << num<< endl;
		}
	}
	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{	
		Point2f location(x, y);			
		camChannels2[2].at<uchar>(location.y, location.x)=camChannels2[2].at<uchar>(location.y, location.x)*(illuminationRed/redVal);	
		//cout << num<< endl;
		}
	}	
	
	//imshow("blue", camChannels2[0]);
	//imshow("green", camChannels2[1]);
	//imshow("red", camChannels2[2]);
	merge(camChannels2, 3, normalized);
	//cvtColor(normalized, normalized, COLOR_BGR2GRAY);

//cout << "im after merge" << endl;	
	cvtColor(normalized, normalized, COLOR_BGR2HSV);
	//cvtColor(cameraFeed, cameraFeed, COLOR_BGR2HSV);
	//cout << "im after hsv" << endl;
}

void onTrackbar(int, void*)
{
}

//definition of createTrackbars functions
void createTrackbars()
{
	
	cout << "in trackbars" << endl;
	string name = "Trackbars";
	//create window for YCrCb trackbars
	namedWindow(name, CV_WINDOW_AUTOSIZE);

	//create memory to store trackbar name on window
	char TrackbarNameYCrCb[50];
	sprintf(TrackbarNameYCrCb, "chan 1 min", chan1MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 1 max", chan1MaxTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 2 min", chan2MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 2 max", chan2MaxTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 3 min", chan3MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 3 max", chan3MaxTrackbar);


	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.Y_MIN),
	//the max value the trackbar can move (eg. Y_MAX), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbarYCrCb)
	createTrackbar("chan 1 min", name, &chan1MinTrackbar, chan1MaxTrackbar, onTrackbar);
	createTrackbar("chan 1 max", name, &chan1MaxTrackbar, chan1MaxTrackbar, onTrackbar);
	createTrackbar("chan 2 min", name, &chan2MinTrackbar, chan2MaxTrackbar, onTrackbar);
	createTrackbar("chan 2 max", name, &chan2MaxTrackbar, chan2MaxTrackbar, onTrackbar);
	createTrackbar("chan 3 min", name, &chan3MinTrackbar, chan3MaxTrackbar, onTrackbar);
	createTrackbar("chan 3 max", name, &chan3MaxTrackbar, chan3MaxTrackbar, onTrackbar);
	
}

void mouseHandler(int event, int x, int y, int flags, void* param)
{
    if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {
	cout << "here"<<endl;
        /* left button clicked. ROI selection begins */
        point1 = Point(x, y);
        drag = 1;
    }
    
    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
	cout << "here2"<<endl;
        /* mouse dragged. ROI being selected */
        Mat img1 = normalized.clone();
        point2 = Point(x, y);
        rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
        imshow("image", img1);
    }
    
    if (event == CV_EVENT_LBUTTONUP && drag)
    {
	cout << drag<<endl;
        point2 = Point(x, y);
        rect = Rect(point1.x,point1.y,x-point1.x,y-point1.y);
	xSize = point2.x-point1.x;
	ySize = point2.y-point1.y;
        drag = 0;
        roiImg = normalized(rect);
    }
    
    if (event == CV_EVENT_LBUTTONUP)
    {
	cout << "here4"<<endl;
       /* ROI selected */
        select_flag = 1;
        drag = 0;
    }
}





