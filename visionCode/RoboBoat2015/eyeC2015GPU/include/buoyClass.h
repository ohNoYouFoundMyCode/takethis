#ifndef BUOY_CLASS_H
#define BUOY_CLASS_H
#include "includes.h"
#include "buoyLocation.h"

class BUOY
{
public:
	BUOY(string buoyColor); 
	void findValues();
	void setThreshold(Mat manipuatedMat);
	void createWindows();
	void createTrackbars();
	void morphOps();
	void trackObject(Mat &cameraFeed);
	void getDistance();
	void setNED();
	void drawObject(Mat &cameraFeed);
	void show();
	void publishUp();
	bool calibrationModeLocal;
	vector<OBJ> objVec;
	Mat thresholdedLocal;

private:
	int maxNumberObjects;
	string color;
	int minObjectArea;
	int maxObjectArea;
	int16_t xPos;
	int16_t yPos;
	int chan1Min;
	int chan1Max;
	int chan2Min;
	int chan2Max;
	int chan3Min;
	int chan3Max;
	int chan1MinTrackbar;
	int chan1MaxTrackbar;
	int chan2MinTrackbar;
	int chan2MaxTrackbar;
	int chan3MinTrackbar;
	int chan3MaxTrackbar;
	const string thresholdWindowName;
	const string trackbarWindowName;
	//distance
	//NED location
};

#endif
