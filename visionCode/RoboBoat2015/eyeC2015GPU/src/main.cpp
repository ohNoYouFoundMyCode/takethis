//Travis Moscicki 2014
//Florida Atlantic University College of Engineering and Computer Science
//AUVSI RoboBoat Competition

//Revision B Beginning May 25, 2015
//


//This program is writen for the purpose of identifying multiple objects of both the same color and different colors in an outdoor setting with both
//high and dynamic lighting.  A buoy class is introced that allows us to populate a vector of this class of objects.
//We use two layers of filtration.  The first is normalizedBGR, which makes colors more vibrant and uniform.  The second is chroGpuMaticity (YCrCb) which 
//is designed to filter out changes in intensity of light.

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

#include "../include/includes.h"
#include "../include/buoyClass.h"
#include "../include/buoyLocation.h"
#include <unistd.h>

bool runWithRecord, threshROI, threshROIAll, threshTrackbar, displayCamera, displayThresh;

//default capture width and height
const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;
const int minSize = 200;
const int maxSize = 20000;
Mat cameraFeed;
gpu::GpuMat normalized;
gpu::GpuMat thresholded;
bool calibrationMode, showImages;
float divideBy;
Scalar theSum;
double illuminationBlue;
double illuminationGreen;
double illuminationRed;

int chan1MinTrackbar = 0;
int chan1MaxTrackbar = 255;
int chan2MinTrackbar = 0;
int chan2MaxTrackbar = 255;
int chan3MinTrackbar = 0;
int chan3MaxTrackbar = 255;
gpu::GpuMat camChannels[3];
double blueSum;
double greenSum;
double redSum;
gpu::GpuMat sumChannels;
double numberPixels = 640*480;
double numberPixelsTotal = 638*478/9;
BUOY green("green"), white("white"), red("red"), black("black"), yellow("yellow");


int xSize, ySize, threshMin1, threshMax1, threshMin2, threshMax2, threshMin3, threshMax3, recSize, meanH, meanS, meanV;			
string threshFile;			
ofstream threshWrite;

Point point1, point2; /* vertical points of the bounding box */
int drag = 0;
Rect rect; /* bounding box */
gpu::GpuMat img, roiImg; /* roiImg - the part of the image in the bounding box */
int select_flag = 0;

void greyEdge(gpu::GpuMat);
void createTrackbars();
void onTrackbar(int, void*);
void mouseHandler(int event, int x, int y, int flags, void* param);

int main(int argc, char* argv[])
{

	/*string truth = "descriptors/truth.txt";
	ifstream truthReader;
	truthReader.open(truth.c_str());
	if (truthReader.is_open())
	{
		truthReader >> runWithRecord >> threshROI >> threshROIAll >> threshTrackbar >> displayCamera >> displayThresh;
	}
	else
		cout << "sorry, couldn't open the truth file" << endl;

	truthReader.close();*/
	VideoCapture capture(0);
	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
	capture.set(CV_CAP_PROP_HUE, 1);
	
	/*VideoWriter feed("cameraFeedGPU.avi", CV_FOURCC('M', 'J', 'P', 'G'), 3, Size(FRAME_WIDTH, FRAME_HEIGHT), true);
	VideoWriter thresh("thresholded.avi", CV_FOURCC('M', 'J', 'P', 'G'), 3, Size(FRAME_WIDTH, FRAME_HEIGHT), true);
	VideoWriter bal("balanced.avi", CV_FOURCC('M', 'J', 'P', 'G'), 3, Size(FRAME_WIDTH, FRAME_HEIGHT), true);*/

	if (threshTrackbar==1)
	{
		createTrackbars();
	}

	green.findValues();
	white.findValues();
	red.findValues();
	black.findValues();
	yellow.findValues();

	while (1)
	{		
		/*red.objVec.clear();
		white.objVec.clear();
		green.objVec.clear();
		black.objVec.clear();
		yellow.objVec.clear();
*/		for (int i=0; i<10; i++)
		{capture>>cameraFeed;}
		cout << "im after clear" << endl;	
		medianBlur(cameraFeed, cameraFeed, 7);
		gpu::GpuMat cameraFeedGPU(cameraFeed);
		cout << "im after cameraRead" << endl;
		//gpu::cvtColor(cameraFeedGPU, cameraFeedGPU, COLOR_BGR2HSV);
		greyEdge(cameraFeedGPU);
		cout << "im after greyEdge" << endl;	

		/*if(threshTrackbar==1)
		{
			Mat thresholdTest;
			inRange(camPost, Scalar(chan1MinTrackbar, chan2MinTrackbar, chan3MinTrackbar), Scalar(chan1MaxTrackbar, chan2MaxTrackbar, chan3MaxTrackbar), thresholdTest);
			string threshColor = "Threshold Tester";
			imshow(threshColor, thresholdTest);
		}*/



		/*if(threshROI==1)
		{
			cout << "in here" << endl;
			
			cvSetMouseCallback("image", mouseHandler, NULL);
			if (select_flag == 1)
			{
			    imshow("ROI", roiImg);  //show the image bounded by the box 
			}
			rectangle(normalized, rect, CV_RGB(255, 0, 0), 3, 8, 0);
			imshow("image", normalized);
			recSize = xSize*ySize;
			
			gpu::GpuMat threshChannels[3];
			split(roiImg, threshChannels);
			meanH = (sum(threshChannels[0])[0])/(recSize);
			meanS = (sum(threshChannels[1])[0])/(recSize);
			meanV = (sum(threshChannels[2])[0])/(recSize);
			//chosen by comparing trackbar values to mean values
			threshMin1 = meanH;
			threshMax1 = meanH+20;
			threshMin2 = meanS;
			threshMax2 = 255;
			threshMin3 = 0;
			threshMax3 = meanV+10;

			threshFile = "descriptors/treshVal.txt";
			threshWrite.open(threshFile.c_str());
			if (threshWrite.is_open())
			{
				threshWrite << threshMin1 << endl << threshMax1 << endl << threshMin2 << endl << threshMax2 << endl << threshMin3 << endl << threshMax3;
			}
			else
				cout << "sorry, couldn't open the truth file" << endl;

			threshWrite.close();
		}*/



			/*Mat reNorm(normalized);
			Mat camPost(cameraFeedGPU);

		cout << "im after transform" << endl;
		green.setThreshold(reNorm);
		cout << "after thresh" << endl;
		green.morphOps();
		cout << "after morph" << endl;
		green.trackObject(camPost);
		cout << "after track" << endl;
		green.drawObject(camPost);
		cout << "after draw" << endl;

		//cout << "im after green" << endl;
		white.setThreshold(reNorm);
		white.morphOps();
		white.trackObject(camPost);
		white.drawObject(camPost);

		//cout << "im after white" << endl;
		red.setThreshold(reNorm);
		red.morphOps();
		red.trackObject(camPost);
		red.drawObject(camPost);

		//cout << "im after red" << endl;
		black.setThreshold(reNorm);
		black.morphOps();
		black.trackObject(camPost);
		black.drawObject(camPost);

		cout << "im after black" << endl;
		yellow.setThreshold(reNorm);
		yellow.morphOps();
		yellow.trackObject(camPost);
		yellow.drawObject(camPost);*/
		
		if(displayCamera==1)
		{
			//imshow("norm", reNorm);
			//imshow("post", camPost);
imshow("output", cameraFeed);
sleep(1);
		}
imshow("output", cameraFeed);
		if(displayThresh==1)
		{
			green.show();
			white.show();
			red.show();
			black.show();
			yellow.show();
		}		

		/*if(runWithRecord==1)
		{		
			feed.write(cameraFeedGPU);
			thresh.write(normalized);
			//bal.write(yellow.thresholdedLocal);
		}*/		
		
		if (waitKey(30) == 27)
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
		cout << "im after wait" << endl;
	}
	return 0;
}//end of main

void greyEdge(gpu::GpuMat cameraFeedGPU)
{
	split(cameraFeedGPU, camChannels);

	/*gpu::GpuMat gradX1, gradX2, gradX3, gradY1, gradY2, gradY3; 

	gpu::GpuMat grad1, grad2, grad3;

	gpu::GpuMat abs_gradX1, abs_gradX2, abs_gradX3, abs_gradY1, abs_gradY2, abs_gradY3; 
		
	gpu::Sobel(camChannels[0], gradX1, CV_16S, 1, 0);
  	abs(gradX1, abs_gradX1);
	gpu::Sobel(camChannels[0], gradY1, CV_16S, 0, 1);	
	abs(gradY1, abs_gradY1);	

	gpu::Sobel(camChannels[1], gradX2, CV_16S, 1, 0);
  	abs(gradX2, abs_gradX2);	
	gpu::Sobel(camChannels[1], gradY2, CV_16S, 0, 1);
	abs(gradY2, abs_gradY2);		

	gpu::Sobel(camChannels[2], gradX3, CV_16S, 1, 0);	
  	abs(gradX3, abs_gradX3);	
	gpu::Sobel(camChannels[2], gradY3, CV_16S, 0, 1);
	abs(gradY3, abs_gradY3);		
	
	
	//gpu::addWeighted(abs_gradX1, 0.5, abs_gradY1, 0.5, 0, grad1);
	//grad1 = gpu::sqrt(abs_gradX1*abs_gradX1+abs_gradY1*abs_gradY1);
	gpu::GpuMat gradPowerBlue, gradPowerGreen, gradPowerRed;
	cout << "tester: " << sum(grad1)[0]/numberPixels << endl;	
	gpu::pow(grad1, 16, gradPowerBlue);
	blueSum = gpu::sum(gradPowerBlue)[0];

	gpu::addWeighted(abs_gradX2, 0.5, abs_gradY2, 0.5, 0, grad2);
	//grad2 = gpu::sqrt(abs_gradX2*abs_gradX2+abs_gradY2*abs_gradY2);
	gpu::pow(grad2, 16, gradPowerGreen);
	redSum = gpu::sum(gradPowerGreen)[0];

	gpu::addWeighted(abs_gradX3, 0.5, abs_gradY3, 0.5, 0, grad3);
	//grad3 = gpu::sqrt(abs_gradX3*abs_gradX3+abs_gradY3*abs_gradY3);
	gpu::pow(grad3, 16, gradPowerRed);
	greenSum = gpu::sum(gradPowerRed)[0];

	//double gradSum = blueSum+greenSum+redSum;
	//illuminationBlue = blueSum/numberPixels;
	//illuminationGreen = greenSum/numberPixels;
	//illuminationRed = redSum/numberPixels;
	illuminationBlue = pow(blueSum/numberPixels, 1/16.0);
	illuminationGreen = pow(greenSum/numberPixels, 1/16.0);
	illuminationRed = pow(redSum/numberPixels, 1/16.0);

	cout << "Sums:" << blueSum << ", " << greenSum << ", " << redSum << endl;

	gpu::GpuMat camChannels2[3];
	split(cameraFeedGPU, camChannels2);
	
	Scalar blue = (sum(camChannels2[0]));
	double blueVal = (blue[0]/numberPixels);
	
	Scalar greenChan = (sum(camChannels2[1]));
	double greenVal = (greenChan[0]/numberPixels);
	
	Scalar redChan = (sum(camChannels2[2]));
	double redVal = (redChan[0]/numberPixels);
	
	cout << blue[0] << endl;
	cout << numberPixels << endl;
	cout << blueVal << endl;
	cout << "Illuminations:" << illuminationBlue << ", " << illuminationGreen << ", " << illuminationRed << endl;
	cout << "Means:" << blueVal << ", " << greenVal << ", " << redVal << endl;*/

	gpu::multiply(camChannels[0], 76.0/100, camChannels[0]);
	gpu::multiply(camChannels[1], 60.0/100, camChannels[1]);
	gpu::multiply(camChannels[2], 60.0/100, camChannels[2]);	

	//imshow("blue", camChannels[0]);
	//imshow("green", camChannels[1]);
	//imshow("red", camChannels[2]);
	merge(camChannels, 3, normalized);
	//cout << "im after merge" << endl;	
	gpu::cvtColor(normalized, normalized, COLOR_BGR2HSV);
	//cout << "im after hsv" << endl;
}

void onTrackbar(int, void*)
{
}

//definition of createTrackbars functions
void createTrackbars()
{
	string name = "Trackbars";
	//create window for YCrCb trackbars
	namedWindow(name, CV_WINDOW_AUTOSIZE);

	//create memory to store trackbar name on window
	char TrackbarNameYCrCb[50];
	sprintf(TrackbarNameYCrCb, "chan 1 min", chan1MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 1 max", chan1MaxTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 2 min", chan2MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 2 max", chan2MaxTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 3 min", chan3MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 3 max", chan3MaxTrackbar);


	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.Y_MIN),
	//the max value the trackbar can move (eg. Y_MAX), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbarYCrCb)
	createTrackbar("chan 1 min", name, &chan1MinTrackbar, chan1MaxTrackbar, onTrackbar);
	createTrackbar("chan 1 max", name, &chan1MaxTrackbar, chan1MaxTrackbar, onTrackbar);
	createTrackbar("chan 2 min", name, &chan2MinTrackbar, chan2MaxTrackbar, onTrackbar);
	createTrackbar("chan 2 max", name, &chan2MaxTrackbar, chan2MaxTrackbar, onTrackbar);
	createTrackbar("chan 3 min", name, &chan3MinTrackbar, chan3MaxTrackbar, onTrackbar);
	createTrackbar("chan 3 max", name, &chan3MaxTrackbar, chan3MaxTrackbar, onTrackbar);
	
}

void mouseHandler(int event, int x, int y, int flags, void* param)
{
    /*if (event == CV_EVENT_LBUTTONDOWN && !drag)
    {
	cout << "here"<<endl;
        //left button clicked. ROI selection begins 
        point1 = Point(x, y);
        drag = 1;
    }
    
    if (event == CV_EVENT_MOUSEMOVE && drag)
    {
	cout << "here2"<<endl;
        //mouse dragged. ROI being selected 
        gpu::GpuMat img1 = normalized.clone();
        point2 = Point(x, y);
        rectangle(img1, point1, point2, CV_RGB(255, 0, 0), 3, 8, 0);
        imshow("image", img1);
    }
    
    if (event == CV_EVENT_LBUTTONUP && drag)
    {
	cout << drag<<endl;
        point2 = Point(x, y);
        rect = Rect(point1.x,point1.y,x-point1.x,y-point1.y);
	xSize = point2.x-point1.x;
	ySize = point2.y-point1.y;
        drag = 0;
        roiImg = normalized(rect);
    }
    
    if (event == CV_EVENT_LBUTTONUP)
    {
	cout << "here4"<<endl;
       	//ROI selected
        select_flag = 1;
        drag = 0;
    }*/
}





