//Travis Moscicki 2014
//Florida Atlantic University College of Engineering and Computer Science
//AUVSI RoboBoat Competition

//This program is writen for the purpose of identifying multiple objects of both the same color and different colors in an outdoor setting with both
//high and dynamic lighting.  A buoy class is introced that allows us to populate a vector of this class of objects.
//We use two layers of filtration.  The first is normalizedBGR, which makes colors more vibrant and uniform.  The second is chromaticity (YCrCb) which 
//is designed to filter out changes in inensity of light.

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

#include <ctime>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <stdint.h>
#include <windows.h>
#include <inttypes.h>
#include <valarray>

#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <opencv2\imgproc\imgproc.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"

#include <lcm/lcm-cpp.hpp>

#include "libconfig.h++"
#include <iomanip>
#include <cstdlib>

#include "ObjectList.hpp"
#include "state.hpp"

using namespace cv;
using namespace std;
using namespace libconfig;

RB::state state;

class StateHandler
{
public:
	~StateHandler(){}
	void handleMessage(const lcm::ReceiveBuffer* rbuf,
                const std::string& chan, 
                const RB::state* msg);
};

int blackRangelMin, blackRangelMax, blackRangeuMin, blackRangeuMax, blackRangevMin, blackRangevMax;
int redRangelMin, redRangelMax,	redRangeuMin, redRangeuMax, redRangevMin, redRangevMax;
int yellowRangelMin, yellowRangelMax, yellowRangeuMin, yellowRangeuMax, yellowRangevMin, yellowRangevMax;
int greenRangelMin, greenRangelMax, greenRangeuMin, greenRangeuMax, greenRangevMin, greenRangevMax;
int blueRangelMin, blueRangelMax, blueRangeuMin, blueRangeuMax, blueRangevMin, blueRangevMax;
int whiteRangelMin, whiteRangelMax, whiteRangeuMin, whiteRangeuMax, whiteRangevMin, whiteRangevMax;

//this functions sets up our trackbars and can either be enable or disabled
void createTrackbarsLUV();

//range for L, U, and V values van be changed by enabling trackbars
int L_MIN = 0;
int L_MAX = 255;
int U_MIN = 0;
int U_MAX = 255;
int V_MIN = 0;
int V_MAX = 255;

//This function gets called whenever a trackbar position is changed
void on_trackbarLUV(int, void*);

//default capture width and height
const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;

const int MIN_OBJECT_AREA_WHITE = 500;
const int MAX_OBJECT_AREA_WHITE = 4000000;

const int MIN_OBJECT_AREA = 100;
const int MAX_OBJECT_AREA = 4000;

//Matrixes to store each frame of the webcam feed, thresholded images, as well as final YCrCb image
Mat cameraFeed;
Mat thresholdBlack;
Mat thresholdRed;
Mat thresholdYellow;
Mat thresholdGreen;
Mat thresholdBlue;
Mat thresholdWhite;
Mat LUV;

//class to allow for easy changes of LUV values
class rangeLUV 
{
	public:
		uint8_t lMin;
		uint8_t lMax;
		uint8_t uMin;
		uint8_t uMax;
		uint8_t vMin;
		uint8_t vMax;
};

//max number of objects to be detected in frame
const int MAX_NUM_OBJECTS = 1000;

//names that will appear at the top of each window
const string windowName = "Original Image";
const string windowName1 = "LUV Image";
const string windowName2 = "Black Thresholding";
const string windowName3 = "Red Thresholding";
const string windowName4 = "Yellow Thresholding";
const string windowName5 = "Green Thresholding";
const string windowName6 = "Blue Thresholding";
const string windowName7 = "White Thresholding";
const string windowName8 = "Normalized BGR";
const string windowName9 = "LUV Threshold Tester";
const string trackbarWindowNameLUV = "LUV Trackbars";

//sets types to make changes in drawObject function easier and for passing through LCM
enum objectType {BLACK_BUOY = 1, RED_BUOY, YELLOW_BUOY, GREEN_BUOY, BLUE_BUOY, WHITE_BUOY};

//initializes a vector called objVec made of "Object" class objects, this is what we will pass through LCM to the spotter algorithm
vector<RB::Object> objVec;

//initializes arguement objList of class ObjectList
RB::ObjectList objList;

//protoype for the function that will draw circles and location on dectected objects
void drawObject(vector<RB::Object> objVec, Mat &cameraFeed);

//create structuring element that will be used to dilate (enlarge) and erode (shrink) image.
//this is useful in destroying outliers common with imperfect filtration
void morphOps(Mat &thresh);
void morphOpsBlack(Mat &thresh);

//these are the prototypes for our algorithm that will track filtered objects
//if more objects are to be tracked, they must be initialized here, added to the enumeration of "objectType", and defined below
void trackFilteredObjectBlack(Mat thresholdBlack, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectRed(Mat thresholdRed, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectYellow(Mat thresholdYellow, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectGreen(Mat thresholdGreen, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectBlue(Mat thresholdBlue, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectWhite(Mat thresholdWhite, Mat LUV, Mat &cameraFeed, uint64_t systemTime);

//protoype for int to string
string intToString(int number);

//beginning of our main function
int main(int argc, char* argv[])
{
	//Set up for message parsing
	Config cfg;

	try
	{
		cfg.readFile("buoysConfig.cfg");
	}
	catch(const FileIOException &fioex)
	{
		std::cerr << "I/O error while reading file." << std::endl;
		return(EXIT_FAILURE);
	}
	catch(const ParseException &pex)
	{
		std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
		<< " - " << pex.getError() << std::endl;
		return(EXIT_FAILURE);
	}
	
	const Setting& root = cfg.getRoot();
	
	// obtains all information about buoy thresholding 
	try
	{
		const Setting &buoysVal = root["valuesLUV"]["buoysVal"];
		int count = buoysVal.getLength();

		for(int i = 0; i < count; ++i)
		{
			const Setting &buoyVal = buoysVal[i];
			
			// Only output the record if all of the expected fields are present.
			if(!(  buoyVal.lookupValue("blackRangelMin", blackRangelMin)
				&& buoyVal.lookupValue("blackRangelMax", blackRangelMax)
				&& buoyVal.lookupValue("blackRangeuMin", blackRangeuMin)
				&& buoyVal.lookupValue("blackRangeuMax", blackRangeuMax)
				&& buoyVal.lookupValue("blackRangevMin", blackRangevMin)
				&& buoyVal.lookupValue("blackRangevMax", blackRangevMax)

				&& buoyVal.lookupValue("redRangelMin", redRangelMin)
				&& buoyVal.lookupValue("redRangelMax", redRangelMax)
				&& buoyVal.lookupValue("redRangeuMin", redRangeuMin)
				&& buoyVal.lookupValue("redRangeuMax", redRangeuMax)
				&& buoyVal.lookupValue("redRangevMin", redRangevMin)
				&& buoyVal.lookupValue("redRangevMax", redRangevMax)

				&& buoyVal.lookupValue("yellowRangelMin", yellowRangelMin)
				&& buoyVal.lookupValue("yellowRangelMax", yellowRangelMax)
				&& buoyVal.lookupValue("yellowRangeuMin", yellowRangeuMin)
				&& buoyVal.lookupValue("yellowRangeuMax", yellowRangeuMax)
				&& buoyVal.lookupValue("yellowRangevMin", yellowRangevMin)
				&& buoyVal.lookupValue("yellowRangevMax", yellowRangevMax)
								
				&& buoyVal.lookupValue("greenRangelMin", greenRangelMin)
				&& buoyVal.lookupValue("greenRangelMax", greenRangelMax)
				&& buoyVal.lookupValue("greenRangeuMin", greenRangeuMin)
				&& buoyVal.lookupValue("greenRangeuMax", greenRangeuMax)
				&& buoyVal.lookupValue("greenRangevMin", greenRangevMin)
				&& buoyVal.lookupValue("greenRangevMax", greenRangevMax)

				&& buoyVal.lookupValue("blueRangelMin", blueRangelMin)
				&& buoyVal.lookupValue("blueRangelMax", blueRangelMax)
				&& buoyVal.lookupValue("blueRangeuMin", blueRangeuMin)
				&& buoyVal.lookupValue("blueRangeuMax", blueRangeuMax)
				&& buoyVal.lookupValue("blueRangevMin", blueRangevMin)
				&& buoyVal.lookupValue("blueRangevMax", blueRangevMax)

				&& buoyVal.lookupValue("whiteRangelMin", whiteRangelMin)
				&& buoyVal.lookupValue("whiteRangelMax", whiteRangelMax)
				&& buoyVal.lookupValue("whiteRangeuMin", whiteRangeuMin)
				&& buoyVal.lookupValue("whiteRangeuMax", whiteRangeuMax)
				&& buoyVal.lookupValue("whiteRangevMin", whiteRangevMin)
				&& buoyVal.lookupValue("whiteRangevMax", whiteRangevMax)))
				continue;
			}
	}
	catch(const SettingNotFoundException &nfex)
	{
		cout<<"Sorry hombre nothing found"<< endl; 
	}

	//creates and verifies our lcm channels for interprocess communication
	lcm::LCM lcm = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!lcm.good())
	{
		printf("failed to create lcm");
		return 1;
	}

/*	lcm::LCM lcmState = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!lcmState.good())
	{
		printf("failed to create lcmState");
		return 1;
	}

	StateHandler stateHandler;
	lcmState.subscribe("MIS_STATE", &StateHandler::handleMessage, &stateHandler);

	struct timeval tv;
	struct timeval timeOut;
	timeOut.tv_sec = 0;
	timeOut.tv_usec = 50000;*/

	//Set to true if we would like to calibrate our filter values
	bool calibrationMode = true;
	if (calibrationMode)
	{
		//create slider bars for Luv filtering
		createTrackbarsLUV();
	}
	
	//video capture object to acquire webcam feed
	VideoCapture capture;
	capture.open(0);

	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
	
	//start an infinite loop where webcam feed is copied to cameraFeed matrix
	//all of our operations will be performed within this loop
	while (1)
	{
		//clears out vector between loops
		objVec.clear();

		//select function used in case there is problem with lcmState
/*		int32_t objectReadFD = lcmState.getFileno();
		fd_set objectFDSet;
		FD_ZERO(&objectFDSet);
		tv = timeOut;
		FD_SET(objectReadFD, &objectFDSet);
	
		if(select(objectReadFD + 1, &objectFDSet,0,0, &tv) > 0)
		{
			lcmState.handle();
		}	*/
		
		//puts VideoCapture object "capture" ints cameraFeed matrix
		capture >> cameraFeed;
		
		//gets system time
		SYSTEMTIME time;
		uint64_t systemTime;
		GetSystemTime(&time);
		systemTime = time.wMilliseconds + time.wSecond * 1000L + time.wMinute * 60000L + time.wHour * 3600000L + time.wDay * 86400000L;

		//declares arguements for our normalizedBGR algorithm
		Mat camChannels[3];
		Mat sumChannels;
		Mat normCamChannels[3];
		Mat normalizedCamChannels;
		split(cameraFeed, camChannels);
		sumChannels = camChannels[0] + camChannels[1] + camChannels[2];
		
		//normalizedBGR algorithm
		normCamChannels[0] = camChannels[0] / sumChannels * 255;
		normCamChannels[1] = camChannels[1] / sumChannels * 255;
		normCamChannels[2] = camChannels[2] / sumChannels * 255;
		
		//operation to turn 3 separate channels into a single 3 channel matrix
		merge(normCamChannels, 3, normalizedCamChannels);
		
		//blur the image to help remove outliers
		GaussianBlur(cameraFeed, cameraFeed, Size(25,25), 4, 4);

		//convert frames from normalized BGR to LUV colorspace
		cvtColor(cameraFeed, LUV, COLOR_BGR2Luv);
			
		rangeLUV blackRange, redRange, yellowRange, greenRange, blueRange, whiteRange; 
		
			blackRange.lMin = blackRangelMin;
			blackRange.lMax = blackRangelMax; 
			blackRange.uMin = blackRangeuMax;
			blackRange.uMax = blackRangeuMax;
			blackRange.vMin = blackRangevMax;
			blackRange.vMax = blackRangevMax;
		
			redRange.lMin = redRangelMin;
			redRange.lMax = redRangelMax;
			redRange.uMin = redRangeuMin;
			redRange.uMax = redRangeuMax;
			redRange.vMin = redRangevMin;
			redRange.vMax = redRangevMax;
			
			yellowRange.lMin = yellowRangelMin;
			yellowRange.lMax = yellowRangelMax;
			yellowRange.uMin = yellowRangeuMin;
			yellowRange.uMax = yellowRangeuMax;
			yellowRange.vMin = yellowRangevMin;
			yellowRange.vMax = yellowRangevMax;

			greenRange.lMin = greenRangelMin;
			greenRange.lMax = greenRangelMax;
			greenRange.uMin = greenRangeuMin;
			greenRange.uMax = greenRangeuMax; 
			greenRange.vMin = greenRangevMin;
			greenRange.vMax = greenRangevMax;
		
			blueRange.lMin = blueRangelMin;
			blueRange.lMax = blueRangelMax;
			blueRange.uMin = blueRangeuMin;
			blueRange.uMax = blueRangeuMax; 
			blueRange.vMin = blueRangevMin;
			blueRange.vMax = blueRangevMax;

			whiteRange.lMin = whiteRangelMin;
			whiteRange.lMax = whiteRangelMax;
			whiteRange.uMin = whiteRangeuMin;
			whiteRange.uMax = whiteRangeuMax;
			whiteRange.vMin = whiteRangevMin;
			whiteRange.vMax = whiteRangevMax;

		//thresholds image for specific colors, erodes and dilates images, and tracks objects
		//a block of code below can be copied, pasted, and changed for tracking of additional colors
			inRange(LUV, Scalar(blackRange.lMin, blackRange.lMin, blackRange.uMin), Scalar(blackRange.uMax, blackRange.vMax, blackRange.vMax), thresholdBlack);
			morphOpsBlack(thresholdBlack);
			trackFilteredObjectBlack(thresholdBlack, LUV, cameraFeed, systemTime);				

			inRange(LUV, Scalar(redRange.lMin, redRange.uMin, redRange.vMin), Scalar(redRange.lMax, redRange.uMax, redRange.vMax), thresholdRed);
			morphOps(thresholdRed);
			trackFilteredObjectRed(thresholdRed, LUV, cameraFeed, systemTime);

			inRange(LUV, Scalar(yellowRange.lMin, yellowRange.uMin, yellowRange.vMin), Scalar(yellowRange.lMax, yellowRange.uMax, yellowRange.vMax), thresholdYellow);
			morphOps(thresholdYellow);
			trackFilteredObjectYellow(thresholdYellow, LUV, cameraFeed, systemTime);
									
			inRange(LUV, Scalar(greenRange.lMin, greenRange.uMin, greenRange.vMin), Scalar(greenRange.lMax, greenRange.uMax, greenRange.vMax), thresholdGreen);
			morphOps(thresholdGreen);
			trackFilteredObjectGreen(thresholdGreen, LUV, cameraFeed, systemTime);
		
			inRange(LUV, Scalar(blueRange.lMin, blueRange.uMin, blueRange.vMin), Scalar(blueRange.lMax, blueRange.uMax, blueRange.vMax), thresholdBlue);
			morphOps(thresholdBlue);
			trackFilteredObjectBlue(thresholdBlue, LUV, cameraFeed, systemTime);

			inRange(LUV, Scalar(whiteRange.lMin, whiteRange.uMin, whiteRange.vMin), Scalar(whiteRange.lMax, whiteRange.uMax, whiteRange.vMax), thresholdWhite);
			morphOps(thresholdWhite);
			trackFilteredObjectWhite(thresholdWhite, LUV, cameraFeed, systemTime);

	
		
		//draws cirlces and on screen location about the center of each tracked object
		drawObject(objVec, cameraFeed);

		//tranfroms the on screen pixel space from the origin being in the top left corner to the center pixel.
		for(int i = 0; i < objVec.size(); i++)
		{
			objVec[i].xPos -= FRAME_WIDTH / 2;
			objVec[i].yPos = FRAME_HEIGHT / 2 - objVec[i].yPos;
		}
		
		//shows different stages of image filtering
		imshow(windowName, cameraFeed);
		imshow(windowName1, LUV);
//		imshow(windowName2, thresholdBlack);
//		imshow(windowName3, thresholdRed);
//		imshow(windowName4, thresholdYellow);
//		imshow(windowName5, thresholdGreen);
//		imshow(windowName6, thresholdBlue);
//		imshow(windowName7, thresholdWhite);
//		imshow(windowName8, normalizedCamChannels);

		//uses trackbars so that we can determine appropriate LUV values for desired colors
		if(calibrationMode)
		{
			Mat thresholdTestLUV;
			inRange(LUV, Scalar(L_MIN, U_MIN, V_MIN), Scalar(L_MAX, U_MAX, V_MAX), thresholdTestLUV);
			imshow(windowName9, thresholdTestLUV);
		}		
		
		//defines argument ObjList for passing through LCM
		objList.numObjects = objVec.size();
		objList.list = objVec;

		//ensures that information is sent through LCM (to spotter) only if there is an object detected
		if (objVec.size()>0)
		{
			lcm.publish("OBJECT", &objList);
		}

		//waits for the "escape" key to be pressed for 27 ms, if it is, it breaks the loop
		if (waitKey(30) == 27) 
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	return 0;
}//end of main

//Definition for state handle
void StateHandler::handleMessage(const lcm::ReceiveBuffer* rbuf,
                const std::string& chan, 
                const RB::state* msg)
{
	cout << "in handler" << endl << (int)msg->objective_number << endl;
	state.mission_number = (int)msg->mission_number;
	state.task_number = (int)msg->task_number;
	state.objective_number = (int)msg->objective_number;

}


//definition for arguement intToString of class string for use in drawObject
string intToString(int number)
{
	stringstream ss;
	ss << number;
	return ss.str();
}

//definition for morphOps function
void morphOps(Mat &thresh)
{
	//the element chosen here is a 4px by 4px rectangle, it will be used 
	Mat erodeElement = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
	
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_ELLIPSE, Size(6, 6));

	//shrinks the thresholded image
	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	//enlarges the remaining thresholded image
	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
}


void morphOpsBlack(Mat &thresh)
{
	//the element chosen here is a 4px by 4px rectangle, it will be used 
	Mat erodeElement = getStructuringElement(MORPH_ELLIPSE, Size(1, 1));
	
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_ELLIPSE, Size(2, 2));

	//shrinks the thresholded image
	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	//enlarges the remaining thresholded image
	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
}

//definition of drawObject
//this function will be called to actually draw the tracker on a specified object at a found location
void drawObject(vector<RB::Object> objVec, Mat &cameraFeed)
{
	for (int i=0; i<objVec.size(); i++)
	{
		int centralX = objVec[i].xPos;
		int centralY = objVec[i].yPos;
		//alogrithm to change origin from top left corner of screen to the center, required for spotter algorithm	
		centralX -= FRAME_WIDTH/2;
		centralY = FRAME_HEIGHT/2 - centralY;
	
		//allows for tracking of multiple color objects using cases
		//more colors can be tracked by adding another case here
		//additions must also be made to enumeration, "trackFilteredObject", rangeLUV arguements, and "inRange... code"
		switch (objVec[i].type)
		{
			case BLACK_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 0));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Black:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case RED_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Red:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case YELLOW_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 255, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Yellow:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case GREEN_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(64, 255, 0));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Green:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));		
			break;
			case BLUE_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 255, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Blue:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case WHITE_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(255, 255, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "White:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
		}
	}
}

//used for changes to trackbar values
void on_trackbarLUV(int, void*)
{
}

//definition of createTrackbars functions
void createTrackbarsLUV()
{
	//create window for LUV trackbars
	namedWindow(trackbarWindowNameLUV, CV_WINDOW_AUTOSIZE);
	
	//create memory to store trackbar name on window
	char TrackbarNameLUV[50];
	sprintf(TrackbarNameLUV, "L_MIN", L_MIN);
	sprintf(TrackbarNameLUV, "L_MAX", L_MAX);
	sprintf(TrackbarNameLUV, "U_MIN", U_MIN);
	sprintf(TrackbarNameLUV, "U_MAX", U_MAX);
	sprintf(TrackbarNameLUV, "V_MIN", V_MIN);
	sprintf(TrackbarNameLUV, "V_MAX", V_MAX);
	

	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.L_MIN),
	//the max value the trackbar can move (eg. L_MAX), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbarLUV)
	createTrackbar("L_MIN", trackbarWindowNameLUV, &L_MIN, L_MAX, on_trackbarLUV);
	createTrackbar("L_MAX", trackbarWindowNameLUV, &L_MAX, L_MAX, on_trackbarLUV);
	createTrackbar("U_MIN", trackbarWindowNameLUV, &U_MIN, U_MAX, on_trackbarLUV);
	createTrackbar("U_MAX", trackbarWindowNameLUV, &U_MAX, U_MAX, on_trackbarLUV);
	createTrackbar("V_MIN", trackbarWindowNameLUV, &V_MIN, V_MAX, on_trackbarLUV);
	createTrackbar("V_MAX", trackbarWindowNameLUV, &V_MAX, V_MAX, on_trackbarLUV);
}

//function for tracking black objects
void trackFilteredObjectBlack(Mat thresholdBlack, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	//sets up temporary location for thresholded image
	Mat temp;
	thresholdBlack.copyTo(temp);
	
	//these two vectors are needed for output of findContours
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//find contours of filtered image using openCV findContours function
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	//use moments method to find our filtered object
	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		//ensures we are not tracking too many objects due to a noisy filter
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
				//openCV class "Moments" for information about location and size
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				
				//thresholds out small objects
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}
					
					//sets x position and y position of the moment of the buoy into "black" Object
					RB::Object black;
					
					black.xPos = moment.m10 / area;
					black.yPos = lowest;

					//Gets the UTC time for all black objects found
					black.timeStamp = systemTime;
					
					//defines object type
					black.type = BLACK_BUOY;
					
					//populates the vector "black" with the data from each Buoy "black"
					objVec.push_back(black);
					objectFound = true;
				}
				else objectFound = false;
			}

		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectRed(Mat thresholdRed, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdRed.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
			
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
			
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}

					RB::Object red;
					red.xPos = moment.m10 / area;
					red.yPos = moment.m01/area;// lowest;
					
					red.timeStamp = systemTime;
				
					red.type = RED_BUOY;
					
					objVec.push_back(red);
					objectFound = true;
				}
			
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectYellow(Mat thresholdYellow, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdYellow.copyTo(temp);

	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
				
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}

					RB::Object yellow;
					yellow.xPos = moment.m10 / area;
					yellow.yPos = lowest;

					yellow.timeStamp = systemTime;
				
					yellow.type = YELLOW_BUOY;
					
					objVec.push_back(yellow);
					objectFound = true;
				}
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectGreen(Mat thresholdGreen, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdGreen.copyTo(temp);

	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0)
	{
		int numObjects = hierarchy.size();
	
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0])
			{
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}

					RB::Object green;
					green.xPos = moment.m10 / area;
					green.yPos = lowest;

					green.timeStamp = systemTime;

					green.type = GREEN_BUOY;
	
					objVec.push_back(green);
					objectFound = true;
				}
				else objectFound = false;
			}
	
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectBlue(Mat thresholdBlue, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdBlue.copyTo(temp);

	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
				
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}

					RB::Object blue;
					blue.xPos = moment.m10 / area;
					blue.yPos = lowest;

					blue.timeStamp = systemTime;
				
					blue.type = BLUE_BUOY;
					
					objVec.push_back(blue);
					objectFound = true;
				}
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectWhite(Mat thresholdWhite, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdWhite.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
	
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
		
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				
				if (area>MIN_OBJECT_AREA_WHITE && area<MAX_OBJECT_AREA_WHITE)
				{
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}

					RB::Object white;
					white.xPos = moment.m10 / area;
					white.yPos = lowest;

					white.timeStamp = systemTime;
					
					white.type = WHITE_BUOY;
					
					objVec.push_back(white);
					objectFound = true;
				}
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}