/*This code is the intellectual property of Travis Moscicki.
Any duplication by Harold Davis is hereby punishable by death.*/

#include "stdafx.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char* argv)
{
	VideoCapture cap(0); // open the video camera no. 0

	if (!cap.isOpened())  // if not success, exit program
	{
		cout << "Cannot open the video file" << endl;
		return -1;
	}


	while (true)
	{
		Mat imgOriginal;

		bool bSuccess = cap.read(imgOriginal);//reads a new file from video

		imshow("Og", imgOriginal);

		//splits imgOriginal into blue, green, and red channels	
		Mat bgrChannels[3];
		split(imgOriginal, bgrChannels);
		
		Mat imgHSVBlue; 
		Mat imgHSVGreen;
		Mat imgHSVRed;
		
		//performs an HSV filter on channel
		cvtColor(bgrChannels[0], imgHSVBlue, CV_BGR2HSV);
		cvtColor(bgrChannels[1], imgHSVGreen, CV_BGR2HSV);
		cvtColor(bgrChannels[2], imgHSVRed, CV_BGR2HSV);

		//separates blue HSV image into hue, saturation, and value
		Mat blueHSVChannels[3];
		split(imgHSVBlue, blueHSVChannels);

		//separates green HSV image into hue, saturation, and value
		Mat greenHSVChannels[3];
		split(imgHSVGreen, greenHSVChannels);
		
		//separates red HSV image into hue, saturation, and value
		Mat redHSVChannels[3];
		split(imgHSVRed, redHSVChannels);

		//Creates a channel for blue chromaticity
		Mat bChroma = (blueHSVChannels[2] / (blueHSVChannels[2] + greenHSVChannels[2] + redHSVChannels[2]));
		//Creates a channel for green chromaticity
		Mat gChroma = (greenHSVChannels[2] / (blueHSVChannels[2] + greenHSVChannels[2] + redHSVChannels[2]));
		//Creates a channel for red chromaticity
		Mat rChroma = (redHSVChannels[2] / (blueHSVChannels[2] + greenHSVChannels[2] + redHSVChannels[2]));

		//combanines the "V" components into one image
		Mat rbChromaticity(imgOriginal.rows, imgOriginal.cols, CV_8UC3);
		Mat in[] = { bChroma, gChroma, rChroma };
		int from_to[] = { 0, 0, 1, 1, 2, 2 };
		mixChannels(in, 3, &rbChromaticity, 1, from_to, 3);

		imshow("Chroma", rbChromaticity);
			
		//Mat imgThresholded;
		
		//inRange(Chroma, 0, 1, imgThresholded);
		//imshow("Thresholded", imgThresholded);

		//dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_RECT, Size(3, 3)));
		
		if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	return 0;
}