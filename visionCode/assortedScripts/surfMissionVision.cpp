//Travis Moscicki 2014
//Florida Atlantic University College of Engineering and Computer Science
//AUVSI RoboBoat Competition

//This program is writen for the purpose of identifying multiple objects of both the same color and different colors in an outdoor setting with both
//high and dynamic lighting.  A buoy class is introced that allows us to populate a vector of this class of objects.
//We use two layers of filtration.  The first is normalizedBGR, which makes colors more vibrant and uniform.  The second is chromaticity (YCrCb) which 
//is designed to filter out changes in inensity of light.

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

#include <ctime>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <stdint.h>
#include <windows.h>
#include <inttypes.h>
#include <valarray>

#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <opencv2\imgproc\imgproc.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"

#include <lcm/lcm-cpp.hpp>

#include "ObjectList.hpp"
#include "state.hpp"
#include "uhuraOut.hpp"



using namespace cv;
using namespace std;

RB::state State;

class StateHandler
{
public:
	~StateHandler(){}
	void handleMessage(const lcm::ReceiveBuffer* rbuf,
                const std::string& chan, 
                const RB::state* msg);
	RB::state state;	
};

//this functions sets up our trackbars and can either be enable or disabled
void createTrackbarsLUV();

//range for L, U, and V values van be changed by enabling trackbars
int L_MIN = 0;
int L_MAX = 255;
int U_MIN = 0;
int U_MAX = 255;
int V_MIN = 0;
int V_MAX = 255;
//RB_state state;
//This function gets called whenever a trackbar position is changed
void on_trackbarLUV(int, void*);

//default capture width and height
const int FRAME_WIDTH = 1080;
const int FRAME_HEIGHT = 640;

//Matrixes to store each frame of the webcam feed, thresholded images, as well as final YCrCb image
Mat cameraFeed;
Mat cameraFeedDown;
Mat thresholdBlack;
Mat thresholdRed;
Mat thresholdRed2;
Mat thresholdRed3;
Mat thresholdYellow;
Mat thresholdGreen;
Mat thresholdGreen2;
Mat thresholdGreen3;
Mat thresholdWhite;
Mat LUV;

//max number of objects to be detected in frame
const int MAX_NUM_OBJECTS = 1000;

//minimum and maximum object area 
//we set a minimum to filter out noise
//we set a maximum to ensure that only objects the size of buoys are tracked 
const int MIN_OBJECT_AREA1 = 100;
const int MAX_OBJECT_AREA1 = 4000;

const int MIN_OBJECT_AREA2 = 1000;
const int MAX_OBJECT_AREA2 = 40000;


//names that will appear at the top of each window
const string windowName = "Original Image";
const string windowName1 = "LUV Image";
const string windowName5 = "Black Thresholding";
const string windowName3 = "Red Thresholding";
const string windowName4 = "Yellow Thresholding";
const string windowName2 = "Green Thresholding";
const string windowName6 = "White Thresholding";
const string windowName7 = "Normalized BGR";
const string windowName8 = "LUV Threshold Tester";
const string trackbarWindowNameLUV = "LUV Trackbars";

//class to allow for easy changes of LUV values
class rangeLUV 
{
	public:
		uint16_t lMin;
		uint16_t lMax;
		uint16_t uMin;
		uint16_t uMax;
		uint16_t vMin;
		uint16_t vMax;
};

//sets types to make changes in drawObject function easier and for passing through LCM
enum objectType {BLACK_BUOY = 1, RED_BUOY, YELLOW_BUOY, GREEN_BUOY,  WHITE_BUOY, RED_BUOY2, GREEN_BUOY2};

//initializes a vector called objVec made of "Object" class objects, this is what we will pass through LCM to the spotter algorithm
vector<RB::Object> objVec;

//initializes arguement objList of class ObjectList
RB::ObjectList objList;

//protoype for the function that will draw circles and location on dectected objects
void drawObject(vector<RB::Object> objVec, Mat &cameraFeed);

//create structuring element that will be used to dilate (enlarge) and erode (shrink) image.
//this is useful in destroying outliers common with imperfect filtration
void morphOps(Mat &thresh);
void morphOpsBlack(Mat &thresh);

//these are the prototypes for our algorithm that will track filtered objects
//this is repeated five times for five different colored objects
//if more objects are to be tracked, they must be initialized here, added to the enumeration of "objectType", and defined below
void trackFilteredObjectBlack(Mat thresholdBlack, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectRed(Mat thresholdRed, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectRed2(Mat thresholdRed2, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectYellow(Mat thresholdYellow, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectGreen(Mat thresholdGreen, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectGreen2(Mat thresholdGreen2, Mat LUV, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectWhite(Mat thresholdWhite, Mat LUV, Mat &cameraFeed, uint64_t systemTime);

//protoype for int to string
string intToString(int number);

//beginning of our main function
int main(int argc, char* argv[])
{
	//creates and verifies our LCM channel for interprocess communication
	lcm::LCM lcm = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!lcm.good())
	{
		printf("failed to create lcm");
		return 1;
	}

	lcm::LCM lcmState = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!lcmState.good())
	{
		printf("failed to create lcmState");
		return 1;
	}

	lcm::LCM lcmUhura = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!lcmUhura.good())
	{
		printf("failed to create lcmState");
		return 1;
	}
	StateHandler stateHandler;

	lcmState.subscribe("MIS_STATE", &StateHandler::handleMessage, &stateHandler);
	lcmUhura.subscribe("UHURA_OUT", &StateHandler::handleMessage, &stateHandler);

	lcmUhura.handle();

	RB::state state;

	struct timeval tv;
	struct timeval timeOut;
	timeOut.tv_sec = 0;
	timeOut.tv_usec = 50000;

	//if we would like to calibrate our filter values, set to true.
	bool calibrationMode = false;
	if (calibrationMode)
	{
		//create slider bars for YCrCb filtering
		createTrackbarsLUV();
	}
	//video capture object to acquire webcam feed
	VideoCapture capture;
	VideoCapture captureDown;

	//open capture object at location zero (default location for webcam)
	capture.open(0);
	captureDown.open(1);

	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);

	//start an infinite loop where webcam feed is copied to cameraFeed matrix
	//all of our operations will be performed within this loop
	while (1)
	{

		//clears out vector between loops
		objVec.clear();

		int32_t objectReadFD = lcmState.getFileno();
		fd_set objectFDSet;
		FD_ZERO(&objectFDSet);
		tv = timeOut;
		FD_SET(objectReadFD, &objectFDSet);
	
		if(select(objectReadFD + 1, &objectFDSet,0,0, &tv) > 0)
		{
			lcmState.handle();
		}	
		
		//selects which image to work with, forward facing or down, depending on which object and task STATE is in
		if (state.objective_number==1 && state.task_number== 5)
		{
			captureDown.read(cameraFeed);
		}
		else capture.read(cameraFeed);
		
		SYSTEMTIME time;
		uint64_t systemTime;
		GetSystemTime(&time);
		systemTime = time.wMilliseconds + time.wSecond * 1000L + time.wMinute * 60000L + time.wHour * 3600000L + time.wDay * 86400000L;

		//declares arguements for our normalizedBGR algorithm
		Mat camChannels[3];
		Mat sumChannels;
		Mat normCamChannels[3];
		Mat normalizedCamChannels;
		split(cameraFeed, camChannels);
		sumChannels = camChannels[0] + camChannels[1] + camChannels[2];
		
		//normalizedBGR algorithm
		normCamChannels[0] = camChannels[0] / sumChannels * 255;
		normCamChannels[1] = camChannels[1] / sumChannels * 255;
		normCamChannels[2] = camChannels[2] / sumChannels * 255;
		
		//operation to turn 3 separate channels into a single 3 channel matrix
		merge(normCamChannels, 3, normalizedCamChannels);

		//convert frames from normalized BGR to LUV colorspace
		cvtColor(normalizedCamChannels, LUV, COLOR_BGR2Luv);
		
		//blur the image to help remove outliers
		GaussianBlur(LUV, LUV, Size(25,25), 1.5, 1.5);

		//where to set values for thresholding, one set for forward facing tasks and one for downward facing task
		rangeLUV blackRange, redRange, redRange2, greenRange, greenRange2, yellowRange, whiteRange; 

		if (state.objective_number == 1 && state.task_number == 8)
		{
			RB::uhuraOut dockObj;
			if (dockObj.dockObj == 't')
			{
				Mat triangle = imread( "triangle.png", CV_LOAD_IMAGE_GRAYSCALE );
	 
				if( !triangle.data )
				{
					std::cout<< "Error reading triangle " << std::endl;
					return -1;
				}
	
				int minHessian = 500;
				//Detect the keypoints using SURF Detector for triangle
				SurfFeatureDetector detector( minHessian );
				std::vector<KeyPoint> kpTriangle;
				detector.detect( triangle, kpTriangle );

				//Calculate descriptors (feature vectors)
				SurfDescriptorExtractor extractor;
				Mat desTriangle;

				extractor.compute( triangle, kpTriangle, desTriangle );

				FlannBasedMatcher matcher;

				VideoCapture cap(0);
		
				std::vector<Point2f> triangle_corners(4);
		  
				//Get the corners from the triangle
				triangle_corners[0] = cvPoint(0,0);
				triangle_corners[1] = cvPoint( triangle.cols, 0 );
				triangle_corners[2] = cvPoint( triangle.cols, triangle.rows );
				triangle_corners[3] = cvPoint( 0, triangle.rows );
		
				char key = 'a';
				int framecount = 0;
		
				while (key != 27)
				{
					Mat frame;
					cap >> frame;

					if (framecount < 5)
					{
						framecount++;
						continue;
					}
		
					//initializations for cross
					Mat desImageTriangle, imgMatchesTriangle;
					std::vector<KeyPoint> kpImageTriangle;
					std::vector<vector<DMatch > > matchesTriangle;
					std::vector<DMatch > goodMatchesTriangle;
					std::vector<Point2f> objTriangle;
					std::vector<Point2f> sceneTriangle;
					std::vector<Point2f> sceneCornersTriangle(4);
					Mat hTriangle;
					Mat imageTriangle;		
			
					cvtColor(frame, imageTriangle, CV_RGB2Luv);
		
					detector.detect( imageTriangle, kpImageTriangle );
					extractor.compute( imageTriangle, kpImageTriangle, desImageTriangle );
			
					matcher.knnMatch(desTriangle, desImageTriangle, matchesTriangle, 2);
					for(int i = 0; i < min(desImageTriangle.rows-1,(int) matchesTriangle.size()); i++) //THIS LOOP IS SENSITIVE TO SEGFAULTS
					{
						if((matchesTriangle[i][0].distance < 0.6*(matchesTriangle[i][1].distance)) && ((int) matchesTriangle[i].size()<=2 && (int) matchesTriangle[i].size()>0))
						{
							goodMatchesTriangle.push_back(matchesTriangle[i][0]);
						}
					}		

					drawMatches( triangle, kpTriangle, triangle, kpImageTriangle, goodMatchesTriangle, imgMatchesTriangle, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
      
					if (goodMatchesTriangle.size() >= 4)
					{
						for( int i = 0; i < goodMatchesTriangle.size(); i++ )
						{
							//Get the keypoints from the good matches
							objTriangle.push_back( kpTriangle[ goodMatchesTriangle[i].queryIdx ].pt );
							sceneTriangle.push_back( kpImageTriangle[ goodMatchesTriangle[i].trainIdx ].pt );
						}

						hTriangle = findHomography( objTriangle, sceneTriangle, CV_RANSAC );

						perspectiveTransform( triangle_corners, sceneCornersTriangle, hTriangle);

						//Draw lines between the corners (the mapped object in the scene image )
						line( imgMatchesTriangle, sceneCornersTriangle[0] + Point2f( triangle.cols, 0), sceneCornersTriangle[1] + Point2f( triangle.cols, 0), Scalar(0, 255, 0), 4 );
						line( imgMatchesTriangle, sceneCornersTriangle[1] + Point2f( triangle.cols, 0), sceneCornersTriangle[2] + Point2f( triangle.cols, 0), Scalar( 0, 255, 0), 4 );
						line( imgMatchesTriangle, sceneCornersTriangle[2] + Point2f( triangle.cols, 0), sceneCornersTriangle[3] + Point2f( triangle.cols, 0), Scalar( 0, 255, 0), 4 );
						line( imgMatchesTriangle, sceneCornersTriangle[3] + Point2f( triangle.cols, 0), sceneCornersTriangle[0] + Point2f( triangle.cols, 0), Scalar( 0, 255, 0), 4 );
					}
				imshow( "Good Matches Triangle", imgMatchesTriangle );
				}
			}
			else if (dockObj.dockObj == 'x')
			{		
				Mat cross = imread( "cross.png", CV_LOAD_IMAGE_GRAYSCALE );
		
				if( !cross.data )
				{
					std::cout<< "Error reading cross " << std::endl;
					return -1;
				}		
		
				int minHessian = 500;
				//Detect the keypoints using SURF Detector for cross	
				SurfFeatureDetector detector( minHessian );
				std::vector<KeyPoint> kpCross;
				detector.detect( cross, kpCross );
		
				//Calculate descriptors (feature vectors)
				SurfDescriptorExtractor extractor;
				Mat desCross;

				extractor.compute( cross, kpCross, desCross );
	    
				FlannBasedMatcher matcher;

				VideoCapture cap(0);

				namedWindow("Good Matches Cross", CV_WINDOW_NORMAL);	

				std::vector<Point2f> cross_corners(4);
		
				//Get the corners from the cross
				cross_corners[0] = cvPoint(0,0);
				cross_corners[1] = cvPoint( cross.cols, 0 );
				cross_corners[2] = cvPoint( cross.cols, cross.rows );
				cross_corners[3] = cvPoint( 0, cross.rows );		
		
				char key = 'a';
				int framecount = 0;
				while (key != 27)
				{
					Mat frame;
					cap >> frame;

					if (framecount < 5)
					{
						framecount++;
						continue;
					}
			
					//initializations for triangles
					Mat desImageCross, imgMatchesCross;
					std::vector<KeyPoint> kpImageCross;
					std::vector<vector<DMatch > > matchesCross;
					std::vector<DMatch > goodMatchesCross;
					std::vector<Point2f> objCross;
					std::vector<Point2f> sceneCross;
					std::vector<Point2f> sceneCornersCross(4);
					Mat hCross;
					Mat imageCross;
			
					cvtColor(frame, imageCross, CV_RGB2Luv);
					detector.detect( imageCross, kpImageCross );
					extractor.compute( imageCross, kpImageCross, desImageCross ); 
					matcher.knnMatch(desCross, desImageCross, matchesCross, 2);
					for(int i = 0; i < min(desImageCross.rows-1,(int) matchesCross.size()); i++) //THIS LOOP IS SENSITIVE TO SEGFAULTS
						{
							if((matchesCross[i][0].distance < 0.6*(matchesCross[i][1].distance)) && ((int) matchesCross[i].size()<=2 && (int) matchesCross[i].size()>0))
							{
								goodMatchesCross.push_back(matchesCross[i][0]);
							}
						}
 
					drawMatches( cross, kpCross, cross, kpImageCross, goodMatchesCross, imgMatchesCross, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
       
					if (goodMatchesCross.size() >= 4)
					{
						for( int i = 0; i < goodMatchesCross.size(); i++ )
						{
							//Get the keypoints from the good matches
							objCross.push_back( kpCross [ goodMatchesCross[i].queryIdx ].pt );
							sceneCross.push_back( kpImageCross[ goodMatchesCross[i].trainIdx ].pt );
						}

						hCross = findHomography( objCross, sceneCross, CV_RANSAC );

						perspectiveTransform( cross_corners, sceneCornersCross, hCross);

						//Draw lines between the corners (the mapped object in the scene image )
						line( imgMatchesCross, sceneCornersCross[0] + Point2f( cross.cols, 0), sceneCornersCross[1] + Point2f( cross.cols, 0), Scalar(0, 255, 0), 4 );
						line( imgMatchesCross, sceneCornersCross[1] + Point2f( cross.cols, 0), sceneCornersCross[2] + Point2f( cross.cols, 0), Scalar( 0, 255, 0), 4 );
						line( imgMatchesCross, sceneCornersCross[2] + Point2f( cross.cols, 0), sceneCornersCross[3] + Point2f( cross.cols, 0), Scalar( 0, 255, 0), 4 );
						line( imgMatchesCross, sceneCornersCross[3] + Point2f( cross.cols, 0), sceneCornersCross[0] + Point2f( cross.cols, 0), Scalar( 0, 255, 0), 4 );
					}
					imshow( "Good Matches Cross", imgMatchesCross);
				}
			}
			else if ( dockObj.dockObj == 'c')
			{
				Mat circle = imread( "circle.png", CV_LOAD_IMAGE_GRAYSCALE );

				if( !circle.data )
				{
					std::cout<< "Error reading circle " << std::endl;
					return -1;
				}

				int minHessian = 500;

				//Detect the keypoints using SURF Detector for circle	
				SurfFeatureDetector detector( minHessian );
				std::vector<KeyPoint> kpCircle;

				detector.detect( circle, kpCircle );

				//Calculate descriptors (feature vectors)
				SurfDescriptorExtractor extractor;
				Mat desCircle;

				extractor.compute( circle, kpCircle, desCircle );
			
				FlannBasedMatcher matcher;

				VideoCapture cap(0);

				namedWindow("Good Matches Circle", CV_WINDOW_NORMAL);

				std::vector<Point2f> circle_corners(4);

				//Get the corners from the triangle
				circle_corners[0] = cvPoint(0,0);
				circle_corners[1] = cvPoint( circle.cols, 0 );
				circle_corners[2] = cvPoint( circle.cols, circle.rows );
				circle_corners[3] = cvPoint( 0, circle.rows );

				char key = 'a';
				int framecount = 0;
				while (key != 27)
				{
					Mat frame;
					cap >> frame;

					if (framecount < 5)
					{
						framecount++;
						continue;
					}

					//initializations for circle
					Mat desImageCircle, imgMatchesCircle;
					std::vector<KeyPoint> kpImageCircle;
					std::vector<vector<DMatch > > matchesCircle;
					std::vector<DMatch > goodMatchesCircle;
					std::vector<Point2f> objCircle;
					std::vector<Point2f> sceneCircle;
					std::vector<Point2f> sceneCornersCircle(4);
					Mat hCircle;
					Mat imageCircle;

					cvtColor(frame, imageCircle, CV_RGB2Luv);

					detector.detect( imageCircle, kpImageCircle );
					extractor.compute( imageCircle, kpImageCircle, desImageCircle );
				
					matcher.knnMatch(desCircle, desImageCircle, matchesCircle, 2);
			
					for(int i = 0; i < min(desImageCircle.rows-1,(int) matchesCircle.size()); i++) //THIS LOOP IS SENSITIVE TO SEGFAULTS
					{
						if((matchesCircle[i][0].distance < 0.6*(matchesCircle[i][1].distance)) && ((int) matchesCircle[i].size()<=2 && (int) matchesCircle[i].size()>0))
						{
							goodMatchesCircle.push_back(matchesCircle[i][0]);
						}
					}

					//Draw only "good" matches
					drawMatches( circle, kpCircle, circle, kpImageCircle, goodMatchesCircle, imgMatchesCircle, Scalar::all(-1), Scalar::all(-1), vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
	 
					if (goodMatchesCircle.size() >= 4)
					{
						for( int i = 0; i < goodMatchesCircle.size(); i++ )
						{
							//Get the keypoints from the good matches
							objCircle.push_back( kpCircle[ goodMatchesCircle[i].queryIdx ].pt );
							sceneCircle.push_back( kpImageCircle[ goodMatchesCircle[i].trainIdx ].pt );
						}

						hCircle = findHomography( objCircle, sceneCircle, CV_RANSAC );

						perspectiveTransform( circle_corners, sceneCornersCircle, hCircle);

						//Draw lines between the corners (the mapped object in the scene image )
						line( imgMatchesCircle, sceneCornersCircle[0] + Point2f( circle.cols, 0), sceneCornersCircle[1] + Point2f( circle.cols, 0), Scalar(0, 255, 0), 4 );
						line( imgMatchesCircle, sceneCornersCircle[1] + Point2f( circle.cols, 0), sceneCornersCircle[2] + Point2f( circle.cols, 0), Scalar( 0, 255, 0), 4 );
						line( imgMatchesCircle, sceneCornersCircle[2] + Point2f( circle.cols, 0), sceneCornersCircle[3] + Point2f( circle.cols, 0), Scalar( 0, 255, 0), 4 );
						line( imgMatchesCircle, sceneCornersCircle[3] + Point2f( circle.cols, 0), sceneCornersCircle[0] + Point2f( circle.cols, 0), Scalar( 0, 255, 0), 4 );
					}
					imshow( "Good Matches Circle", imgMatchesCircle );
				}
				key = waitKey(1);
			}
		}
		

		else if(state.objective_number==1 && state.task_number == 4)
		{
			blackRange.lMin = 0;
			blackRange.lMax = 93;
			blackRange.uMin = 57;
			blackRange.uMax = 117;
			blackRange.vMin = 0;
			blackRange.vMax = 143;
		
			redRange.lMin = L_MIN;
			redRange.lMax = L_MAX;
			redRange.uMin = 126;
			redRange.uMax = U_MAX;
			redRange.vMin = 0;
			redRange.vMax = V_MAX;

			yellowRange.lMin = L_MIN;
			yellowRange.lMax = L_MAX;
			yellowRange.uMin = 0;
			yellowRange.uMax = 110;
			yellowRange.vMin = 177;
			yellowRange.vMax = 255;

			greenRange.lMin = 0;
			greenRange.lMax = 255;
			greenRange.uMin = 0;
			greenRange.uMax = 87;
			greenRange.vMin = 138;
			greenRange.vMax = 255;

			whiteRange.lMin = 0;
			whiteRange.lMax = 0;
			whiteRange.uMin = 0;
			whiteRange.uMax = 0;
			whiteRange.vMin = 0;
			whiteRange.vMax = 0;
		}
		else
		{
			blackRange.lMin = 0;
			blackRange.lMax = 0; 
			blackRange.uMin = 0;
			blackRange.uMax = 0;
			blackRange.vMin = 0;
			blackRange.vMax = 0;
		
			redRange.lMin = L_MIN;
			redRange.lMax = L_MAX;
			redRange.uMin = 118;
			redRange.uMax = 255;
			redRange.vMin = 0;
			redRange.vMax = 255;

			redRange2.lMin = L_MIN;
			redRange2.lMax = L_MAX;
			redRange2.uMin = 118;
			redRange2.uMax = U_MAX;
			redRange2.vMin = 0;
			redRange2.vMax = V_MAX;
			
			yellowRange.lMin = 0;
			yellowRange.lMax = 255;
			yellowRange.uMin = 84;
			yellowRange.uMax = 112;
			yellowRange.vMin = 193;
			yellowRange.vMax = 255;

			greenRange.lMin = 0;
			greenRange.lMax = 255;
			greenRange.uMin = 51;
			greenRange.uMax = 90; 
			greenRange.vMin = 130;
			greenRange.vMax = 255;
			
			greenRange2.lMin = 0;
			greenRange2.lMax = 255;
			greenRange2.uMin = 51;
			greenRange2.uMax = 90;
			greenRange2.vMin = 130;
			greenRange2.vMax = 255;

			whiteRange.lMin = 250;
			whiteRange.lMax = 255;
			whiteRange.uMin = 0;
			whiteRange.uMax = 255;
			whiteRange.vMin = 0;
			whiteRange.vMax = 255;
		}
						
		//thresholds image for specific colors, erodes and dilates images, and tracks objects
		//a block of code below can be copied, pasted, and changed for tracking of additional colors
		inRange(LUV, Scalar(blackRange.lMin, blackRange.lMin, blackRange.uMin), Scalar(blackRange.uMax, blackRange.vMax, blackRange.vMax), thresholdBlack);
		morphOpsBlack(thresholdBlack);
		trackFilteredObjectBlack(thresholdBlack, LUV, cameraFeed, systemTime);				

		inRange(LUV, Scalar(redRange.lMin, redRange.uMin, redRange.vMin), Scalar(redRange.lMax, redRange.uMax, redRange.vMax), thresholdRed);
		morphOps(thresholdRed);
		trackFilteredObjectRed(thresholdRed, LUV, cameraFeed, systemTime);		

		inRange(LUV, Scalar(redRange2.lMin, redRange2.uMin, redRange2.vMin), Scalar(redRange2.lMax, redRange2.uMax, redRange2.vMax), thresholdRed2);
		morphOps(thresholdRed2);
		trackFilteredObjectRed2(thresholdRed2, LUV, cameraFeed, systemTime);	
	
		inRange(LUV, Scalar(redRange.lMin, yellowRange.uMin, yellowRange.vMin), Scalar(yellowRange.lMax, yellowRange.uMax, yellowRange.vMax), thresholdYellow);	
		morphOps(thresholdYellow);
		trackFilteredObjectYellow(thresholdYellow, LUV, cameraFeed, systemTime);
					
		inRange(LUV, Scalar(greenRange.lMin, greenRange.uMin, greenRange.vMin), Scalar(greenRange.lMax, greenRange.uMax, greenRange.vMax), thresholdGreen);
		morphOps(thresholdGreen);
		trackFilteredObjectGreen(thresholdGreen, LUV, cameraFeed, systemTime);
							
		inRange(LUV, Scalar(greenRange2.lMin, greenRange2.uMin, greenRange2.vMin), Scalar(greenRange2.lMax, greenRange2.uMax, greenRange2.vMax), thresholdGreen2);
		morphOps(thresholdGreen2);
		trackFilteredObjectGreen2(thresholdGreen2, LUV, cameraFeed, systemTime);
		
		inRange(LUV, Scalar(whiteRange.lMin, whiteRange.uMin, whiteRange.vMin), Scalar(whiteRange.lMax, whiteRange.uMax, whiteRange.vMax), thresholdWhite);
		morphOps(thresholdWhite);
		trackFilteredObjectWhite(thresholdWhite, LUV, cameraFeed, systemTime);
		
		//draws cirlces and on screen location about the center of each tracked object
		drawObject(objVec, cameraFeed);

		//tranfroms the on screen pixel space from the origin being in the top left corner to the center pixel.
		for(int i = 0; i < objVec.size(); i++)
		{
			objVec[i].xPos -= FRAME_WIDTH / 2;
			objVec[i].yPos = FRAME_HEIGHT / 2 - objVec[i].yPos;
		}
		
		//shows different stages of image filtering
		imshow(windowName, cameraFeed);
//		imshow(windowName1, LUV);
//		imshow(windowName5, thresholdBlack);
//		imshow(windowName3, thresholdRed);
//		imshow("Threshold Red 2", thresholdRed2);
//		imshow(windowName4, thresholdYellow);
//		imshow(windowName2, thresholdGreen);
//		imshow("Threshold Green 2", thresholdGreen2);
//		imshow(windowName6, thresholdWhite);
//		imshow(windowName7, normalizedCamChannels);

		//uses trackbars so that we can determine appropriate YCrCb values for deried colors
		if(calibrationMode)
		{
			Mat thresholdTestLUV;
			inRange(LUV, Scalar(L_MIN, U_MIN, V_MIN), Scalar(L_MAX, U_MAX, V_MAX), thresholdTestLUV);
			imshow(windowName8, thresholdTestLUV);
		}		
		
		//defines argument ObjList for passing through LCM
		objList.numObjects = objVec.size();
		objList.list = objVec;

		//ensures that information is sent through LCM only if there is an object detected
		if (objVec.size()>0)
		{
			lcm.publish("OBJECT", &objList);
		}

		//waits for the "escape" key to be pressed for 27 ms, if it is, it breaks the loop
		if (waitKey(30) == 27) 
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
//	lcm_destroy(lcmState);
	return 0;
}//end of main


void StateHandler::handleMessage(const lcm::ReceiveBuffer* rbuf,
                const std::string& chan, 
                const RB::state* msg)
{
	state = *msg;
}


//definition for arguement intToString of class string for use in drawObject
string intToString(int number)
{
	stringstream ss;
	ss << number;
	return ss.str();
}

//definition for morphOps function
void morphOps(Mat &thresh)
{
	//the element chosen here is a 4px by 4px rectangle, it will be used 
	Mat erodeElement = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
	
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_ELLIPSE, Size(6, 6));

	//shrinks the thresholded image
	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	//enlarges the remaining thresholded image
	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
}


void morphOpsBlack(Mat &thresh)
{
	//the element chosen here is a 4px by 4px rectangle, it will be used 
	Mat erodeElement = getStructuringElement(MORPH_ELLIPSE, Size(1, 1));
	
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_ELLIPSE, Size(2, 2));

	//shrinks the thresholded image
	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	//enlarges the remaining thresholded image
	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
}

//definition of drawObject
//this function will be called to actually draw the tracker on a specified object at a found location
void drawObject(vector<RB::Object> objVec, Mat &cameraFeed)
{
	for (int i=0; i<objVec.size(); i++)
	{
		int centralX = objVec[i].xPos;
		int centralY = objVec[i].yPos;
		//alogrithm to change origin from top left corner of screen to the center, required for spotter algorithm	
		centralX -= FRAME_WIDTH/2;
		centralY = FRAME_HEIGHT/2 - centralY;
	
		//allows for tracking of multiple color objects using cases
		//more colors can be tracked by adding another case here
		//additions must also be made to enumeration, "trackFilteredObject", rangeLUV arguements, and "inRange... code"
		switch (objVec[i].type)
		{
			case BLACK_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 0));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Black:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case RED_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Red:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case RED_BUOY2:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Red2:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case YELLOW_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 255, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Yellow:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case GREEN_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(64, 255, 0));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Green:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));		
			break;
			case GREEN_BUOY2:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(64, 255, 0));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Green2:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));		
			break;
			case WHITE_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(255, 255, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "White:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
		}
	}
}

//used for changes to trackbar values
void on_trackbarLUV(int, void*)
{
}

//definition of createTrackbars functions
void createTrackbarsLUV()
{
	//create window for LUV trackbars
	namedWindow(trackbarWindowNameLUV, CV_WINDOW_AUTOSIZE);
	
	//create memory to store trackbar name on window
	char TrackbarNameLUV[50];
	sprintf(TrackbarNameLUV, "L_MIN", L_MIN);
	sprintf(TrackbarNameLUV, "L_MAX", L_MAX);
	sprintf(TrackbarNameLUV, "U_MIN", U_MIN);
	sprintf(TrackbarNameLUV, "U_MAX", U_MAX);
	sprintf(TrackbarNameLUV, "V_MIN", V_MIN);
	sprintf(TrackbarNameLUV, "V_MAX", V_MAX);
	

	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.L_MIN),
	//the max value the trackbar can move (eg. L_MAX), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbarLUV)
	createTrackbar("L_MIN", trackbarWindowNameLUV, &L_MIN, L_MAX, on_trackbarLUV);
	createTrackbar("L_MAX", trackbarWindowNameLUV, &L_MAX, L_MAX, on_trackbarLUV);
	createTrackbar("U_MIN", trackbarWindowNameLUV, &U_MIN, U_MAX, on_trackbarLUV);
	createTrackbar("U_MAX", trackbarWindowNameLUV, &U_MAX, U_MAX, on_trackbarLUV);
	createTrackbar("V_MIN", trackbarWindowNameLUV, &V_MIN, V_MAX, on_trackbarLUV);
	createTrackbar("V_MAX", trackbarWindowNameLUV, &V_MAX, V_MAX, on_trackbarLUV);
}

//function for tracking black objects
void trackFilteredObjectBlack(Mat thresholdBlack, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	//sets up temporary location for thresholded image
	Mat temp;
	thresholdBlack.copyTo(temp);
	
	//these two vectors are needed for output of findContours
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//find contours of filtered image using openCV findContours function
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	//use moments method to find our filtered object
	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		//ensures we are not tracking too many objects due to a noisy filter
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
				//openCV class "Moments" for information about location and size
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				
				//thresholds out small objects
				if (area>MIN_OBJECT_AREA1 && area<MAX_OBJECT_AREA1)
				{
					
					//sets x position and y position of the moment of the buoy into "black" Object
					RB::Object black;
					
					black.xPos = moment.m10 / area;
					black.yPos = moment.m01 / area;

					//Gets the UTC time for all black objects found
					black.timeStamp = systemTime;
					
					//defines object type
					black.type = BLACK_BUOY;
					
					//populates the vector "black" with the data from each Buoy "black"
					objVec.push_back(black);
					objectFound = true;
				}
				else objectFound = false;
			}

		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectRed(Mat thresholdRed, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdRed.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
			
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
			
				if (area>MIN_OBJECT_AREA1 && area<MAX_OBJECT_AREA1)
				{

					RB::Object red;
					red.xPos = moment.m10 / area;
					red.yPos = moment.m01 / area;
					
					red.timeStamp = systemTime;
				
					red.type = RED_BUOY;
					
					objVec.push_back(red);
					objectFound = true;
				}
			
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectRed2(Mat thresholdRed2, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdRed2.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
			
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
			
				if (area>MIN_OBJECT_AREA2 && area<MAX_OBJECT_AREA2)
				{

					cout << "size of vector" << contours.size() << "\n" << endl;
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}
					//cout << "number of points" << cnt.size() << "\n" << endl;
					RB::Object red2;
					red2.xPos = moment.m10 / area;
					red2.yPos = lowest;
					
					red2.timeStamp = systemTime;
				
					red2.type = RED_BUOY2;
					
					objVec.push_back(red2);
					objectFound = true;
				}
			
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}
void trackFilteredObjectYellow(Mat thresholdYellow, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdYellow.copyTo(temp);

	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
				
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA1 && area<MAX_OBJECT_AREA1)
				{
					
					RB::Object yellow;
					yellow.xPos = moment.m10 / area;
					yellow.yPos = moment.m01 / area;

					yellow.timeStamp = systemTime;
				
					yellow.type = YELLOW_BUOY;
					
					objVec.push_back(yellow);
					objectFound = true;
				}
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectGreen(Mat thresholdGreen, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdGreen.copyTo(temp);

	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0)
	{
		int numObjects = hierarchy.size();
	
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0])
			{
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA1 && area<MAX_OBJECT_AREA1)
				{
	
					RB::Object green;
					green.xPos = moment.m10 / area;
					green.yPos = moment.m01 / area;

					green.timeStamp = systemTime;

					green.type = GREEN_BUOY;
	
					objVec.push_back(green);
					objectFound = true;
				}
				else objectFound = false;
			}
	
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}


void trackFilteredObjectGreen2(Mat thresholdGreen2, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdGreen2.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
			
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
			
				if (area>MIN_OBJECT_AREA2 && area<MAX_OBJECT_AREA2)
				{

					cout << "size of vector" << contours.size() << "\n" << endl;
					vector <Point> cnt = contours[index];
					int lowest = cnt[0].y;
					for(int j = 0; j < cnt.size(); j++)
					{
						if(cnt[j].y > lowest) lowest = cnt[j].y;
					}
					cout << "number of points" << cnt.size() << "\n" << endl;
					RB::Object green2;
					green2.xPos = moment.m10 / area;
					green2.yPos = lowest;
					
					green2.timeStamp = systemTime;
				
					green2.type = GREEN_BUOY2;
					
					objVec.push_back(green2);
					objectFound = true;
				}
			
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectWhite(Mat thresholdWhite, Mat LUV, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdWhite.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
	
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
		
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				
				if (area>MIN_OBJECT_AREA1 && area<MAX_OBJECT_AREA1)
				{
				
					RB::Object white;
					white.xPos = moment.m10 / area;
					white.yPos = moment.m01 / area;

					white.timeStamp = systemTime;
					
					white.type = WHITE_BUOY;
					
					objVec.push_back(white);
					objectFound = true;
				}
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}
