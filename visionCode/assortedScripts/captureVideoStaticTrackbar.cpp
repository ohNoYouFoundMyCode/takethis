#include "stdafx.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char* argv)
{
	VideoCapture cap(0); // open the video camera no. 0

	if (!cap.isOpened())  // if not success, exit program
	{
		cout << "Cannot open the video file" << endl;
		return -1;
	}

	namedWindow("Marb Red", CV_WINDOW_AUTOSIZE); //create a window called "Marb Red"

	int iLowH = 0;
	int iHighH = 179;

	int iLowV = 0;
	int iHighV = 240;

	int iLowS = 0;
	int iHighS = 240;

	//creates trackbars in "Marb Red" window
	cvCreateTrackbar("LowH", "Marb Red", &iLowH, 255);
	cvCreateTrackbar("HighH", "Marb Red", &iHighH, 255);

	cvCreateTrackbar("LowS", "Marb Red", &iLowS, 255);
	cvCreateTrackbar("HighS", "Marb Red", &iHighS, 255);

	cvCreateTrackbar("LowV", "Marb Red", &iLowV, 255);
	cvCreateTrackbar("HighV", "Marb Red", &iHighV, 255);

	while (true)
	{
		Mat imgOriginal;

		bool bSuccess = cap.read(imgOriginal);//reads a new image from video

		if (!bSuccess)//breaks loop if cant capture
		{
			cout << "Device not found" << endl;
			break;
		}

		Mat imgHSV;

		cvtColor(imgOriginal, imgHSV, COLOR_BGR2HSV);

		Mat imgThresholded;
		inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded);

		dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_RECT, Size(3, 3)));

		imshow("Thresholded Image", imgThresholded);
		imshow("Original", imgOriginal);

		if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	return 0;
}
