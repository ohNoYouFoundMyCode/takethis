#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "math.h"

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	VideoCapture cap(0); //capture the video from web cam

	if (!cap.isOpened())  // if not success, exit program
	{
		cout << "Cannot open the web cam" << endl;
		return -1;
	}

	namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"

	//Sets range for Luminance, Blue Luminance, and Red Luminance
	int iLowY = 0;
	int iHighY = 240;

	int iLowCr = 0;
	int iHighCr = 240;

	int iLowCb = 0;
	int iHighCb = 240;

	//Create trackbars in "Control" window
	cvCreateTrackbar("LowY", "Control", &iLowY, 240); //Hue (0 - 179)
	cvCreateTrackbar("HighY", "Control", &iHighY, 240);

	cvCreateTrackbar("LowCr", "Control", &iLowCr, 240); //Saturation (0 - 255)
	cvCreateTrackbar("HighCr", "Control", &iHighCr, 240);

	cvCreateTrackbar("LowCb", "Control", &iLowCb, 240); //Value (0 - 255)
	cvCreateTrackbar("HighCb", "Control", &iHighCb, 240);

	while (true)
	{
		Mat imgOriginal;

		bool bSuccess = cap.read(imgOriginal); // read a new frame from video

		if (!bSuccess) //if not success, break loop
		{
			cout << "Cannot read a frame from video stream" << endl;
			break;
		}
		
		Mat imgHLS;
		Mat aimgHLS[3];
		cvtColor(imgOriginal, imgHLS, COLOR_BGR2HLS);

		//Serparates normalized YCrCb channels for access
		split(imgHLS, aimgHLS);
		
		imshow("Hue", aimgHLS[0]);
		imshow("Lightness", aimgHLS[1]);
		imshow("Saturation", aimgHLS[2]);

		if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}

	return 0;

}