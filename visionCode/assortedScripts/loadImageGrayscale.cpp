#include "stdafx.h"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, const char** argv)
{
	Mat img = imread("owl.jpg", CV_LOAD_IMAGE_GRAYSCALE); // reads the image in the data file "owl" and changes it to gray scale
	if (img.empty())// checks is the image loaded
	{
		cout << "Error: No Image" << endl;
		system("pause"); //wait for a key press
		return -1;
	}

	namedWindow("MyWindow", CV_WINDOW_AUTOSIZE);//creates me a window
	imshow("MyWindow", img);//displays one gray ass owl

	waitKey(0); //waits forever for a key press

	destroyWindow("MyWIndow");//exits cleanly
	return 0;
}
