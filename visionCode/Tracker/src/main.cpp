#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>
#include <iostream>

using namespace std;
using namespace cv;

int chan1MinTrackbar = 0, chan1MaxTrackbar = 179, chan2MinTrackbar = 0, chan2MaxTrackbar = 255, chan3MinTrackbar = 0, chan3MaxTrackbar = 255;
void createTrackbars(int &chan1MinTrackbar, int &chan2MinTrackbar, int &chan3MinTrackbar, int &chan1MaxTrackbar, int &chan2MaxTrackbar, int &chan3MaxTrackbar);
void onTrackbar(int, void*);
void greyEdge(Mat &cameraFeed, Mat &greyNormalized);

int main(int argc, char** argv)
{
	VideoCapture cap(0);
	if(!cap.isOpened())
	{
		cout << "Plug in the camera fool!" << endl;
		return -1;
	}
	//Mat cameraFeed;
	Mat blurredCameraFeed;
	Mat greyNormalized;
	Mat thresholdTest;
	namedWindow("cameraFeed", WINDOW_AUTOSIZE);
	//the element chosen here is a 4px by 4px rectangle, it will be used
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(4, 4));

	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));
	Mat cameraFeed = imread(argv[1]);
	while(1)
	{
		//cap>>cameraFeed;
		//greyEdge(cameraFeed, greyNormalized);
		blur(cameraFeed, blurredCameraFeed, Size(10,10), Point(-1,-1), BORDER_DEFAULT);
		cvtColor(blurredCameraFeed,greyNormalized,CV_BGR2HSV);
		createTrackbars(chan1MinTrackbar, chan2MinTrackbar, chan3MinTrackbar, chan1MaxTrackbar, chan2MaxTrackbar, chan3MaxTrackbar);

		//shrinks the thresholded image
		erode(greyNormalized, greyNormalized, erodeElement);

		//enlarges the remaining thresholded image
		dilate(greyNormalized, greyNormalized, dilateElement);
		inRange(greyNormalized, Scalar(chan1MinTrackbar, chan2MinTrackbar, chan3MinTrackbar), Scalar(chan1MaxTrackbar, chan2MaxTrackbar, chan3MaxTrackbar), thresholdTest);

		imshow("cameraFeed", thresholdTest);
		imshow("RGB", cameraFeed);
		int c=cvWaitKey(30);
		if (c=='c')
			break;
	}
	return 0;
}

void onTrackbar(int, void*)
{
}

void createTrackbars(int &chan1MinTrackbar, int &chan2MinTrackbar, int &chan3MinTrackbar, int &chan1MaxTrackbar, int &chan2MaxTrackbar, int &chan3MaxTrackbar)
{

	//cout << "in trackbars" << endl;
	string name = "Trackbars";
	//create window for YCrCb trackbars
	namedWindow(name, CV_WINDOW_AUTOSIZE);

	//create memory to store trackbar name on window
	char TrackbarNameYCrCb[50];
	sprintf(TrackbarNameYCrCb, "chan 1 min %i", chan1MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 1 max %i", chan1MaxTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 2 min %i", chan2MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 2 max %i", chan2MaxTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 3 min %i", chan3MinTrackbar);
	sprintf(TrackbarNameYCrCb, "chan 3 max %i", chan3MaxTrackbar);


	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.Y_MIN),
	//the max value the trackbar can move (eg. Y_MAX),
	//and the function that is called whenever the trackbar is moved(eg. on_trackbarYCrCb)
	createTrackbar("chan 1 min", name, &chan1MinTrackbar, chan1MaxTrackbar, onTrackbar);
	createTrackbar("chan 1 max", name, &chan1MaxTrackbar, chan1MaxTrackbar, onTrackbar);
	createTrackbar("chan 2 min", name, &chan2MinTrackbar, chan2MaxTrackbar, onTrackbar);
	createTrackbar("chan 2 max", name, &chan2MaxTrackbar, chan2MaxTrackbar, onTrackbar);
	createTrackbar("chan 3 min", name, &chan3MinTrackbar, chan3MaxTrackbar, onTrackbar);
	createTrackbar("chan 3 max", name, &chan3MaxTrackbar, chan3MaxTrackbar, onTrackbar);

}

void greyEdge(Mat &cameraFeed, Mat &greyNormalized)
{
	float divideBy;
	Scalar theSum;
	double illuminationGreen;
	double illuminationBlue;
	double illuminationRed;
	Mat camChannels[3];
	Mat camChannels2[3];
	double blueSum;
	double greenSum;
	double redSum;
	Mat sumChannels;
	blueSum=greenSum=redSum=0;
	const int numberPixels = 640*480;

	split(cameraFeed, camChannels);
	split(cameraFeed, camChannels2);

	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{
			Point2f location(x, y);
			double dy, dx;
			dy=(camChannels[0].at<uchar>(location.y+1, location.x)-camChannels[0].at<uchar>(location.y-1, location.x))/2;
			dx=(camChannels[0].at<uchar>(location.y, location.x+1)-camChannels[0].at<uchar>(location.y, location.x-1))/2;
			blueSum+=pow(sqrt(dy*dy+dx*dx),16);
			//blueSum+=sqrt(dy*dy+dx*dx);
			//blueGrad.at<uchar>(location.y, location.x) = sqrt(dy*dy+dx*dx);
			//cout << num<< endl;

		}
	}
	for (int y = 1; y < 480; y++)
	{
		for (int x = 1; x < 640; x++)
		{
			Point2f location(x, y);
			double dy, dx;
			dy=(camChannels[1].at<uchar>(location.y+1, location.x)-camChannels[1].at<uchar>(location.y-1, location.x))/2;
			dx=(camChannels[1].at<uchar>(location.y, location.x+1)-camChannels[1].at<uchar>(location.y, location.x-1))/2;
			greenSum+=pow(sqrt(dy*dy+dx*dx),16);
			//greenSum+=sqrt(dy*dy+dx*dx);
			//cout << num<< endl;

		}
	}

	for (int y = 1; y < 479; y++)
	{
		for (int x = 1; x < 639; x++)
		{
			Point2f location(x, y);
			double dy, dx;
			dy=(camChannels[2].at<uchar>(location.y+1, location.x)-camChannels[2].at<uchar>(location.y-1, location.x))/2;
			dx=(camChannels[2].at<uchar>(location.y, location.x+1)-camChannels[2].at<uchar>(location.y, location.x-1))/2;
			redSum+=pow(sqrt(dy*dy+dx*dx),16);
			//redSum+=sqrt(dy*dy+dx*dx);
			//cout << num<< endl;

		}
	}

	illuminationBlue = pow(blueSum/numberPixels, 1/16.0);
	illuminationGreen = pow(greenSum/numberPixels, 1/16.0);
	illuminationRed = pow(redSum/numberPixels, 1/16.0);

	Scalar blue = (sum(camChannels2[0]));
	double blueVal = (blue[0]/numberPixels);

	Scalar greenChan = (sum(camChannels2[1]));
	double greenVal = (greenChan[0]/numberPixels);

	Scalar redChan = (sum(camChannels2[2]));
	double redVal = (redChan[0]/numberPixels);

	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{
		Point2f location(x, y);
		camChannels2[0].at<uchar>(location.y, location.x)=camChannels2[0].at<uchar>(location.y, location.x)*(illuminationBlue/blueVal);
		//cout << num<< endl;
		}
	}
	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{
		Point2f location(x, y);
		camChannels2[1].at<uchar>(location.y, location.x)=camChannels2[1].at<uchar>(location.y, location.x)*(illuminationGreen/greenVal);
		//cout << num<< endl;
		}
	}
	for (int y = 0; y < 480; y++)
	{
		for (int x = 0; x < 640; x++)
		{
		Point2f location(x, y);
		camChannels2[2].at<uchar>(location.y, location.x)=camChannels2[2].at<uchar>(location.y, location.x)*(illuminationRed/redVal);
		//cout << num<< endl;
		}
	}
	merge(camChannels2, 3, greyNormalized);
}
