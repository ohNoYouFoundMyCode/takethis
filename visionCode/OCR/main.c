// tesscv.cpp:
// Using Tesseract API with OpenCV

#include "include/includes.h"
#include "include/buoyClass.h"
#include "include/buoyLocation.h"
#include <tesseract/baseapi.h>

using namespace cv;
using namespace std;

int lowThreshold;
int ratio = 3;
int kernel_size = 3;
Mat edges;
BUOY image("Image");
int main(int argc, char** argv)
{

	image.findValues();
	image.objVec.clear();
	// Usage: tesscv image.png
	if (argc != 2)
	{
	std::cout << "Please specify the input image!" << std::endl;
	return -1;
	}

	// Load image
	cv::Mat im = cv::imread(argv[1]);
	if (im.empty())
	{
	std::cout << "Cannot open source image!" << std::endl;
	return -1;
	}

	cv::Mat gray;
	cv::cvtColor(im, gray, CV_BGR2GRAY);	
	float trueCon = 0;
	char* out;
	vector<string> outVec;
	vector<float> confVec;
	vector<int> countVec;
	for (int t = 0; t<364; t+=5)
	{
		Point2f src_center(gray.cols/2.0F, gray.rows/2.0F);
		Mat rot_mat = getRotationMatrix2D(src_center, t, 1.0);
		Mat dst(640,640, CV_8UC1);
		warpAffine(gray, dst, rot_mat, gray.size());
		imshow("rotate", dst);

		//the element chosen here is a 4px by 4px rectangle, it will be used 
		Mat erodeElement = getStructuringElement(MORPH_RECT, Size(4, 4));

		//dilate with larger element so make sure object is nicely visible
		Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));

		//enlarges the remaining thresholded image
		dilate(dst, dst, dilateElement);
		dilate(dst, dst, dilateElement);
		dilate(dst, dst, dilateElement);
		dilate(dst, dst, dilateElement);
		dilate(dst, dst, dilateElement);
		dilate(dst, dst, dilateElement);

		/*erode(gray, gray, erodeElement);
		erode(gray, gray, erodeElement);
		erode(gray, gray, erodeElement);
		erode(gray, gray, erodeElement);
		erode(gray, gray, erodeElement);*/

		//shrinks the thresholded image
		Mat thresholdTest;

		inRange(dst, Scalar(200, 200, 200), Scalar(255, 255, 255), thresholdTest);

		image.trackObject(thresholdTest);
		image.drawObject

		imshow("gray", gray);
		imshow("threshold", thresholdTest);

		// Pass it to Tesseract API
		tesseract::TessBaseAPI tess;
		tess.Init(NULL, "letsgodigital", tesseract::OEM_DEFAULT);
		tess.SetVariable("tessedit_char_whitelist", "0123456789AbCdEF");
		tess.SetPageSegMode(tesseract::PSM_SINGLE_BLOCK);
		tess.SetImage((uchar*)thresholdTest.data, thresholdTest.cols, thresholdTest.rows, 1, thresholdTest.cols);
		// Get the text
		out = tess.GetUTF8Text();
		tesseract::ResultIterator* ri = tess.GetIterator();
		tesseract::PageIteratorLevel level = tesseract::RIL_SYMBOL;
		float conf = ri->Confidence(level);
		int iterator=0;
		bool found;
		cout << out << endl;
		cout << conf<< endl;
		float min1, min2;
		while(iterator<outVec.size())
		{
			if(outVec[iterator]==out)
			{
				confVec[iterator]+=conf;
				countVec[iterator]++;
				found=true;
			}
			iterator++;
		}
		if(!found)
		{
			outVec.push_back(out);
			confVec.push_back(conf);
			countVec.push_back(1);
		}
	}
	
	float max;
	int maxIndex;
	for(int i=0; i<outVec.size(); i++)
	{
		if(max<(confVec[i]))
		{	
			max=confVec[i];
			maxIndex = i;
		}

	}	
	
	std::cout << "the character is: " << outVec[maxIndex] << "and the confidence is: " << confVec[maxIndex] << std::endl;

	cv::waitKey(1000000);

	return 0;
}

