#ifndef INCLUDES_H
#define INCLUDES_H

#include <ctime>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <valarray>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>   

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cv.h>


using namespace cv;
using namespace std;

#endif
