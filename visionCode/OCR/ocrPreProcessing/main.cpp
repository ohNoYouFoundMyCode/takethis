#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

using namespace cv;

int main(int argc, char** argv )
{
	if ( argc !=2 )
	{
		printf("usage: DisplayImage.out ~/Desktop/Open_cv/'Display Image' \n");
		return -1;
	}

	Mat image;
	image = imread( argv[1], 1 );

	if (!image.data )
	{
		printf("no image data \n");
		return -1;
	}

	blur(image, image, Size(10,10));
	// Convert RGB image to grayscale
	Mat im_rgb = image;
	Mat im_gray;

	cvtColor(im_rgb, im_gray, CV_RGB2GRAY);

	// Convert to binary
	Mat img_bw = im_gray > 90;

	//the element chosen here is a 4px by 4px rectangle, it will be used
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(2, 2));

	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(2, 2));

	//enlarges the remaining thresholded image
	//dilate(img_bw, img_bw, dilateElement);

	//shrinks the thresholded image
	//erode(img_bw, img_bw, erodeElement);


	// Invert
	Mat src = img_bw;
	Mat dst;
	bitwise_not ( src, dst );
	Mat im_inv = dst;


	// Save to disk
	//imwrite("im_gray.png", im_gray);
	//imwrite("im_bw.png",img_bw);
	//imwrite("im_inv.png",im_inv);

	// Teseract 
	system("tesseract -psm 4 im_gray.png outputGRAY -1 eng");
	system("tesseract -psm 4 im_bw.png outputBW -1 eng");
	system("tesseract -psm 4 im_inv.png outputINV -1 eng");


   return 0;
}
