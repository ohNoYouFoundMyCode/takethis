//Travis Moscicki 2014
//Florida Atlantic University College of Engineering and Computer Science
//AUVSI RoboBoat Competition

//This program is writen for the purpose of identifying multiple objects of both the same color and different colors in an outdoor setting with both
//high and dynamic lighting.  A buoy class is introced that allows us to populate a vector of this class of objects.
//We use two layers of filtration.  The first is normalizedBGR, which makes colors more vibrant and uniform.  The second is chromaticity (YCrCb) which 
//is designed to filter out changes in inensity of light.

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

#include <ctime>
#include <sstream>
#include <string>
#include <iostream>
#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <vector>
#include <stdint.h>
#include <windows.h>
#include <lcm/lcm-cpp.hpp>
#include <inttypes.h>
#include "ObjectList.hpp"

using namespace cv;
using namespace std;

//this functions sets up our trackbars and can either be enable or disabled
void createTrackbarsYCrCb();
void createTrackbarsBlack();

//range for Y, Cr, and Cb values can be changed by enabling trackbars
int Y_MIN = 0;
int Y_MAX = 510;
int Cr_MIN = 0;
int Cr_MAX = 510;
int Cb_MIN = 0;
int Cb_MAX = 510;

//range for H, L, and S values van be changed by enabling trackbars
int H_MIN = 0;
int H_MAX = 510;
int L_MIN = 0;
int L_MAX = 510;
int S_MIN = 0;
int S_MAX = 510;

//This function gets called whenever a trackbar position is changed
void on_trackbarYCrCb(int, void*);

void on_trackbarBlack(int, void*);


//default capture width and height
const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;

//Matrixes to store each frame of the webcam feed, thresholded images, as well as final YCrCb image
Mat cameraFeed;
Mat thresholdBlack;
Mat thresholdRed;
Mat thresholdYellow;
Mat thresholdGreen;
Mat thresholdWhite;
Mat Black;
Mat YCrCb;

//max number of objects to be detected in frame
const int MAX_NUM_OBJECTS = 30;

//minimum and maximum object area 
//we set a minimum to filter out noise (20 x 20 pixles)
//we set a maximum to ensure that only objects the size of buoys are tracked (60 x 60 pixels: area of buoy at .2 meters away)
const int MIN_OBJECT_AREA = 20 * 20;
const int MAX_OBJECT_AREA = 100 * 100;

//names that will appear at the top of each window
const string windowName = "Original Image";
const string windowName1 = "YCrCb Image";
const string windowName5 = "Black Thresholding";
const string windowName3 = "Red Thresholding";
const string windowName4 = "Yellow Thresholding";
const string windowName2 = "Green Thresholding";
const string windowName6 = "White Thresholding";
const string windowName7 = "Normalized BGR";
const string windowName8 = "YCrCb Threshold Tester";
const string windowName9 = "Black Thresholded Tester";
const string windowName10 = "Black Tracking With Sample Filters";
const string trackbarWindowNameYCrCb = "YCrCb Trackbars";
const string trackbarWindowNameBlack = "Black Trackbars";


//class to allow for easy changes to values for thresholding of each color buoy
class YCrCbRange
{
	public:
		uint16_t yMin;
		uint16_t yMax;
		uint16_t crMin;
		uint16_t crMax;
		uint16_t cbMin;
		uint16_t cbMax;
};

//class to allow for easy changes of black objects, introduced because we are using a different filtration process
class shadeRange
{
	public:
		uint16_t hMin;
		uint16_t hMax;
		uint16_t lMin;
		uint16_t lMax;
		uint16_t sMin;
		uint16_t sMax;
};

//sets types to make changes in drawObject function easier and for passing through LCM
enum objectType {BLACK_BUOY = 1, RED_BUOY, YELLOW_BUOY, GREEN_BUOY,  WHITE_BUOY};

//initializes a vector called objVec made of "Object" class objects, this is what we will pass through LCM to the spotter algorithm
vector<RB::Object> objVec;

//initializes arguement objList of class ObjectList
RB::ObjectList objList;

//protoype for the function that will draw circles and location on dectected objects
void drawObject(vector<RB::Object> objVec, Mat &cameraFeed);

//create structuring element that will be used to dilate (enlarge) and erode (shrink) image.
//this is useful in destroying outliers common with imperfect filtration
void morphOps(Mat &thresh);

//these are the prototypes for our algorithm that will track filtered objects
//this is repeated five times for five different colored objects
//if more objects are to be tracked, they must be initialized here, added to the enumeration of "objectType", and defined below
void trackFilteredObjectBlack(Mat thresholdBlack, Mat Black, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectRed(Mat thresholdRed, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectYellow(Mat thresholdYellow, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectGreen(Mat thresholdGreen, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime);
void trackFilteredObjectWhite(Mat thresholdWhite, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime);

//protoype for int to string
string intToString(int number);

//beginning of our main function
int main(int argc, char* argv[])
{
	//creates and verifies our LCM channel for interprocess communication
	lcm::LCM lcm = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!lcm.good())
	{
		printf("failed to create lcm");
		return 1;
	}
		
	//if we would like to calibrate our filter values, set to true.
	bool calibrationMode = true;

	if (calibrationMode)
	{
		//create slider bars for YCrCb filtering
//		createTrackbarsYCrCb();
		createTrackbarsBlack();
	}
	//video capture object to acquire webcam feed
	VideoCapture capture;

	//open capture object at location zero (default location for webcam)
	capture.open(0);

	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);


	//start an infinite loop where webcam feed is copied to cameraFeed matrix
	//all of our operations will be performed within this loop
	while (1)
	{

		//clears out vector between loops
		objVec.clear();

		//store image to matrix
		capture.read(cameraFeed);

		SYSTEMTIME time;
		uint64_t systemTime;
		GetSystemTime(&time);
		systemTime = time.wMilliseconds + time.wSecond * 1000L + time.wMinute * 60000L + time.wHour * 3600000L + time.wDay * 86400000L;

		//declares arguements for our normalizedBGR algorithm
		Mat camChannels[3];
		Mat sumChannels;
		Mat normCamChannels[3];
		Mat normalizedCamChannels;
		split(cameraFeed, camChannels);
		sumChannels = camChannels[0] + camChannels[1] + camChannels[2];
		
		//normalizedBGR algorithm
		normCamChannels[0] = camChannels[0] / sumChannels * 255;
		normCamChannels[1] = camChannels[1] / sumChannels * 255;
		normCamChannels[2] = camChannels[2] / sumChannels * 255;
		
		//operation to turn 3 separate channels into a single 3 channel matrix
		merge(normCamChannels, 3, normalizedCamChannels);

		//convert frame from normalized BGR to normalized YCrCb colorspace
		cvtColor(normalizedCamChannels, YCrCb, COLOR_BGR2YCrCb);

		//function used to apply transform for black objects
//		cvtColor(cameraFeed, Black, COLOR_BGR2RGB);
		Black=cameraFeed;

		//use these arguements to change values for different filters
		//yMin and yMax are left open to best account for changes in light intensity
		//more arguements can be added for thresholding of additional colors
		YCrCbRange redRange, yellowRange, greenRange, whiteRange;
		
		//we will use some openCV function to track black and white objects
		shadeRange blackRange; 
		
		blackRange.hMin = 77;
		blackRange.hMax = 119;
		blackRange.lMin = 73;
		blackRange.lMax = 126;
		blackRange.sMin = 70;
		blackRange.sMax = 190;

		redRange.yMin = Y_MIN;
		redRange.yMax = Y_MAX;
		redRange.crMin = 0;
		redRange.crMax = 0;
		redRange.cbMin = 0;
		redRange.cbMax = 0;

		yellowRange.yMin = Y_MIN;
		yellowRange.yMax = Y_MAX;
		yellowRange.crMin = 140;
		yellowRange.crMax = 159;
		yellowRange.cbMin = 57;
		yellowRange.cbMax = 98;

		greenRange.yMin = Y_MIN;
		greenRange.yMax = Y_MAX;
		greenRange.crMin = 0;
		greenRange.crMax = 0;
		greenRange.cbMin = 0;
		greenRange.cbMax = 0;

		whiteRange.yMin = Y_MIN;
		whiteRange.yMax = Y_MAX;
		whiteRange.crMin = 0;
		whiteRange.crMax = 0;
		whiteRange.cbMin = 0;
		whiteRange.cbMax = 0;

		//thresholds image for specific colors, erodes and dilates images, and tracks objects
		//a block of code below can be copied, pasted, and changed for tracking of additional colors
		inRange(Black, Scalar(blackRange.hMin, blackRange.lMin, blackRange.sMin), Scalar(blackRange.hMax, blackRange.lMax, blackRange.sMax), thresholdBlack);
		morphOps(thresholdBlack);
		trackFilteredObjectBlack(thresholdBlack, Black, cameraFeed, systemTime);				

		inRange(YCrCb, Scalar(redRange.yMin, redRange.crMin, redRange.cbMin), Scalar(redRange.yMax, redRange.crMax, redRange.cbMax), thresholdRed);
		morphOps(thresholdRed);
		trackFilteredObjectRed(thresholdRed, YCrCb, cameraFeed, systemTime);		
			
		inRange(YCrCb, Scalar(redRange.yMin, yellowRange.crMin, yellowRange.cbMin), Scalar(yellowRange.yMax, yellowRange.crMax, yellowRange.cbMax), thresholdYellow);	
		morphOps(thresholdYellow);
		trackFilteredObjectYellow(thresholdYellow, YCrCb, cameraFeed, systemTime);
					
		inRange(YCrCb, Scalar(greenRange.yMin, greenRange.crMin, greenRange.cbMin), Scalar(greenRange.yMax, greenRange.crMax, greenRange.cbMax), thresholdGreen);
		morphOps(thresholdGreen);
		trackFilteredObjectGreen(thresholdGreen, YCrCb, cameraFeed, systemTime);

		inRange(YCrCb, Scalar(whiteRange.yMin, whiteRange.crMin, whiteRange.cbMin), Scalar(whiteRange.yMax, whiteRange.crMax, whiteRange.cbMax), thresholdWhite);
		morphOps(thresholdWhite);
		trackFilteredObjectWhite(thresholdWhite, YCrCb, cameraFeed, systemTime);
		
		//draws cirlces and on screen location about the center of each tracked object
		drawObject(objVec, cameraFeed);

		//tranfroms the on screen pixel space from the origin being in the top left corner to the center pixel.
		for(int i = 0; i < objVec.size(); i++)
		{
			objVec[i].xPos -= FRAME_WIDTH / 2;
			objVec[i].yPos = FRAME_HEIGHT / 2 - objVec[i].yPos;
		}
		
		//shows different stages of image filtering
		imshow(windowName, cameraFeed);
//		imshow(windowName1,YCrCb);
//		imshow(windowName5,thresholdBlack);
//		imshow(windowName3,thresholdRed);
//		imshow(windowName4,thresholdYellow);
//		imshow(windowName2,thresholdGreen);
//		imshow(windowName6, thresholdWhite);
//		imshow(windowName7, normalizedCamChannels);
//		imshow(windowName10, Black);


		//uses trackbars so that we can determine appropriate YCrCb values for deried colors
		if(calibrationMode)
		{
			Mat thresholdTestYCrCb;
			inRange(YCrCb, Scalar(Y_MIN, Cr_MIN, Cb_MIN), Scalar(Y_MAX, Cr_MAX, Cb_MAX), thresholdTestYCrCb);
//			imshow(windowName8, thresholdTestYCrCb);


			Mat thresholdTestBlack;
			inRange(Black, Scalar(H_MIN, L_MIN, S_MIN), Scalar(H_MAX, L_MAX, S_MAX), thresholdTestBlack);
			imshow(windowName9, thresholdTestBlack);
		}		
		
		//defines argument ObjList for passing through LCM
		objList.numObjects = objVec.size();
		objList.list = objVec;

		
		//ensures that information is sent through LCM only if there is an object detected
		if (objVec.size()>0)
		{
			lcm.publish("OBJ_SIM", &objList);
		}

		//waits for the "escape" key to be pressed for 27 ms, if it is, it breaks the loop
		if (waitKey(30) == 27) 
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	return 0;
}//end of main


//definition for arguement intToString of class string for use in drawObject
string intToString(int number)
{
	stringstream ss;
	ss << number;
	return ss.str();
}

//definition for morphOps function
void morphOps(Mat &thresh)
{
	//the element chosen here is a 4px by 4px rectangle, it will be used 
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(4, 4));
	
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(6, 6));

	//shrinks the thresholded image
	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	//enlarges the remaining thresholded image
	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
}

//definition of drawObject
//this function will be called to actually draw the tracker on a specified object at a found location
void drawObject(vector<RB::Object> objVec, Mat &cameraFeed)
{
	for (int i=0; i<objVec.size(); i++)
	{
		int centralX = objVec[i].xPos;
		int centralY = objVec[i].yPos;
		//alogrithm to change origin from top left corner of screen to the center, required for spotter algorithm	
		centralX -= FRAME_WIDTH/2;
		centralY = FRAME_HEIGHT/2 - centralY;
	
		//allows for tracking of multiple color objects using cases
		//more colors can be tracked by adding another case here
		//additions must also be made to enumeration, "trackFilteredObject", YCrCbRange arguements, and "inRange... code"
		switch (objVec[i].type)
		{
			case BLACK_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 0));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Black:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case RED_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Red:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case YELLOW_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 255, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Yellow:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
			case GREEN_BUOY:

				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(64, 255, 0));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "Green:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));		
			break;
			case WHITE_BUOY:
				cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(255, 255, 255));//scalar dictates color in BGR space
				cv::putText(cameraFeed, "White:" + intToString(centralX) + " , " + intToString(centralY), cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			break;
		}
	}
}

//used for changes to trackbar values
void on_trackbarYCrCb(int, void*)
{
}

void on_trackbarBlack(int, void*)
{
}

//definition of createTrackbars functions
void createTrackbarsYCrCb()
{
	//create window for YCrCb trackbars
	namedWindow(trackbarWindowNameYCrCb, CV_WINDOW_AUTOSIZE);
	
	//create memory to store trackbar name on window
	char TrackbarNameYCrCb[50];
	sprintf(TrackbarNameYCrCb, "Y_MIN", Y_MIN);
	sprintf(TrackbarNameYCrCb, "Y_MAX", Y_MAX);
	sprintf(TrackbarNameYCrCb, "Cr_MIN", Cr_MIN);
	sprintf(TrackbarNameYCrCb, "Cr_MAX", Cr_MAX);
	sprintf(TrackbarNameYCrCb, "Cb_MIN", Cb_MIN);
	sprintf(TrackbarNameYCrCb, "Cb_MAX", Cb_MAX);
	

	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.Y_MIN),
	//the max value the trackbar can move (eg. Y_MAX), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbarYCrCb)
	createTrackbar("Y_MIN", trackbarWindowNameYCrCb, &Y_MIN, Y_MAX, on_trackbarYCrCb);
	createTrackbar("Y_MAX", trackbarWindowNameYCrCb, &Y_MAX, Y_MAX, on_trackbarYCrCb);
	createTrackbar("Cr_MIN", trackbarWindowNameYCrCb, &Cr_MIN, Cr_MAX, on_trackbarYCrCb);
	createTrackbar("Cr_MAX", trackbarWindowNameYCrCb, &Cr_MAX, Cr_MAX, on_trackbarYCrCb);
	createTrackbar("Cb_MIN", trackbarWindowNameYCrCb, &Cb_MIN, Cb_MAX, on_trackbarYCrCb);
	createTrackbar("Cb_MAX", trackbarWindowNameYCrCb, &Cb_MAX, Cb_MAX, on_trackbarYCrCb);
}

void createTrackbarsBlack()
{	//create windows for HLS trackbar
	namedWindow(trackbarWindowNameBlack, CV_WINDOW_AUTOSIZE);
	
	//create memory to store trackbar name on window
	char trackbarNameBlack[50];
	sprintf(trackbarNameBlack, "H_MIN", H_MIN);
	sprintf(trackbarNameBlack, "H_MAX", H_MAX);
	sprintf(trackbarNameBlack, "L_MIN", L_MIN);
	sprintf(trackbarNameBlack, "L_MAX", L_MAX);
	sprintf(trackbarNameBlack, "S_MIN", S_MIN);
	sprintf(trackbarNameBlack, "S_MAX", S_MAX);
	
	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.Y_MIN),
	//the max value the trackbar can move (eg. Y_MAX), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbarHLS)
	createTrackbar("H_MIN", trackbarWindowNameBlack, &H_MIN, H_MAX, on_trackbarBlack);
	createTrackbar("H_MAX", trackbarWindowNameBlack, &H_MAX, H_MAX, on_trackbarBlack);
	createTrackbar("L_MIN", trackbarWindowNameBlack, &L_MIN, L_MAX, on_trackbarBlack);
	createTrackbar("L_MAX", trackbarWindowNameBlack, &L_MAX, L_MAX, on_trackbarBlack);
	createTrackbar("S_MIN", trackbarWindowNameBlack, &S_MIN, S_MAX, on_trackbarBlack);
	createTrackbar("S_MAX", trackbarWindowNameBlack, &S_MAX, S_MAX, on_trackbarBlack);
}

//function for tracking black objects
void trackFilteredObjectBlack(Mat thresholdBlack, Mat Black, Mat &cameraFeed, uint64_t systemTime)
{
	//sets up temporary location for thresholded image
	Mat temp;
	thresholdBlack.copyTo(temp);
	
	//these two vectors are needed for output of findContours
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//find contours of filtered image using openCV findContours function
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	//use moments method to find our filtered object
	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		//ensures we are not tracking too many objects due to a noisy filter
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
				//openCV class "Moments" for information about location and size
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				
				//thresholds out small objects
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
					
					//sets x position and y position of the moment of the buoy into "black" Object
					RB::Object black;
					black.xPos = moment.m10 / area;
					black.yPos = moment.m01 / area;
							
					//Gets the UTC time for all black objects found
					black.timeStamp = systemTime;
					cout << "Black Buoy found at : " << black.timeStamp << "milliseconds \n" << endl;
					
					//defines object type
					black.type = BLACK_BUOY;
					
					//populates the vector "black" with the data from each Buoy "black"
					objVec.push_back(black);
					objectFound = true;
				}
				else objectFound = false;
			}

		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectRed(Mat thresholdRed, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdRed.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
			
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
			
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
				
					RB::Object red;
					red.xPos = moment.m10 / area;
					red.yPos = moment.m01 / area;

					red.timeStamp = systemTime;
				
					red.type = RED_BUOY;
					
					objVec.push_back(red);
					objectFound = true;
				}
			
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectYellow(Mat thresholdYellow, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdYellow.copyTo(temp);

	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
				
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
					
					RB::Object yellow;
					yellow.xPos = moment.m10 / area;
					yellow.yPos = moment.m01 / area;

					yellow.timeStamp = systemTime;
				
					yellow.type = YELLOW_BUOY;
					
					objVec.push_back(yellow);
					objectFound = true;
				}
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectGreen(Mat thresholdGreen, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdGreen.copyTo(temp);

	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0)
	{
		int numObjects = hierarchy.size();
	
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0])
			{
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
	
					RB::Object green;
					green.xPos = moment.m10 / area;
					green.yPos = moment.m01 / area;

					green.timeStamp = systemTime;

					green.type = GREEN_BUOY;
	
					objVec.push_back(green);
					objectFound = true;
				}
				else objectFound = false;
			}
	
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void trackFilteredObjectWhite(Mat thresholdWhite, Mat YCrCb, Mat &cameraFeed, uint64_t systemTime)
{
	Mat temp;
	thresholdWhite.copyTo(temp);
	
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	
	double refArea = 0;
	bool objectFound = false;
	
	if (hierarchy.size() > 0) 
	{
	
		int numObjects = hierarchy.size();
		if (numObjects<MAX_NUM_OBJECTS)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{
		
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA)
				{
				
					RB::Object white;
					white.xPos = moment.m10 / area;
					white.yPos = moment.m01 / area;

					white.timeStamp = systemTime;
					
					white.type = WHITE_BUOY;
					
					objVec.push_back(white);
					objectFound = true;
				}
				else objectFound = false;
			}
		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}