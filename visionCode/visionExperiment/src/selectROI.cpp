#include "../include/selectROI.h"

selectROI::selectROI()
{ 
}

selectROI::~selectROI()
{
	destroyAllWindows();
}

void selectROI::insertNewFrame(Mat &cameraFeedMain)
{
	cameraFeed=cameraFeedMain;
}

void selectROI::display()
{
	imshow("cameraFeed", cameraFeed);
}

bool selectROI::escape()
{
	int esc = cvWaitKey(20);
	if( esc == 1048603 ) return true;
	else return false;
}