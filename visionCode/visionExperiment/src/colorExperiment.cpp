//Original Author:Travis Moscicki
//Date:4/16/2016
//Last Edit:5/11/2016 - Travis Moscicki

//The purpose of this script is to capture image data about different colored objects
//by selecting a region of interest in the frame, converting this to a number of
//different color spaces, and then logging said data.

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <vector>
#include <unistd.h>
#include <fstream>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace cv;
using namespace boost;

int drag = 0;
bool boxCounter=false;
Point point1, point2;
vector<Rect> rect;
int numSamples=0;
bool theFlag=false;
int deltaY;
int deltaX;
vector<int> numPixels;

Mat greyEdge(Mat &cameraFeed);
void mouseHandler(int event, int x, int y, int flags, void* param);

int main(int argc, char **argv)
{
	Mat cameraFeed;
	vector<shared_ptr<ofstream> > dataFile;
	
	vector<Mat> hsvROI;
	Mat hsvROIChannels[3];

	vector<Mat> yCrCbROI;
	Mat yCrCbROIChannels[3];

	vector<Mat> grayscaleROI;
	Mat grayscaleROIChannels[3];
	Mat grayscaleROITemp;

	vector<Mat> averageROI;
	Mat averageROIChannels[3];

	vector<Mat> rgbROI;
	Mat rgbROIChannels[3];

	vector<Mat> greyEdgeROI;
	Mat greyEdgeROIChannels[3];

	cameraFeed = imread(argv[1], CV_LOAD_IMAGE_COLOR);
		blur(cameraFeed, cameraFeed, Size(15,15));

	
    	while(1)
	{
		cvSetMouseCallback("cameraFeed", mouseHandler, NULL);
        	namedWindow("cameraFeed", CV_WINDOW_AUTOSIZE);

		hsvROI.clear();
		yCrCbROI.clear();
		grayscaleROI.clear();
		averageROI.clear();
		rgbROI.clear();
		greyEdgeROI.clear();
		if(theFlag){
			deltaY=point2.y-point1.y;
			deltaX=point2.x-point1.x;
			numPixels.push_back(deltaX*deltaY);
			stringstream streamer;
			streamer<<numSamples;
			string numSamplesStringer;
			numSamplesStringer=streamer.str();
			//change this to the local directory
			string name="/home/seatechmsl/Desktop/visionExperiment/data/dataFile"+numSamplesStringer+".txt";
			//
			dataFile.push_back(make_shared<ofstream>(name.c_str()));
			*dataFile[numSamples] << "hChannelTemp\tsChannelTemp\tvChannelTemp\tYChannelTemp\tCrChannelTemp\tCbChannelTemp\tgrayChannelTemp\t";
			*dataFile[numSamples] << "averageColorChannelTemp\tbChannelTemp\tgChannelTemp\trChannelTemp\tbGreyChannelTemp\tgGreyChannelTemp\trGreyChannelTemp\n";
			theFlag=false;
		}	
		if(boxCounter){
			
			cout << "Start" << endl;
			for(int i=0; i<=numSamples; i++){
				stringstream stream;
				stream<<i;
				string numSamplesString;
				numSamplesString=stream.str();
				
			    	namedWindow("hsvROI"+numSamplesString, CV_WINDOW_AUTOSIZE);
			    	namedWindow("yCrCbROI"+numSamplesString, CV_WINDOW_AUTOSIZE);
			    	namedWindow("grayscaleROI"+numSamplesString, CV_WINDOW_AUTOSIZE);
			    	namedWindow("averageROI"+numSamplesString, CV_WINDOW_AUTOSIZE);
			    	namedWindow("rgbROI"+numSamplesString, CV_WINDOW_AUTOSIZE);
			    	namedWindow("greyEdgeROI"+numSamplesString, CV_WINDOW_AUTOSIZE);

				cout << "hsvROi.size() is:" << hsvROI.size() << endl;
				cout << "rect[numSamples] is :" << rect[numSamples] << endl;
				cout << "numSamples is :" << numSamples << endl;
				cout << "numSamplesString is :" << numSamplesString << endl << endl;
				
				hsvROI.push_back(cameraFeed(rect[i]).clone());
				yCrCbROI.push_back(cameraFeed(rect[i]).clone());
				grayscaleROI.push_back(cameraFeed(rect[i]).clone());
				averageROI.push_back(cameraFeed(rect[i]).clone());
				rgbROI.push_back(cameraFeed(rect[i]).clone());
				greyEdgeROI.push_back(cameraFeed(rect[i]).clone());

			    	cvtColor(hsvROI[i], hsvROI[i], COLOR_BGR2HSV);
			    	cvtColor(yCrCbROI[i], yCrCbROI[i], COLOR_BGR2YCrCb);
			    	cvtColor(grayscaleROI[i], grayscaleROI[i], COLOR_BGR2GRAY);
				greyEdgeROI[i]=greyEdge(greyEdgeROI[i]);

				split(hsvROI[i], hsvROIChannels);
				imshow("channels0", hsvROIChannels[0]);
				imshow("channels1", hsvROIChannels[1]);
				imshow("channels2", hsvROIChannels[2]);

				split(yCrCbROI[i], yCrCbROIChannels);
				imshow("channels00", yCrCbROIChannels[0]);
				imshow("channels01", yCrCbROIChannels[1]);
				imshow("channels02", yCrCbROIChannels[2]);

				//grayScale needs no split since it is only one channel

				split(averageROI[i], averageROIChannels);
				imshow("channels10", averageROIChannels[0]);
				imshow("channels11", averageROIChannels[1]);
				imshow("channels12", averageROIChannels[2]);

				split(rgbROI[i], rgbROIChannels);
				imshow("channels20", rgbROIChannels[0]);
				imshow("channels21", rgbROIChannels[1]);
				imshow("channels22", rgbROIChannels[2]);

				split(greyEdgeROI[i], greyEdgeROIChannels);
				imshow("channels30", greyEdgeROIChannels[0]);
				imshow("channels31", greyEdgeROIChannels[1]);
				imshow("channels32", greyEdgeROIChannels[2]);

				//Variables that will be written to data file
				//h channel
				//v channel
				//s channel
				//Y channel
				//Cr channel
				//Cb channel
				//gray channel
				//average color channel
				//b channel
				//g channel
				//r channel
				//bGE channel
				//gGE channel
				//rGE channel
				float hChannelTemp, sChannelTemp, vChannelTemp, YChannelTemp, CrChannelTemp, CbChannelTemp;
				float grayChannelTemp, averageColorChannelTemp, bChannelTemp, gChannelTemp, rChannelTemp;
				float bGreyChannelTemp, gGreyChannelTemp, rGreyChannelTemp;

				vector<float> valueVector;

				valueVector.push_back(sum(hsvROIChannels[0])[0]/numPixels[i]);
				valueVector.push_back(sum(hsvROIChannels[1])[0]/numPixels[i]);
				valueVector.push_back(sum(hsvROIChannels[2])[0]/numPixels[i]);	

				valueVector.push_back(sum(yCrCbROIChannels[0])[0]/numPixels[i]);	
				valueVector.push_back(sum(yCrCbROIChannels[1])[0]/numPixels[i]);
				valueVector.push_back(sum(yCrCbROIChannels[2])[0]/numPixels[i]);	

				valueVector.push_back(sum(grayscaleROI[i])[0]/numPixels[i]);
				
				valueVector.push_back((sum(rgbROIChannels[0])[0]+sum(rgbROIChannels[1])[0]+sum(rgbROIChannels[2])[0])/(3*numPixels[i]));
				
				valueVector.push_back(sum(rgbROIChannels[0])[0]/numPixels[i]);	
				valueVector.push_back(sum(rgbROIChannels[1])[0]/numPixels[i]);
				valueVector.push_back(sum(rgbROIChannels[2])[0]/numPixels[i]);

				valueVector.push_back(sum(greyEdgeROIChannels[0])[0]/numPixels[i]);	
				valueVector.push_back(sum(greyEdgeROIChannels[1])[0]/numPixels[i]);
				valueVector.push_back(sum(greyEdgeROIChannels[2])[0]/numPixels[i]);

				//write data
				for(int w=0; w<valueVector.size(); w++){
					stringstream writer;
					writer<<valueVector[w];
					string valString;
					valString=writer.str();
					if(w==7) *dataFile[i]<<valString+"\t\t\t";
					else *dataFile[i]<<valString+"\t\t";
				}
				*dataFile[i]<<"\n";
				merge(hsvROIChannels, 3, hsvROI[i]);
				merge(yCrCbROIChannels, 3, yCrCbROI[i]);
				merge(averageROIChannels, 3, averageROI[i]);
				merge(rgbROIChannels, 3, rgbROI[i]);
				merge(greyEdgeROIChannels, 3, greyEdgeROI[i]);
				
				imshow("hsvROI"+numSamplesString, hsvROI[i]);
				imshow("yCrCbROI"+numSamplesString, yCrCbROI[i]);
				imshow("grayscaleROI"+numSamplesString, grayscaleROI[i]);
				imshow("averageROI"+numSamplesString, averageROI[i]);
				imshow("rgbROI"+numSamplesString, rgbROI[i]);
				imshow("greyEdgeROI"+numSamplesString, greyEdgeROI[i]);
			}
		}
		imshow("cameraFeed", cameraFeed);
		int esc = cvWaitKey(20);
		if( esc == 1048603){
			for(int b=0; b<numSamples; b++){
				dataFile[b]->close();
			}
			break;
		}
		usleep(50000);
	}
	return 0;
}

Mat greyEdge(Mat &cameraFeed)
{
	float divideBy;
	Scalar theSum;
	double illuminationGreen;
	double illuminationBlue;
	double illuminationRed;
	Mat camChannels[3];
	Mat camChannels2[3];
	double blueSum;
	double greenSum;
	double redSum;
	Mat sumChannels;
	blueSum=greenSum=redSum=0;
	const int numberPixels = 640*480;
	Mat normalized;

	split(cameraFeed, camChannels);
	split(cameraFeed, camChannels2);

	for (int y = 0; y < cameraFeed.rows; y++)
	{
		for (int x = 0; x < cameraFeed.cols; x++)

		{
			Point2f location(x, y);
			double dy, dx;
			dy=(camChannels[0].at<uchar>(location.y+1, location.x)-camChannels[0].at<uchar>(location.y-1, location.x))/2;
			dx=(camChannels[0].at<uchar>(location.y, location.x+1)-camChannels[0].at<uchar>(location.y, location.x-1))/2;
			blueSum+=pow(sqrt(dy*dy+dx*dx),16);
			//blueSum+=sqrt(dy*dy+dx*dx);
			//blueGrad.at<uchar>(location.y, location.x) = sqrt(dy*dy+dx*dx);
			//cout << num<< endl;

		}
	}
	for (int y = 0; y < cameraFeed.rows; y++)
	{
		for (int x = 0; x < cameraFeed.cols; x++)

		{
			Point2f location(x, y);
			double dy, dx;
			dy=(camChannels[1].at<uchar>(location.y+1, location.x)-camChannels[1].at<uchar>(location.y-1, location.x))/2;
			dx=(camChannels[1].at<uchar>(location.y, location.x+1)-camChannels[1].at<uchar>(location.y, location.x-1))/2;
			greenSum+=pow(sqrt(dy*dy+dx*dx),16);
			//greenSum+=sqrt(dy*dy+dx*dx);
			//cout << num<< endl;

		}
	}

	for (int y = 0; y < cameraFeed.rows; y++)
	{
		for (int x = 0; x < cameraFeed.cols; x++)
		{
			Point2f location(x, y);
			double dy, dx;
			dy=(camChannels[2].at<uchar>(location.y+1, location.x)-camChannels[2].at<uchar>(location.y-1, location.x))/2;
			dx=(camChannels[2].at<uchar>(location.y, location.x+1)-camChannels[2].at<uchar>(location.y, location.x-1))/2;
			redSum+=pow(sqrt(dy*dy+dx*dx),16);
			//redSum+=sqrt(dy*dy+dx*dx);
			//cout << num<< endl;

		}
	}

	illuminationBlue = pow(blueSum/numberPixels, 1/16.0);
	illuminationGreen = pow(greenSum/numberPixels, 1/16.0);
	illuminationRed = pow(redSum/numberPixels, 1/16.0);

	Scalar blue = (sum(camChannels2[0]));
	double blueVal = (blue[0]/numberPixels);

	Scalar greenChan = (sum(camChannels2[1]));
	double greenVal = (greenChan[0]/numberPixels);

	Scalar redChan = (sum(camChannels2[2]));
	double redVal = (redChan[0]/numberPixels);

	for (int y = 0; y < cameraFeed.rows; y++)
	{
		for (int x = 0; x < cameraFeed.cols; x++)
		{
		Point2f location(x, y);
		camChannels2[0].at<uchar>(location.y, location.x)=camChannels2[0].at<uchar>(location.y, location.x)*(illuminationBlue/blueVal);
		//cout << num<< endl;
		}
	}
	for (int y = 0; y < cameraFeed.rows; y++)
	{
		for (int x = 0; x < cameraFeed.cols; x++)

		{
		Point2f location(x, y);
		camChannels2[1].at<uchar>(location.y, location.x)=camChannels2[1].at<uchar>(location.y, location.x)*(illuminationGreen/greenVal);
		//cout << num<< endl;
		}
	}
	for (int y = 0; y < cameraFeed.rows; y++)
	{
		for (int x = 0; x < cameraFeed.cols; x++)
		{
		Point2f location(x, y);
		camChannels2[2].at<uchar>(location.y, location.x)=camChannels2[2].at<uchar>(location.y, location.x)*(illuminationRed/redVal);
		//cout << num<< endl;
		}
	}
	merge(camChannels2, 3, normalized);
	return normalized;
}

void mouseHandler(int event, int x, int y, int flags, void* param)
{
	extern int numSamples;
	extern vector<Rect> rect;
	if (event == CV_EVENT_LBUTTONDOWN && !drag){
		/* left button clicked. ROI selection begins */
		point1 = Point(x, y);
		drag = 1;
	}

	if (event == CV_EVENT_MOUSEMOVE && drag){
		/* mouse dragged. ROI being selected */
		point2 = Point(x, y);
	}

	if (event == CV_EVENT_LBUTTONUP && drag){
		point2 = Point(x, y);
		rect.push_back(Rect(point1.x,point1.y,x-point1.x,y-point1.y));
		drag = 0;
	}

	if (event == CV_EVENT_LBUTTONUP){
		/* ROI selected */
		if(boxCounter) numSamples++;
		boxCounter=true;
		drag = 0;
		theFlag=true;
	}
}
