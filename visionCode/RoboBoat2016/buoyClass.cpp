#ifndef BUOY_CLASS_CPP
#define BUOY_CLASS_CPP

#include "../include/sulu/buoyClass.h"
#include "../include/sulu/buoyLocation.h"

BUOY::BUOY(string buoyColor)
{
	maxNumberObjects = 40;
	color = buoyColor;
}

void BUOY::findValues()
{
	string fileName = "../Documents/RoboBoat2015/rosCatkinWS/src/sulu/descriptors/" +  color + ".txt";
	//cout << fileName << endl;
	ifstream valueReader;
	valueReader.open(fileName.c_str());
	if (valueReader.is_open())
	{
		valueReader >> minObjectArea >> maxObjectArea >> chan1Min >> chan1Max >> chan2Min >> chan2Max >> chan3Min >> chan3Max;
	}
	else
		cout << "sorry, couldn't open the value file" << endl;

	valueReader.close();

	//cout << chan1Min << endl;
	//cout << chan1Max << endl;
	//cout << chan2Min << endl;
	//cout << chan2Max << endl;
	//cout << chan3Min << endl;
	//cout << chan3Max << endl;
}

void BUOY::setThreshold(Mat manipuatedMat)
{
	inRange(manipuatedMat, Scalar(chan1Min, chan2Min, chan3Min), Scalar(chan1Max, chan2Max, chan3Max), thresholdedLocal);
}

void BUOY::morphOps()
{
	//the element chosen here is a 4px by 4px rectangle, it will be used
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(4, 4));

	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));

	//shrinks the thresholded image
	erode(thresholdedLocal, thresholdedLocal, erodeElement);
	//erode(thresholdedLocal, thresholdedLocal, erodeElement);

	//enlarges the remaining thresholded image
	dilate(thresholdedLocal, thresholdedLocal, dilateElement);
	dilate(thresholdedLocal, thresholdedLocal, dilateElement);
}

void BUOY::trackObject(Mat &cameraFeed)
{
	//sets up temporary location for thresholded image
	Mat temp;
	OBJ buoy;
	thresholdedLocal.copyTo(temp);
	//these two vectors are needed for output of findContours
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	//find contours of filtered image using openCV findContours function
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	//use moments method to find our filtered object
	double refArea = 0;
	this->objectFound = false;
	if (hierarchy.size() > 0)
	{
		//ensures we are not tracking too many objects due to a noisy filter
		int numObjects = hierarchy.size();
		//cout << numObjects << ", " << maxNumberObjects << endl;
		if (numObjects<maxNumberObjects)
		{
			for (int index = 0; index >= 0; index = hierarchy[index][0])
			{
				//openCV class "Moments" for information about location and size
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;

				//thresholds out small objects
				if (area>minObjectArea && area<maxObjectArea)
				{
					double xPos = moment.m10 / area;
					double yPos = moment.m01 / area;
					buoy.set(xPos, yPos);

					//cout << location.xPos << ", " << location.yPos << endl;
					//populates the vector for this object with x, y coordinates for plotting
					objVec.push_back(buoy);
					//cout << objVec.size();
					this->objectFound = true;
					count++;
				}
				else this->objectFound = false;
			}

		}
		else putText(cameraFeed, "sorry, i just can't handle all this noise", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void BUOY::drawObject(Mat &cameraFeed, Mat &distanceMat)
{
	//cout << objVec.size() << endl;
	for (unsigned int i = 0; i<objVec.size(); i++)
	{
		int startX = floor(objVec[i].xPos)-10, startY = floor(objVec[i].yPos)-30;
		distance=0;
		float count=0;
		bearing=0;
		Mat tempMat[3];
		split(distanceMat, tempMat);
		for(startY = floor(objVec[i].yPos); startY<floor(objVec[i].yPos)+30; startY++)
		{
			for(startX = floor(objVec[i].xPos); startX<floor(objVec[i].xPos)+10; startX++)
			{
				//cout << distanceMat.at<float>(startY, startX << endl;
				if(tempMat[0].at<float>(startY, startX)>.05)
				{
					distance+=tempMat[0].at<float>(startY, startX);
					bearing+=tempMat[1].at<float>(startY, startX);
					//cout << "the first distance is: " << distance << endl;
					count++;
				}
			}
		}
		merge(tempMat, 2, distanceMat);
		if(distance&&bearing)
		{
			distance/=count;
			bearing/=count;
			bearing*=-1;
			//cout << "the distance is: " << distance << endl;
			//cout << "the bearing is: " << bearing << endl;
		}


		stringstream ssDistance, ssbearing;
		ssDistance<<distance;
		ssbearing<<bearing;
		string sDistance = ssDistance.str();
		string sbearing = ssbearing.str();
		if(distance&&bearing)
		{
			cv::circle(cameraFeed, cv::Point(objVec[i].xPos, objVec[i].yPos), 10, cv::Scalar(0, 0, 0));
			cv::putText(cameraFeed, color + sDistance + ", " + sbearing, cv::Point(objVec[i].xPos, objVec[i].yPos + 20), 1, 1, Scalar(0, 255, 0));
			this->objectFound = true;
		}
		else
       		{
			this->objectFound = false;
        	}
	}
}

void BUOY::show()
{
	string threshColor = "threshold: " + color;
	imshow(threshColor, thresholdedLocal);
}

#endif
