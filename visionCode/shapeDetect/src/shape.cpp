#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <fstream>
#include <iostream>

using namespace std;
using namespace cv;

int blackdetecter(Mat &cameraFeed);
int bluedetecter(Mat &cameraFeed);
int reddetecter(Mat &cameraFeed);
int greendetecter(Mat &cameraFeed);

void setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour);

int main(int argc, char** argv)
{
	Mat cameraFeed;// = imread(argv[1]);
	Mat selectedColorSpace;
	Mat blurredCameraFeed;
	//if(cameraFeed.empty()&&argv[1]!=0)
	//{
	//	cout << "error with your request, please check your shape file names" << endl;
	//	cout << "and try again." << endl;
	//	return -1;
	//}
	//if(cameraFeed.empty())
	//{
	//	cout << "Selecting default image (cross)" << endl;
	//	cameraFeed = imread("cross.jpg");
	//}

	VideoCapture cap("../cameraFeed.avi");

	//the element chosen here is a 4px by 4px rectangle, it will be used
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(4, 4));

	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));

	while(1)
	{
		cap>>cameraFeed;
		blur(cameraFeed, blurredCameraFeed, Size(10,10), Point(-1,-1), BORDER_DEFAULT);
		cvtColor(blurredCameraFeed,selectedColorSpace,CV_BGR2YCrCb);
		//cvtColor(blurredCameraFeed,selectedColorSpace,CV_BGR2YCrCb);
		erode(selectedColorSpace, selectedColorSpace, erodeElement);

		//enlarges the remaining thresholded image
		dilate(selectedColorSpace, selectedColorSpace, dilateElement);
		if(cameraFeed.empty())
		{
			cout << "not good" << endl;
			return -1;
		}

		blackdetecter(selectedColorSpace);
		bluedetecter(selectedColorSpace);
		greendetecter(selectedColorSpace);
		reddetecter(selectedColorSpace);
        	if(waitKey(30) >= 0) break;
	}
    return 0;
}

//This function will return an integer and must be passed an OpenCV camera matrix when called.
int blackdetecter(Mat &cameraFeed)
{
	//setup
	int black1, black2, black3, black4, black5, black6;

	std::string black = "descriptors/blackycrcb.txt";

	ifstream blackReader;
	blackReader.open(black.c_str());
	if (blackReader.is_open())
	{
		blackReader >> black1 >> black2 >> black3 >> black4 >> black5 >> black6;
	}
	else
		cout << "sorry, couldn't open the black file" << endl;

	blackReader.close();

	cv::Mat src = cameraFeed;
	if (src.empty())
	{
		cout << "Nada Boss" << endl;
		return -1;
	}
	// Convert to grayscale
	cv::Mat bwBlack;

	inRange(src, cv::Scalar(black1, black3, black5), cv::Scalar(black2, black4, black6), bwBlack);//black

	imshow("filtered black", bwBlack);

	// Find contours
	std::vector<std::vector<cv::Point> > contoursBlack;
	cv::findContours(bwBlack.clone(), contoursBlack, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approxBlack;
	cv::Mat dstBlack = src.clone();


	for (int i = 0; i < contoursBlack.size(); i++)
		{
			// Approximate contour with accuracy proportional
			// to the contour perimeter
			cv::approxPolyDP(cv::Mat(contoursBlack[i]), approxBlack, cv::arcLength(cv::Mat(contoursBlack[i]), true)*0.02, true);
			cout << approxBlack.size() << endl;
			// Skip small or non-convex objects
			//if (std::fabs(cv::contourArea(contoursBlack[i])) < 100 || !cv::isContourConvex(approxBlack))
				//continue;
			if (approxBlack.size() == 3)
			{
				setLabel(dstBlack, "BLACK-TRI", contoursBlack[i]);    // Triangles
			}
			else if (approxBlack.size() == 12)
			{
				setLabel(dstBlack, "BLACK-CROSS", contoursBlack[i]);    // Triangles
			}
			else
			{
				// Detect and label circles
				double area = cv::contourArea(contoursBlack[i]);
				cv::Rect r = cv::boundingRect(contoursBlack[i]);
				int radius = r.width / 2;

				if (std::abs(1 - ((double)r.width / r.height)) <= 0.2 &&
				    std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.2)
					setLabel(dstBlack, "BLACK-CIR", contoursBlack[i]);
			}
		}

		imshow("src", src);
		imshow("dstBlack", dstBlack);
}

int bluedetecter(Mat &cameraFeed)
{
	//setup
	int blue1, blue2, blue3, blue4, blue5, blue6;

	std::string blue = "descriptors/blueycrcb.txt";

	ifstream blueReader;
	blueReader.open(blue.c_str());
	if (blueReader.is_open())
	{
		blueReader >> blue1 >> blue2 >> blue3 >> blue4 >> blue5 >> blue6;
	}
	else
		cout << "sorry, couldn't open the blue file" << endl;

	blueReader.close();

	cv::Mat src = cameraFeed;
	if (src.empty())
	{
		cout << "Nada Boss" << endl;
		return -1;
	}

	cv::Mat bwBlue;

	inRange(src, cv::Scalar(blue1, blue3, blue5), cv::Scalar(blue2, blue4, blue6), bwBlue);//blue

	imshow("filtered blue", bwBlue);

	// Find contours
	std::vector<std::vector<cv::Point> > contoursBlue;
	cv::findContours(bwBlue.clone(), contoursBlue, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approxBlue;
	cv::Mat dstBlue = src.clone();


	for (int i = 0; i < contoursBlue.size(); i++)
		{
			// Approximate contour with accuracy proportional
			// to the contour perimeter
			cv::approxPolyDP(cv::Mat(contoursBlue[i]), approxBlue, cv::arcLength(cv::Mat(contoursBlue[i]), true)*0.02, true);
			cout << approxBlue.size() << endl;
			// Skip small or non-convex objects
			//if (std::fabs(cv::contourArea(contoursBlue[i])) < 100 || !cv::isContourConvex(approxBlue))
				//continue;
			if (approxBlue.size() == 3)
			{
				setLabel(dstBlue, "BLUE-TRI", contoursBlue[i]);    // Triangles
			}
			else if (approxBlue.size() == 12)
			{
				setLabel(dstBlue, "BLUE-CROSS", contoursBlue[i]);    // Triangles
			}
			else
			{
				// Detect and label circles
				double area = cv::contourArea(contoursBlue[i]);
				cv::Rect r = cv::boundingRect(contoursBlue[i]);
				int radius = r.width / 2;

				if (std::abs(1 - ((double)r.width / r.height)) <= 0.2 &&
				    std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.2)
					setLabel(dstBlue, "BLUE-CIR", contoursBlue[i]);
			}
		}

		imshow("src", src);
		imshow("dstBlue", dstBlue);
}

int reddetecter(Mat &cameraFeed)
{
	//setup
	int red1, red2, red3, red4, red5, red6;

	std::string red = "descriptors/redycrcb.txt";
	ifstream redReader;
	redReader.open(red.c_str());
	if (redReader.is_open())
	{
		redReader >> red1 >> red2 >> red3 >> red4 >> red5 >> red6;
	}
	else
		cout << "sorry, couldn't open the red file" << endl;

	redReader.close();

	cv::Mat src = cameraFeed;
	if (src.empty())
	{
		cout << "Nada Boss" << endl;
		return -1;
	}

	cv::Mat bwRed;

	inRange(src, cv::Scalar(red1, red3, red5), cv::Scalar(red2, red4, red6), bwRed);//red

	imshow("filtered red", bwRed);

	// Find contours
	std::vector<std::vector<cv::Point> > contoursRed;
	cv::findContours(bwRed.clone(), contoursRed, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approxRed;
	cv::Mat dstRed = src.clone();


	for (int i = 0; i < contoursRed.size(); i++)
		{
			// Approximate contour with accuracy proportional
			// to the contour perimeter
			cv::approxPolyDP(cv::Mat(contoursRed[i]), approxRed, cv::arcLength(cv::Mat(contoursRed[i]), true)*0.02, true);
			cout << approxRed.size() << endl;
			// Skip small or non-convex objects
			 //(std::fabs(cv::contourArea(contoursRed[i])) < 100 || !cv::isContourConvex(approxRed));
				//continue;
			if (approxRed.size() == 3)
			{
				setLabel(dstRed, "RED-TRI", contoursRed[i]);    // Triangles
			}
			else if (approxRed.size() == 12)
			{
				setLabel(dstRed, "RED-CROSS", contoursRed[i]);    // Triangles
			}
			else
			{
				// Detect and label circles
				double area = cv::contourArea(contoursRed[i]);
				cv::Rect r = cv::boundingRect(contoursRed[i]);
				int radius = r.width / 2;

				if (std::abs(1 - ((double)r.width / r.height)) <= 0.2 &&
				    std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.2)
					setLabel(dstRed, "RED-CIR", contoursRed[i]);
			}
		}

		imshow("src", src);
		imshow("dstRed", dstRed);
}

int greendetecter(Mat &cameraFeed)
{
	//setup
	int green1, green2, green3, green4, green5, green6;

	std::string green = "descriptors/greenycrcb.txt";
	ifstream greenReader;
	greenReader.open(green.c_str());
	if (greenReader.is_open())
	{
		greenReader >> green1 >> green2 >> green3 >> green4 >> green5 >> green6;
	}
	else
		cout << "sorry, couldn't open the green file" << endl;

	greenReader.close();

	cv::Mat src = cameraFeed;
	if (src.empty())
	{
		cout << "Nada Boss" << endl;
		return -1;
	}
	// Convert to grayscale

	cv::Mat bwGreen;

	inRange(src, cv::Scalar(green1, green3, green5), cv::Scalar(green2, green4, green6), bwGreen);//green

	imshow("filtered green", bwGreen);

	// Find contours
	std::vector<std::vector<cv::Point> > contoursGreen;
	cv::findContours(bwGreen.clone(), contoursGreen, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<cv::Point> approxGreen;
	cv::Mat dstGreen = src.clone();


	for (int i = 0; i < contoursGreen.size(); i++)
		{
			// Approximate contour with accuracy proportional
			// to the contour perimeter
			cv::approxPolyDP(cv::Mat(contoursGreen[i]), approxGreen, cv::arcLength(cv::Mat(contoursGreen[i]), true)*0.02, true);
			cout << approxGreen.size() << endl;
			// Skip small or non-convex objects
			//if (std::fabs(cv::contourArea(contoursGreen[i])) < 100 || !cv::isContourConvex(approxGreen))
				//continue;
			if (approxGreen.size() == 3)
			{
				setLabel(dstGreen, "GREEN-TRI", contoursGreen[i]);    // Triangles
			}
			else if (approxGreen.size() == 12)
			{
				setLabel(dstGreen, "GREEN-CROSS", contoursGreen[i]);    // Triangles
			}
			else
			{
				// Detect and label circles
				double area = cv::contourArea(contoursGreen[i]);
				cv::Rect r = cv::boundingRect(contoursGreen[i]);
				int radius = r.width / 2;

				if (std::abs(1 - ((double)r.width / r.height)) <= 0.2 &&
				    std::abs(1 - (area / (CV_PI * std::pow(radius, 2)))) <= 0.2)
					setLabel(dstGreen, "GREEN-CIR", contoursGreen[i]);
			}
		}

		imshow("src", src);
		imshow("dstGreen", dstGreen);
}

void setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour)
{
	int fontface = cv::FONT_HERSHEY_SIMPLEX;
	double scale = 0.4;
	int thickness = 1;
	int baseline = 0;

	cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
	cv::Rect r = cv::boundingRect(contour);

	cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
	cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
	cv::putText(im, label, pt, fontface, scale, CV_RGB(120,120,120), thickness, 8);
}
