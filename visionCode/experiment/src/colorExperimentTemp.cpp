//Original Author:Travis Moscicki
//Date:4/16/2016
//Last Edit:5/11/2016 - Travis Moscicki

//The purpose of this script is to capture image data about different colored objects
//by selecting a region of interest in the frame, converting this to a number of
//different color spaces, and then logging said data.

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <vector>

using namespace std;
using namespace cv;

int drag = 0;
bool boxCounter=false;
Point point1, point2;
Rect rect;
Mat img, roiImg;
Point point3(100, 100);
Point point4(200, 200);

Mat cameraFeed;
vector<Mat> hsvROI;
vector<Mat> yCrCbROI;
vector<Mat> grayscaleROI;
Mat grayscaleROITemp;
vector<Mat> averageROI;
vector<Mat> rgbROI;

void mouseHandler(int event, int x, int y, int flags, void* param);

int main()
{
	VideoCapture cap(0);
	if(!cap.isOpened()){
		cout<<"Is the camera plugged in?"<<endl;
		return 1;
	}

    int i=0;
    string str;
    bool display=false;
	while(1)
	{
		cap>>cameraFeed;
		cvSetMouseCallback("cameraFeed", mouseHandler, NULL);
        namedWindow("cameraFeed", CV_WINDOW_AUTOSIZE);

		if(boxCounter){
            display=true;
	        hsvROI.push_back(cameraFeed(rect).clone());
	        yCrCbROI.push_back(cameraFeed(rect).clone());
            cvtColor(cameraFeed(rect).clone(), grayscaleROITemp, COLOR_BGR2GRAY);
	        grayscaleROI.push_back(grayscaleROITemp);
	        averageROI.push_back(cameraFeed(rect).clone());
	        rgbROI.push_back(cameraFeed(rect).clone());
	        for(int t=0; t<=i; t++){
                cvtColor(hsvROI[t], hsvROI[t], COLOR_BGR2HSV);
                cvtColor(yCrCbROI[t], yCrCbROI[t], COLOR_BGR2YCrCb);
		boxCounter=false;
            }
            stringstream stream;
            stream<<i;
            str=stream.str();
            namedWindow("hsvROI"+str, CV_WINDOW_AUTOSIZE);
            namedWindow("yCrCbROI"+str, CV_WINDOW_AUTOSIZE);
            namedWindow("grayscaleROI"+str, CV_WINDOW_AUTOSIZE);
            namedWindow("averageROI"+str, CV_WINDOW_AUTOSIZE);
            namedWindow("rgbROI"+str, CV_WINDOW_AUTOSIZE);
            i++;
		}
        if(display){
            imshow("hsvROI"+str, hsvROI[i-1]);
			imshow("yCrCbROI"+str, yCrCbROI[i-1]);
			imshow("grayscaleROI"+str, grayscaleROI[i-1]);
			imshow("averageROI"+str, averageROI[i-1]);
			imshow("rgbROI"+str, rgbROI[i-1]);
        }
		imshow("cameraFeed", cameraFeed);
        int esc = cvWaitKey(20);
        if( esc == 1048603) break;
	}
	return 0;
}

void mouseHandler(int event, int x, int y, int flags, void* param)
{
	if (event == CV_EVENT_LBUTTONDOWN && !drag){
		/* left button clicked. ROI selection begins */
		point1 = Point(x, y);
		drag = 1;
	}

	if (event == CV_EVENT_MOUSEMOVE && drag){
		/* mouse dragged. ROI being selected */
		point2 = Point(x, y);
	}

	if (event == CV_EVENT_LBUTTONUP && drag){
		point2 = Point(x, y);
		rect = Rect(point1.x,point1.y,x-point1.x,y-point1.y);
		drag = 0;
	}

	if (event == CV_EVENT_LBUTTONUP){
		/* ROI selected */
		boxCounter=true;
		drag = 0;
	}
}
