#ifndef SELECTROI_H
#define SELECTROI_H

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

class selectROI
{
public:
	selectROI();
	~selectROI();
	void insertNewFrame(Mat &cameraFeedMain);
	void display();
	void split();
	bool escape();

private:
	Mat cameraFeed;
	Mat cameraFeedROI;
	Mat hsvROI;
	Mat yCrCbROI;
	Mat greyscaleROI;
	Mat averageROI;
	Mat rgbROI;
};

#endif
